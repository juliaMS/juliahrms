
using CSV



##############################################################################
# Test area

path2file="C:\\Temp_File\\Jake\\180420-SWATH-JO054-006 Day5 C_report.csv"


#############################################################################
# Start of the module
#
# Hard coded parameters

pos_add_m=[18.033823,22.989218,33.033489,38.963158,42.033823,44.971160,61.065340,
    64.015765,76.919040,79.021220,83.060370,84.055110]
pos_add_name=["M+NH4","M+Na","M+CH3OH+H","M+K","M+ACN+H","M+2Na-H","M+IsoProp+H",
    "M+ACN+Na","M+2K+H","M+DMSO+H","M+2ACN+H","M+IsoProp+Na+H"]


############################################################################
# Start functions

# Wrapper function

function adduct_rem_int(path2file)

    feature_list=CSV.read(path2file)

    for i=1:length(feature_list.Nr)
        Nr=feature_list.Nr[i]

    end


end # function

############################################################################
#
# Adduct find

function adduct_detect_int(Nr,feature_list)
    mass=feature_list.MeasMass
    mass_win=abs(feature_list.MaxMass[Nr]-feature_list.MinMass[Nr])/2
    ret_win=feature_list.ScanInPeak[Nr]
    ret_t=feature_list.ScanNum


end # function
