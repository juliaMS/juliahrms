
push!(LOAD_PATH,pwd())

using SAFD
using MS_Import

##############################################################################

pathin="D:\\Data\\Phil_Data\\mzXML_files"


max_numb_iter=20000
max_t_peak_w=40
res=20000
min_ms_w=0.02
r_tresh=0.85
min_int=2000
sig_inc_thresh=5
S2N=2
#format="cdf"
#format="mat"
format="mzxml"
mz_thresh=[0,600]
min_peak_w_s=3

function feature_detect_batch(pathin,max_numb_iter,
    max_t_peak_w,res,min_ms_w,r_tresh,min_int,sig_inc_thresh,S2N,format,min_peak_w_s,mz_thresh)


    mz_vales_mat,mz_int_mat,t0_mat,t_end_mat,m_mat,pathin_mat=Import_files_batch_MS1(format,pathin,mz_thresh)

    Max_it=repeat([max_numb_iter],size(pathin_mat,1))
    Max_t_peak_w=repeat([max_t_peak_w],size(pathin_mat,1))
    Res=repeat([res],size(pathin_mat,1))
    Min_ms_w=repeat([min_ms_w],size(pathin_mat,1))
    R_tresh=repeat([r_tresh],size(pathin_mat,1))
    Min_int=repeat([min_int],size(pathin_mat,1))
    Sig_inc_thresh=repeat([sig_inc_thresh],size(pathin_mat,1))
    S2N1=repeat([S2N],size(pathin_mat,1))
    Min_peak_w_s=repeat([min_peak_w_s],size(pathin_mat,1))


    # i=1

    for i=1:size(pathin_mat,1)
        feature_detect_signle_data(mz_vales_mat[i],
        mz_int_mat[i],t0_mat[i],t_end_mat[i],m_mat[i],pathin_mat[i],Max_it[i],
        Max_t_peak_w[i],Res[i],Min_ms_w[i],R_tresh[i],Min_int[i],Sig_inc_thresh[i],
        S2N1[i],Min_peak_w_s[i])

    end


end




@time feature_detect_batch(pathin,max_numb_iter,
    max_t_peak_w,res,min_ms_w,r_tresh,min_int,sig_inc_thresh,S2N,format,min_peak_w_s,mz_thresh)


#interrupt()
