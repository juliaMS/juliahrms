
using LightXML
using Unitful
using Codecs

#using EzXML


cd("/Users/saersamanipour/Desktop/dev/ongoing/juliahrms/")
push!(LOAD_PATH,"/Users/saersamanipour/Desktop/dev/ongoing/juliahrms/")



###############################################################################
# Testing area

#path2mzxml="/Users/saersamanipour/Desktop/dev/ongoing/mzXML.jl/test/test32.mzXML"
path2mzxml="/Users/saersamanipour/Desktop/dev/Data/100PPB DRUGS.mzXML"
mz_thresh=[0,0]

@time chrom=mzxml_read(path2mzxml,mz_thresh)





###############################################################################
# mzXML read

function mzxml_read(path2mzxml,mz_thresh)

    xdoc = parse_file(path2mzxml)
    xroot = root(xdoc)
    if name(xroot) != "mzXML"
        error("Not an mzXML file")
    end
    # Find the msRun node
    msRun = find_element(xroot, "msRun")

    el_ins = find_element(msRun, "msInstrument")
    Ins = attribute(find_element(el_ins, "msModel"),"value")
    el_ion = attribute(find_element(el_ins,"msIonisation"),"value")
    el_man = attribute(find_element(el_ins,"msManufacturer"),"value")

    polarity,pre_mz,pre_mz_win,retentionTime,msLevel,basePeakMz,
    totIonCurrent,scan_type,centroided,mz,mz_int=read_scan(msRun,mz_thresh)
    chrom=chrom_fold(polarity,pre_mz,pre_mz_win,retentionTime,msLevel,basePeakMz,
        totIonCurrent,scan_type,centroided,mz,mz_int)

    return chrom

end # function



###############################################################################
# Reading the scans

function read_scan(msRun,mz_thresh)
    polarity=[]
    pre_mz=[]
    pre_mz_win=[]
    retentionTime=[]
    msLevel=[]
    basePeakMz=[]
    totIonCurrent=[]
    scan_type=[]
    centroided=[]
    mz=[]
    mz_int=[]

    for line in child_elements(msRun)
        if name(line) == "scan"
            #println(line)
            polarity1,pre_mz1,pre_mz_win1,retentionTime1,msLevel1,basePeakMz1,
            totIonCurrent1,scan_type1,centroided1,mz1,I=read_scan_info(line,mz_thresh)

            polarity=vcat(polarity,polarity1)
            pre_mz=vcat(pre_mz,pre_mz1)
            pre_mz_win=vcat(pre_mz_win,pre_mz_win1)
            retentionTime=vcat(retentionTime,retentionTime1)
            msLevel=vcat(msLevel,msLevel1)
            basePeakMz=vcat(basePeakMz,basePeakMz1)
            totIonCurrent=vcat(totIonCurrent,totIonCurrent1)
            scan_type=vcat(scan_type,scan_type1)
            centroided=vcat(centroided,centroided1)
            mz=vcat(mz,[mz1])
            mz_int=vcat(mz_int,[I])
            #println(pre_mz_win)

        end

    end

    return(polarity,pre_mz,pre_mz_win,retentionTime,msLevel,basePeakMz,
    totIonCurrent,scan_type,centroided,mz,mz_int)

end # function


###############################################################################
# Extracting info from the scans

function read_scan_info(line,mz_thresh)

    precisiondict = Dict("32" => (Float32, Float32, Float32), "64" => (Float64, Float64, Float64))

    polarity = attribute(line, "polarity")
    #println(line)
    mm=find_element(line,"precursorMz")
    if mm != nothing
        pre_mz=parse(Float64,content(mm))
        if attribute(mm,"windowWideness") != nothing
            pre_mz_win =parse(Float64,attribute(mm,"windowWideness"))
        else
            pre_mz_win=0.0
        end
    else
        pre_mz=0.0
        pre_mz_win=0.0
    end

    tstr=attribute(line, "retentionTime")
    retentionTime = parse(Float64, tstr[3:end-1])
    msLevel = attribute(line, "msLevel")
    basePeakMz = parse(Float64, attribute(line, "basePeakMz"))
    totIonCurrent = parse(Float64, attribute(line, "totIonCurrent"))
    scan_type = attribute(line, "scanType")
    centroided = attribute(line,"centroided")
    peak = find_element(line, "peaks")
    data = decode(Base64, content(peak))
    TI, T, nochildren = precisiondict[attribute(peak, "precision")]
    A = reinterpret(TI, data)
    bo = attribute(peak, "byteOrder")
    if bo == "network"
        ntoh!(A)
    else
        error("Don't know what to do with byteOrder $bo")
    end
    I = A[2:2:end]

    mz = reinterpret(T, A[1:2:end])

    if mz_thresh[2]>0
        mz1=mz[findall(x -> mz_thresh[1]<= x <= mz_thresh[2], mz)]
        I1=I[findall(x -> mz_thresh[1]<= x <= mz_thresh[2], mz)]
    elseif mz_thresh[1] == 0
        mz1=mz[mz .>= mz_thresh[1]]
        I1=I[mz .>= mz_thresh[1]]

    end


    return(polarity,pre_mz,pre_mz_win,retentionTime,msLevel,basePeakMz,totIonCurrent,scan_type,centroided,mz1,I1)
end # function






##############################################################################
# XML encoding


function ntoh!(A)
    for i = 1:length(A)
        A[i] = ntoh(A[i])
    end
end



##############################################################################
# Chrom folding

function chrom_fold(polarity,pre_mz,pre_mz_win,retentionTime,msLevel,basePeakMz,
    totIonCurrent,scan_type,centroided,mz,mz_int)

    chrom=Dict()

    ms_levels=unique(msLevel)
    par=["Mz_values","Mz_intensity",]

    for i=1:length(ms_levels)
        chrom["MS$i"] = Dict("Rt" => round.(retentionTime[msLevel .== ms_levels[i]]./60, digits=2),
        "Polarity" => polarity[msLevel .== ms_levels[i]],
        "PrecursorIon" => pre_mz[msLevel .== ms_levels[i]],
        "PrecursorIonWin" => pre_mz_win[msLevel .== ms_levels[i]],
        "BasePeak" => basePeakMz[msLevel .== ms_levels[i]],
        "TIC" => totIonCurrent[msLevel .== ms_levels[i]],
        "ScanType" => scan_type[msLevel .== ms_levels[i]],
        "Centroid" => centroided[msLevel .== ms_levels[i]],
        "Mz_values"=> Array{Float64,2}(undef,1,1),
        "Mz_intensity"=>Array{Float64,2}(undef,1,1))
        mz_s=mz[msLevel .== ms_levels[i]]
        mz_int_s=mz_int[msLevel .== ms_levels[i]]

        chrom["MS$i"]["Mz_values"]=array2mat(mz_s)
        chrom["MS$i"]["Mz_intensity"]=array2mat(mz_int_s)


    end

    return chrom

end # function



########################################################################
# Array to matrix
#

function array2mat(array)

    dummy_var=zeros(size(array,1),maximum(size.(array[1:end],1)))

    for i=1:size(array,1)
        dummy_var[i,1:length(array[i])]=array[i]
    end
    GC.gc()
    return dummy_var

end


###############################################################################
#
