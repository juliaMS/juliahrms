
cd("/media/saer/Data/Data/julia/juliahrms/")
push!(LOAD_PATH,"/media/saer/Data/Data/julia/juliahrms/")

#using Conda

#using FeatureDetection
using SAFD
using BenchmarkTools

using MS_Import



###############################################################################
# Run the Script

# Import parameters

pathin="/Users/saersamanipour/Desktop/dev/Data"
filenames=["200907_SWATH_NEG_10-50 PFAS std mix.mzXML"]
mz_thresh=[0,550]

max_numb_iter=10000

max_t_peak_w=300
res=20000
min_ms_w=0.02
r_tresh=0.75
min_int=1000
sig_inc_thresh=5
S2N=2


min_peak_w_s=3

min_peak_w_s=3



GC.gc()


mz_vals,mz_int,t0,t_end,m,pathin,msModel,msIonisation,msManufacturer,polarity=import_files_MS1(pathin,filenames,mz_thresh)


GC.gc()

FileName=m[1]

@time rep_table,final_table=safd_s3D(mz_vals,mz_int,t0,
    t_end,FileName,pathin,max_numb_iter,max_t_peak_w,res,min_ms_w,r_tresh,
    min_int,sig_inc_thresh,S2N,min_peak_w_s)
