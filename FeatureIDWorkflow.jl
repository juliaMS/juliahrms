
using Distributed


push!(LOAD_PATH,"/media/saer/Data/Data/julia/juliahrms/")
#@everywhere push!(LOAD_PATH,$pwd())

using FeatureDeconvolution
using FeatureIdentification
using MS_Import
using HDF5
using JLD
using CSV
using BenchmarkTools
using DataFrames


############################################################################
#
# Wrapping function

function featureID_SWATH_chrom_inter(lib,chrom,path2features,pathin,
    filenames,path2SWATH,r_tresh,min_int,mass_win_per,ret_win_per,mode,source)


    feature_list=CSV.read(path2features)


    global final_table=DataFrame(zeros(1,17),[:ID,:Rt,:MS1Mass,:Name,:RefMatchFrag,:UsrMatchFrag,
    :MS1Error,:MS2Error,:MS2ErrorStd,:DirectMatch,:ReversMatch,:FinalScore,
    :SpecType,:MatchedFrags,:Cas,:FragMZ,:FragInt])

    # i=445

    for i=1:size(feature_list,1)
        println(i)
        try
            frag_mz,frag_int,mass_tol=feature_decon_SWATH_singlefeature_internal(chrom,
                filenames,path2SWATH,r_tresh,min_int,feature_list[i,:],mass_win_per,ret_win_per)
            if size(frag_mz,1)>0

                rep=featureID_single_feature_inter(lib,frag_mz,frag_int,mass_tol,feature_list[i,:],
                    mode,source,parent,weight_f)
            else
                rep=[]
            end

            if size(rep,1)>0
                global final_table=vcat(final_table,rep)
            end
        catch
            println("An error occured during the identification of feature number $i !")
            continue
        end

    end


    table=final_table[2:end,:]

    sort!(table,(:ID,:FinalScore),rev=(false, true))

    if isa(filenames,Array)==1
        m=split(filenames[1],".")
    else
        m=split(filenames,".")
    end

    output=joinpath(pathin,m[1]) * "_IDs.csv"
    CSV.write(output,table)
    println("The final report is saved in the indicated folder!")
    return table
end



############################################################################
#
# Warpping function

function featureID_DIA_chrom_inter(lib,chrom,path2features,pathin,
    filenames,r_tresh,min_int,mass_win_per,ret_win_per,mode,source)


    feature_list=CSV.read(path2features)


    global final_table=DataFrame(zeros(1,17),[:ID,:Rt,:MS1Mass,:Name,:RefMatchFrag,:UsrMatchFrag,
    :MS1Error,:MS2Error,:MS2ErrorStd,:DirectMatch,:ReversMatch,:FinalScore,
    :SpecType,:MatchedFrags,:Cas,:FragMZ,:FragInt])

    # i=445

    for i=1:size(feature_list,1)
        println(i)
        try
            frag_mz,frag_int,mass_tol=feature_decon_DIA_singlefeature_internal(chrom,filenames,r_tresh,
                min_int,feature_list[i,:],mass_win_per,ret_win_per)


            if size(frag_mz,1)>0

                rep=featureID_single_feature_inter(lib,frag_mz,frag_int,mass_tol,feature_list[i,:],
                    mode,source,parent,weight_f)
            else
                rep=[]
            end

            if size(rep,1)>0
                global final_table=vcat(final_table,rep)
            end
        catch
            println("An error occured during the identification of feature number $i !")
            continue
        end

    end


    table=final_table[2:end,:]

    sort!(table,(:ID,:FinalScore),rev=(false, true))

    if isa(filenames,Array)==1
        m=split(filenames[1],".")
    else
        m=split(filenames,".")
    end

    output=joinpath(pathin,m[1]) * "_IDs.csv"
    CSV.write(output,table)
    println("The final report is saved in the indicated folder!")
    return table
end

#############################################################################
#
# Feature ID batch via components

function ulsa_comp_batch(path2comps,lib,mode,source,weight_f)

    com_list=readdir(path2comps)
    for i=1:size(com_list,1)
        m=split(com_list[i],".")
<<<<<<< HEAD
        if m[2] == "csv" && m[1][end-4:end] == "_comp"
=======
        if m[2] == "csv" && m[1][end-3:end]=="comp"
>>>>>>> 6e51373230f789f9b46513dcd28a3fe0749cf5e0
            path2spec=joinpath(path2comps,com_list[i])
            table = featureID_comp_inter(lib,mode,source,path2spec,weight_f)
        end
    end

end # function

i=3

<<<<<<< HEAD
m=basename(path2spec)

output=joinpath(dirname(path2spec),m[1:end-4]) * "_IDs.csv"

=======
# i=3
>>>>>>> 6e51373230f789f9b46513dcd28a3fe0749cf5e0


###############################################################################
#
# Testing area

#GC.gc()

# Importing the files
#format="mzxml"
#pathin="C:\\Temp_File\\1000Lakes"
#filenames=["Ea_B3_A_pos-B3_A_pos.mzXML"]
#filenames=["TRU200116_018.RAW.mzXML"]
#mz_thresh=[0,0]

# Decon
#path2features="C:\\Temp_File\\1000Lakes\\TRU200116_018_report.csv"
#mass_win_per=0.8
#ret_win_per=0.25
#r_tresh=0.9
#min_int=500


# Identification
<<<<<<< HEAD
path2comps="/media/saer/Data/Data/Temp_data/Phil/Cal/"
path2lib="/media/saer/Data/Data/julia/juliahrms/MassBankJulia.jld"
=======
path2comps="/Users/saersamanipour/Desktop/UvA/Temp_files/Phil/Drug cal/mzxml_files"
path2lib="/Users/saersamanipour/Desktop/dev/ongoing/juliahrms/MassBankJulia.jld"
>>>>>>> 6e51373230f789f9b46513dcd28a3fe0749cf5e0
mode="POSITIVE"
source="ESI"
weight_f=[1,1,1,1,1,1,1]
#parent=0

##
#chrom=import_files(format,pathin,filenames,mz_thresh)
#println("The chromatogram has been imported!")

#GC.gc()

lib=load(path2lib,"MassBankJulia")
println("The library has been imported!")

#GC.gc()

#@time featureID_DIA_chrom_inter(lib,chrom,path2features,pathin,
#    filenames,r_tresh,min_int,mass_win_per,ret_win_per,mode,source)
#GC.gc()

@time ulsa_comp_batch(path2comps,lib,mode,source,weight_f)
