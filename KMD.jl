 __precompile__(false)

module KMD #start of the module

using Plots
using CSV
using DataFrames
using Glob
using Statistics
using XLSX

export KMD_filter, KMD_filter_batch, KMD_calc, KMD_calc_ext
##############################################################################
## Testing area

#path2file="C:\\Temp_File\\1000Lakes\\Low\\TRU200116_01201_report.csv"
#path2folder="/media/saer/Data/Data/1000Lakes/Reports"

#m_ru=49.9968 # The accurate mass of the reapiting unite
#kmd_bounds=[-0.25,0.1]
#mass_tol=0.05



##############################################################################
# Kendrick mass defect calculation

function KMD_calc(path2file,m_ru)

    feature_list=CSV.read(path2file)
    KM=feature_list.MeasMass.*(round(m_ru)/m_ru) # Kendrick mass
    KMD=round.(round.(KM) .- KM ; digits=3)
    MM=feature_list.MeasMass

    return(KMD,feature_list)
end

##############################################################################
## Kendrick mass defect filter

function KMD_filter(KMD,kmd_bounds,mass_tol,feature_list,path2file)

    feature_list_filtered=feature_list[findall(x -> kmd_bounds[2]+mass_tol > x >= kmd_bounds[1]-mass_tol,KMD),:]
    insertcols!(feature_list_filtered,size(feature_list,2)+1,:KMD => KMD[findall(x -> kmd_bounds[2]+mass_tol > x >= kmd_bounds[1]-mass_tol,KMD)])
    m=split(path2file,".")
    output= m[1] * "_KMD_filtered.csv"
    CSV.write(output,feature_list_filtered)

    return feature_list_filtered
end

##############################################################################
## Kendrick mass defect filter batch

function KMD_filter_batch(kmd_bounds,mass_tol,m_ru,path2folder)

    dir_name=readdir(path2folder)

    for i=1:size(dir_name,1)
        path2file=joinpath(path2folder,dir_name[i])
        KMD,feature_list=KMD_calc(path2file,m_ru)
        feature_list_filtered=KMD_filter(KMD,kmd_bounds,mass_tol,feature_list,path2file)
        println("The file "* dir_name[i] *" has been filtered and saved.")

    end

end


###############################################################################
## KMD calculation of external files

function KMD_calc_ext(mz_values,m_ru)

    KMD=Array{Any}(undef,length(mz_values))
    for i=1:length(mz_values)
        KM=mz_values[i].*(round.(m_ru)./m_ru) # Kendrick mass
        KMD[i]=round.(round.(KM) .- KM ; digits=3)
    end

    return(KMD)
end


end # end of the module
