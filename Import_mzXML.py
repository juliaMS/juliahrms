# -*- coding: utf-8 -*-
"""
Created on Thu Aug 30 13:42:21 2018
@author: SSA, ZCR
This is a function to import mzXML files into python
"""
import numpy as np
import gc
import pyopenms as pm


# import ms data using pyopenms tools
def read_mzxml_py(path2file, mz_value_thresh):
    mz_values = []
    mz_intensity = []
    retention_time = []
    ms_level = []
    experiment = pm.MSExperiment()
    pm.MzXMLFile().load(path2file, experiment)
    for spec in experiment:
        mz, intensity = spec.get_peaks()
        if mz_value_thresh[0] > 0 and mz_value_thresh[1] > 0:
            indices_selected_masses = np.where((mz >= mz_value_thresh[0]) & (mz <= mz_value_thresh[1]))
        elif mz_value_thresh[0] == 0 and mz_value_thresh[1] > 0:
            indices_selected_masses = np.where(mz <= mz_value_thresh[1])
        elif mz_value_thresh[0] > 0 and mz_value_thresh[1] == 0:
            indices_selected_masses = np.where(mz >= mz_value_thresh[0])
        else:
            raise Exception("Wrong thresholds! Its should be [lb, ub]")
        mz_values.append(mz[indices_selected_masses])
        mz_intensity.append(intensity[indices_selected_masses])
        ms_level.append(spec.getMSLevel())
        retention_time.append(spec.getRT())
    # NOTE: do we need gc or python takes care of it??
    gc.collect()
    return mz_values, mz_intensity, retention_time, ms_level


# wrapper
def mzxml_import_py(pathin, filename, mz_value_thresh, requested_level=None):
    path2file = pathin + filename
    result = {}
    mz_values, mz_intensity, retention_time, ms_level = read_mzxml_py(path2file, mz_value_thresh)
    number_of_levels = max(ms_level)

    for ilevel in range(number_of_levels):
        if requested_level is not None and requested_level != ilevel + 1:
            continue
        ms_ilevel = "MS" + str(ilevel + 1)
        ilevel_retention_time = []
        ilevel_mz_values = []
        ilevel_intensity = []
        result[ms_ilevel] = {"Rt": [], "Mz_values": [], "Mz_intensity": []}
        max_scan_length = 0
        for index, level in enumerate(ms_level):
            if level == ilevel + 1:
                ilevel_retention_time.append(retention_time[index])
                ilevel_mz_values.append(mz_values[index])
                ilevel_intensity.append(mz_intensity[index])
                if len(mz_values[index]) > max_scan_length:
                    max_scan_length = len(mz_values[index])
        # Put values and intensities in a matrix
        number_of_ms_scans = len(mz_values)
        ilevel_mz_values_matrix = np.zeros((number_of_ms_scans, max_scan_length))
        ilevel_mz_intensity_matrix = np.zeros((number_of_ms_scans, max_scan_length))
        for scan_number, (mz_scan, int_scan) in enumerate(zip(ilevel_mz_values, ilevel_intensity)):
            if len(mz_scan) != len(int_scan):
                raise Exception("Error importing data: intensities and m/z values have different length")
            if len(mz_scan) > 0 and len(int_scan) > 0:
                ilevel_mz_values_matrix[scan_number, : np.array(mz_scan).shape[0]] = np.array(mz_scan)
                ilevel_mz_intensity_matrix[scan_number, : np.array(int_scan).shape[0]] = np.array(int_scan)

        result[ms_ilevel]["Mz_values"] = ilevel_mz_values_matrix
        result[ms_ilevel]["Mz_intensity"] = ilevel_mz_intensity_matrix
        result[ms_ilevel]["Rt"] = ilevel_retention_time

    if requested_level:
        level = "MS" + str(requested_level)
        return result[level]["Mz_values"], result[level]["Mz_intensity"], result[level]["Rt"][0] / 60, \
               result[level]["Rt"][-1] / 60
    else:
        return result
