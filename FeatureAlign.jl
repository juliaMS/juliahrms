__precompile__()

module FeatureAlign

#push!(LOAD_PATH,pwd())
using Base

using Plots
using CSV
using DataFrames
using Glob
using Statistics
using XLSX

export feature_alignment_wraper

#####
# Starting the Functions

###
# A function to import the files



function report_import(path2files,ext,source)
    if source == "Internal"
        nm=glob("*_report*."*ext,path2files)
        Reps=Dict()
        name=[]
        for i=1:size(nm,1)
            nf=splitdir(nm[i])
            nf1=split(nf[2],".")
            name=vcat(name,nf1[1])
            df = CSV.File(nm[i]) |> DataFrame
            Reps[nf1[1]]=df
            df=[]
        end
    end

    return(Reps,name)


end

#Reps,name=report_import(path2files,ext,source)

###
# Feature Creation

function feature_creation(Reps,name)
    features=vcat(Reps[name[1]],Reps[name[2]])

    sort!(features,[:MeasMass,:ScanNum])
    feature_li=convert(Matrix,features[:,2:8])
    feature_list=hcat(feature_li[:,1:3],feature_li[:,5:7])

    return feature_list

end

# feature_list=feature_creation(Reps,name)


###
# Master feature list creation

function master_feature_list(feature_list)

    uni_features=zeros(1,size(feature_list,2))
    for i=1:size(feature_list,1)
        tv1=feature_list[i,:]
        if tv1[1]>0
            tv2_m=feature_list[findall(x -> tv1[6]>=x>=tv1[5],feature_list[:,4]),:]
            tv3=mean(tv2_m[:,2])
            tv2_r=tv2_m[findall(x -> tv1[1]+tv3>=x>=tv1[1]-tv3,tv2_m[:,1]),:]
            ind_m=findall(x -> tv1[6]>=x>=tv1[5],feature_list[:,4])
            ind_r=findall(x -> tv1[1]+tv3>=x>=tv1[1]-tv3,tv2_m[:,1])
            f1=mean(tv2_r,dims=1)
            uni_features=vcat(uni_features,f1)
            feature_list[ind_m[ind_r],:]=zeros(length(ind_r),size(feature_list,2))
            #println(i)
        end
    end
    m_feature_list=uni_features[2:end,:]
    return m_feature_list

end

# m_feature_list=master_feature_list(feature_list)


##########################
# Feature find
# i=3

function feature_find(Reps,Name,m_feature_list)
    int=zeros(size(m_feature_list,1),1)
    area=zeros(size(m_feature_list,1),1)
    res=zeros(size(m_feature_list,1),1)
    file=convert(Matrix, Reps[Name])

    for i=1:size(m_feature_list,1)
        tv1=m_feature_list[i,:]
        tv2_m=file[findall(x -> tv1[6]>=x>=tv1[5],file[:,6]),:]
        tv2_r=tv2_m[findall(x -> tv1[1]+tv1[2]>=x>=tv1[1]-tv1[2],tv2_m[:,2]),:]


        if size(tv2_r,1)>1
            #println(i)

            int[i]=maximum(tv2_r[:,10])
            area[i]=sum(tv2_r[:,9])
            res[i]=mean(tv2_r[:,12])

        elseif size(tv2_r,1)==1
            #println(i)

            int[i]=tv2_r[10]
            area[i]=tv2_r[9]
            res[i]=tv2_r[12]
        end

    end

    return(int,area,res)


end


# int,area,res=feature_find(Reps,Name,m_feature_list)

###
# Alignment function

function feature_alignment(Reps,name,m_feature_list)
    Inten=zeros(size(m_feature_list,1),size(name,1))
    Area=zeros(size(m_feature_list,1),size(name,1))
    Ress=zeros(size(m_feature_list,1),size(name,1))

    for i=1:size(name,1)
        #i=9
        Name=name[i]
        int,area,res=feature_find(Reps,Name,m_feature_list)

        #scatter(area,int)


        Inten[:,i]=int
        Area[:,i]=area
        Ress[:,i]=res
        #scatter(Area[:,i],Inten[:,i])

    end
    table1=DataFrame(hcat(round.(m_feature_list[:,1]),m_feature_list[:,3:end]),[:AveScanNum,:AveRt,
    :AveMeasMass,:AveMinMass,:AveMaxMass])

    table_int=DataFrame(Inten)
    table_are=DataFrame(Area)
    table_res=DataFrame(Ress)

    names!(table_int,Symbol.(name))
    names!(table_are,Symbol.(name))
    names!(table_res,Symbol.(name))

    table_int_f=hcat(table1,table_int)
    table_are_f=hcat(table1,table_are)
    table_res_f=hcat(table1,table_res)

    sort!(table_int_f,[:AveRt,:AveMeasMass])
    sort!(table_are_f,[:AveRt,:AveMeasMass])
    sort!(table_res_f,[:AveRt,:AveMeasMass])

    return(table_int_f,table_are_f,table_res_f)

end

# table_int_f,table_are_f,table_res_f=feature_alignment(Reps,name,m_feature_list)
#########################################
function feature_alignment_wraper(path2files,ext,source)

    Reps,name=report_import(path2files,ext,source)

    println("All the files have been imported.")

    feature_list=feature_creation(Reps,name)

    println("All possible features are created.")

    m_feature_list=master_feature_list(feature_list)
    m=size(m_feature_list,1)
    #m=1000
    m1=string(m)
    println("A master feature list of "*m1 * " features has been created.")
    #int,area,res=feature_find(Reps,Name,m_feature_list)
    table_int_f,table_are_f,table_res_f=feature_alignment(Reps,name,m_feature_list)

    output=joinpath(path2files,"FeatureList_Aligned.xlsx")

    XLSX.writetable(output, Intensities=(collect(DataFrames.eachcol(table_int_f)),
     DataFrames.names(table_int_f)  ), Areas=(collect(DataFrames.eachcol(table_are_f)),
      DataFrames.names(table_are_f)  ), Resolutions=(collect(DataFrames.eachcol(table_res_f)),
       DataFrames.names(table_res_f)  ))


    println("The final report has been saved in the output path")

    return table_int_f

end

end #End of the module

####
# Test
#path2files="D:\\Data\\julia\\data_jake"
#ext="csv"
#source="Internal"

#@time feature_alignment_warper(path2files,ext,source)


#path2files="/home/saer/Desktop/juliaMS_data_tests/Files2align"
#ext="csv"
#source="Internal"
