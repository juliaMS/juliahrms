#########################################################################
## Put all the packages here

push!(LOAD_PATH,pwd())
using KMD
using CSV
using DataFrames



###########################################################################
## This is a file for running long scripts that are not generic enough to
# part of a module.
###########################################################################



#m_ru=[CO,CCl,CN,CS,CF,CH]
m_ru=[27.9949,46.9689,26.003,43.972,30.9984,13.0078]
file="C:\\Temp_File\\Comp_project\\iso_dist.csv"




function KMD_run(file,m_ru)

    data=CSV.read(file)
    iso_KMD_dist=-99 .* ones(size(data,1),47)
    for i=1:size(data,1)


        iso_m1=split(data.mz_iso[i][2:end-1],',')
        prob_m1=split(data.int_iso[i][2:end-1],',')
        if length(iso_m1)>1
            #exc_m=parse.(Float64,data.exact_mass[i])
            exc_m=data.exact_mass[i]
            iso_m=parse.(Float64,iso_m1)
            prob_m=parse.(Float64,prob_m1)
            ind=sortperm(prob_m,rev=true)
            if length(iso_m)>=5
                prob_m_s=prob_m[ind[1:5]]
                iso_m_s=iso_m[ind[1:5]]
            else
                prob_m_s=prob_m
                iso_m_s=iso_m

            end
            masses=zeros(1,length(prob_m_s[prob_m_s .>= 0])+1)
            masses[1]=exc_m
            masses[2:end]=iso_m_s[prob_m_s .>= 0]
            ind1=[[2,8,14,20,26,32],[7,13,19,25,31,37]]



            kmd=KMD_calc_ext(masses,m_ru)
            for j=1:length(kmd)
                iso_KMD_dist[i,1]=exc_m
                iso_KMD_dist[i,ind1[1][j]:ind1[2][j]]=vec(kmd[j][:])
                iso_KMD_dist[i,38:38+length(prob_m_s)-1]=vec(prob_m_s)


            end
            iso_KMD_dist[i,43:43+length(prob_m_s)-1]= exc_m .- masses[2:end]


        end
        println(i)


    end

    CSV.write("iso_kmd_dist_old.csv",DataFrame(iso_KMD_dist), writeheader=false)


end # function


##
# run the function

KMD_run(file,m_ru)

#i=2
#iso_KMD_dist[i,:]
# j=1

# data[1,6]
