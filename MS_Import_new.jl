
using Base

#using PyCall
using BenchmarkTools
using NCDatasets
using LightXML
using Unitful
using Codecs


cd("/Users/saersamanipour/Desktop/dev/ongoing/juliahrms/")
push!(LOAD_PATH,"/Users/saersamanipour/Desktop/dev/ongoing/juliahrms/")


#########################




function readCDF(File)
    DD=Dict()
    #File="D:\\Data\\julia\\Data_test_juliaHRMS\\Chrom_batch\\C1\\VANN-DANMARK01.CDF"
    ds = Dataset(File)

    scan_acquisition_time=ds["scan_acquisition_time"]
    scan_duration=ds["scan_duration"]                                           #The duration of each scan
    inter_scan_time=ds["inter_scan_time"]                                       #The time between scans
    total_intensity=ds["total_intensity"]                                       #TIC
    mass_range_min=ds["mass_range_min"]                                         #min mass range
    mass_range_max=ds["mass_range_max"]                                         #max mass range
    scan_index=ds["scan_index"]
    point_count=ds["point_count"]
    mass_values = ds["mass_values"]                                             #m/z values
    intensity_values=ds["intensity_values"]                                     #intensity at each m/z value

    #defining the starting point and the end point of the chromatogram
    t0=scan_acquisition_time[1]/60                                              #min
    t_end=scan_acquisition_time[end]/60                                         #min

    Max_Mz_displacement=maximum(point_count)
    Mz_values=zeros(length(scan_index),Max_Mz_displacement)
    Mz_intensity=zeros(length(scan_index),Max_Mz_displacement)

    for i=1:length(scan_index)-1
        if (scan_index[i+1] < size(mass_values,1))
            Mz_values[i,1:point_count[i]]=mass_values[scan_index[i]+1:scan_index[i+1]]
            Mz_intensity[i,1:point_count[i]]=intensity_values[scan_index[i]+1:scan_index[i+1]]
        else
            break
        end
        #println(i)
    end

    DD["total_intensity"]=total_intensity
    DD["t0"]=t0
    DD["t_end"]=t_end
    DD["Mz_values"]=Mz_values
    DD["Mz_intensity"]=Mz_intensity
    DD["scan_duration"]=scan_duration

    return DD


end



##############################################################################
## The warper function


function cdf_Import(pathin,filenames)
    chrom=Dict()
    for i=1:length(filenames)

        File=joinpath(pathin,filenames[i])
        chrom["MS$i"]=readCDF(File)

    end

    return chrom

end





#####################################################################
## Wrapper to import the files

function import_files(pathin,filenames,mz_thresh)

    m=split(filenames[1],".")

    if m[2] == "cdf" || m[2] == "CDF"

        chrom=cdf_Import(pathin,filenames)


    elseif m[2] == "mzxml" || m[2] == "mzXML" || m[2] == "MZXML"

        if isa(filenames,Array)==1
            path_in=joinpath(pathin,filenames[])
        else
            path_in=joinpath(pathin,filenames)
        end


        chrom = mzxml_read(path_in,mz_thresh)

    end

    return (chrom)

end




##########################################################################
# MS1 import

function import_files_MS1(pathin,filenames,mz_thresh)

    if isa(filenames,Array)==1
        m=split(filenames[1],".")
    else
        m=split(filenames,".")
    end


    if m[2] == "cdf" || m[2] == "CDF"

        chrom=cdf_Import(pathin,filenames)
        mz_vals=chrom["MS1"]["Mz_values"]
        mz_int=chrom["MS1"]["Mz_intensity"]
        t0=chrom["MS1"]["t0"][1]
        t_end=chrom["MS1"]["t_end"][1]
        msModel="NA"
        msIonisation="NA"
        msManufacturer="NA"
        polarity="NA"

    elseif m[2] == "mzxml" || m[2] == "mzXML" || m[2] == "MZXML"
        if isa(filenames,Array)==1
            path_in=joinpath(pathin,filenames[])
        else
            path_in=joinpath(pathin,filenames)
        end


        ch = mzxml_read(path_in,mz_thresh)
        ch1 = emp_scan_rm!(ch)
        chrom = ms2_align!(ch1)
        mz_vals=chrom["MS1"]["Mz_values"]
        mz_int=chrom["MS1"]["Mz_intensity"]
        t0=chrom["MS1"]["Rt"][1]
        t_end=chrom["MS1"]["Rt"][end]
        msModel=chrom["MS_Instrument"]["msModel"]
        msIonisation=chrom["MS_Instrument"]["msIonisation"]
        msManufacturer=chrom["MS_Instrument"]["msManufacturer"]
        polarity=chrom["MS1"]["Polarity"][1]

    end

    return (mz_vals,mz_int,t0,t_end,m,pathin,msModel,msIonisation,msManufacturer,polarity)

end

######################################################################
## Import function batch

function Import_files_batch_MS1(pathin,mz_thresh)


    Dname=readdir(pathin)
    ind=zeros(length(Dname),1)
# i=1
    for i=1:length(Dname)
        if Dname[i][end-4:end] == "mzXML" || Dname[i][end-4:end]== "mzxml" || Dname[i][end-4:end] == "MZXML"
            ind[i]=1
        elseif Dname[i][end-3:end] == "CDF" || Dname[i][end-3:end]== "cdf"
            ind[i]=1

        end

    end
    ind_s=findall(x -> x > 0,ind)
    mz_vales_mat=Array{Any}(undef,size(ind_s,1))
    mz_int_mat=Array{Any}(undef,size(ind_s,1))
    t0_mat=Array{Float64}(undef,size(ind_s,1))
    t_end_mat=Array{Float64}(undef,size(ind_s,1))
    m_mat=Array{Any}(undef,size(ind_s,1))
    pathin_mat=Array{String}(undef,size(ind_s,1))


    for i=1:size(ind_s,1)


        filenames=[Dname[ind_s[i]]]
        mz_vals,mz_int,t0,t_end,m,pathin=import_files_MS1(pathin,filenames,mz_thresh)
        mz_vales_mat[i]=mz_vals
        mz_int_mat[i]=mz_int
        t0_mat[i]=t0
        t_end_mat[i]=t_end
        m_mat[i]=m[end-1]
        pathin_mat[i]=pathin



    end


    return (mz_vales_mat,mz_int_mat,t0_mat,t_end_mat,m_mat,pathin_mat)

end


######################################################################
## Import function batch

function Import_files_batch(pathin,mz_thresh)


    Dname=readdir(pathin)
    ind=zeros(length(Dname),1)
# i=1
    for i=1:length(Dname)
        if Dname[i][end-4:end] == "mzXML" || Dname[i][end-4:end]== "mzxml" || Dname[i][end-4:end] == "MZXML"
            ind[i]=1
        elseif Dname[i][end-3:end] == "CDF" || Dname[i][end-3:end]== "cdf"
            ind[i]=1

        end

    end
    ind_s=findall(x -> x > 0,ind)
    chrom_mat=Array{Any,1}(undef,size(ind_s,1))



    for i=1:size(ind_s,1)


        filenames=[Dname[ind_s[i]]]
        ch=import_files(pathin,filenames,mz_thresh)
        ch1=emp_scan_rm!(ch)
        chrom_mat[i]=ms2_align!(ch1)



    end


    return (chrom_mat)

end




###############################################################################
# mzXML read

# path2mzxml=path_in

function mzxml_read(path2mzxml,mz_thresh)

    xdoc = parse_file(path2mzxml)
    xroot = root(xdoc)
    if LightXML.name(xroot) != "mzXML"
        error("Not an mzXML file")
    end
    # Find the msRun node
    msRun = find_element(xroot, "msRun")

    el_ins = find_element(msRun, "msInstrument")
    Ins = attribute(find_element(el_ins, "msModel"),"value")
    el_ion = attribute(find_element(el_ins,"msIonisation"),"value")
    el_man = attribute(find_element(el_ins,"msManufacturer"),"value")

    polarity,pre_mz,pre_mz_win,retentionTime,msLevel,basePeakMz,
    totIonCurrent,scan_type,centroided,mz,mz_int=read_scan(msRun,mz_thresh)
    chrom=chrom_fold(polarity,pre_mz,pre_mz_win,retentionTime,msLevel,basePeakMz,
        totIonCurrent,scan_type,centroided,mz,mz_int)

    MS_Instrument=Dict("msModel" => Ins, "msIonisation" => el_ion, "msManufacturer" => el_man)

    chrom["MS_Instrument"]=MS_Instrument

    return chrom

end # function



###############################################################################
# Reading the scans

function read_scan(msRun,mz_thresh)
    polarity=[]
    pre_mz=[]
    pre_mz_win=[]
    retentionTime=[]
    msLevel=[]
    basePeakMz=[]
    totIonCurrent=[]
    scan_type=[]
    centroided=[]
    mz=[]
    mz_int=[]

    for line in child_elements(msRun)
        if LightXML.name(line) == "scan"
            #println(line)
            polarity1,pre_mz1,pre_mz_win1,retentionTime1,msLevel1,basePeakMz1,
            totIonCurrent1,scan_type1,centroided1,mz1,I=read_scan_info(line,mz_thresh)

            polarity=vcat(polarity,polarity1)
            pre_mz=vcat(pre_mz,pre_mz1)
            pre_mz_win=vcat(pre_mz_win,pre_mz_win1)
            retentionTime=vcat(retentionTime,retentionTime1)
            msLevel=vcat(msLevel,msLevel1)
            basePeakMz=vcat(basePeakMz,basePeakMz1)
            totIonCurrent=vcat(totIonCurrent,totIonCurrent1)
            scan_type=vcat(scan_type,scan_type1)
            centroided=vcat(centroided,centroided1)
            mz=vcat(mz,[mz1])
            mz_int=vcat(mz_int,[I])
            #println(pre_mz_win)

        end

    end

    return(polarity,pre_mz,pre_mz_win,retentionTime,msLevel,basePeakMz,
    totIonCurrent,scan_type,centroided,mz,mz_int)

end # function


###############################################################################
# Extracting info from the scans

function read_scan_info(line,mz_thresh)

    precisiondict = Dict("32" => (Float32, Float32, Float32), "64" => (Float64, Float64, Float64))

    polarity = attribute(line, "polarity")

    mm=find_element(line,"precursorMz")

    if mm != nothing
        pre_mz=parse(Float64,content(mm))
        #println(pre_mz)
        if attribute(mm,"windowWideness") != nothing
            pre_mz_win =parse(Float64,attribute(mm,"windowWideness"))
        else
            pre_mz_win=0.0
        end
    else
        pre_mz=0
        pre_mz_win=0.0

    end

    tstr=attribute(line, "retentionTime")
    retentionTime = parse(Float64, tstr[3:end-1])
    msLevel = attribute(line, "msLevel")
    basePeakMz = parse(Float64, attribute(line, "basePeakMz"))
    totIonCurrent = parse(Float64, attribute(line, "totIonCurrent"))
    scan_type = attribute(line, "scanType")
    centroided = attribute(line,"centroided")
    peak = find_element(line, "peaks")
    data = decode(Base64, content(peak))
    TI, T, nochildren = precisiondict[attribute(peak, "precision")]
    A = reinterpret(TI, data)
    bo = attribute(peak, "byteOrder")
    if bo == "network"
        ntoh!(A)
    else
        error("Don't know what to do with byteOrder $bo")
    end
    I = A[2:2:end]

    mz = reinterpret(T, A[1:2:end])

    if mz_thresh[2]>0
        mz1=mz[findall(x -> mz_thresh[1]<= x <= mz_thresh[2], mz)]
        I1=I[findall(x -> mz_thresh[1]<= x <= mz_thresh[2], mz)]
    elseif mz_thresh[1] == 0
        mz1=mz[mz .>= mz_thresh[1]]
        I1=I[mz .>= mz_thresh[1]]

    end


    return(polarity,pre_mz,pre_mz_win,retentionTime,msLevel,basePeakMz,totIonCurrent,scan_type,centroided,mz1,I1)
end # function






##############################################################################
# XML encoding


function ntoh!(A)
    for i = 1:length(A)
        A[i] = ntoh(A[i])
    end
end



##############################################################################
# Chrom folding

function chrom_fold(polarity,pre_mz,pre_mz_win,retentionTime,msLevel,basePeakMz,
    totIonCurrent,scan_type,centroided,mz,mz_int)

    chrom=Dict()

    ms_levels=sort(unique(msLevel))
    par=["Mz_values","Mz_intensity",]

    for i=1:length(ms_levels)
        chrom["MS$i"] = Dict("Rt" => round.(retentionTime[msLevel .== ms_levels[i]]./60, digits=2),
        "Polarity" => polarity[msLevel .== ms_levels[i]],
        "PrecursorIon" => pre_mz[msLevel .== ms_levels[i]],
        "PrecursorIonWin" => pre_mz_win[msLevel .== ms_levels[i]],
        "BasePeak" => basePeakMz[msLevel .== ms_levels[i]],
        "TIC" => totIonCurrent[msLevel .== ms_levels[i]],
        "ScanType" => scan_type[msLevel .== ms_levels[i]],
        "Centroid" => centroided[msLevel .== ms_levels[i]],
        "Mz_values"=> Array{Float64,2}(undef,1,1),
        "Mz_intensity"=>Array{Float64,2}(undef,1,1))
        mz_s=mz[msLevel .== ms_levels[i]]
        mz_int_s=mz_int[msLevel .== ms_levels[i]]

        chrom["MS$i"]["Mz_values"]=array2mat(mz_s)
        chrom["MS$i"]["Mz_intensity"]=array2mat(mz_int_s)


    end

    return chrom

end # function



########################################################################
# Array to matrix
#

function array2mat(array)

    dummy_var=zeros(size(array,1),maximum(size.(array[1:end],1)))

    for i=1:size(array,1)
        dummy_var[i,1:length(array[i])]=array[i]
    end
    GC.gc()
    return dummy_var

end


#############################################################################
# Remove empty scans

function emp_scan_rm!(chrom)

    tv1=sum(chrom["MS1"]["Mz_intensity"], dims=2);
    chrom["MS1"]["Polarity"]=chrom["MS1"]["Polarity"][tv1[:] .> 0];
    chrom["MS1"]["TIC"]=chrom["MS1"]["TIC"][tv1[:] .> 0];
    chrom["MS1"]["Rt"]=chrom["MS1"]["Rt"][tv1[:] .> 0];
    chrom["MS1"]["BasePeak"]=chrom["MS1"]["BasePeak"][tv1[:] .> 0];
    chrom["MS1"]["PrecursorIon"]=chrom["MS1"]["PrecursorIon"][tv1[:] .> 0];
    chrom["MS1"]["Centroid"]=chrom["MS1"]["Centroid"][tv1[:] .> 0];
    chrom["MS1"]["PrecursorIonWin"]=chrom["MS1"]["PrecursorIonWin"][tv1[:] .> 0];
    chrom["MS1"]["ScanType"]=chrom["MS1"]["ScanType"][tv1[:] .> 0];
    chrom["MS1"]["Mz_values"]=chrom["MS1"]["Mz_values"][tv1[:] .> 0,:];
    chrom["MS1"]["Mz_intensity"]=chrom["MS1"]["Mz_intensity"][tv1[:] .> 0,:];

    return chrom


end # function



#############################################################################
# Removes the MS1 scans without MS2

function ms2_align!(chrom1)


    t_ms2=chrom1["MS2"]["Rt"][1]
    t_end_ms2=chrom1["MS2"]["Rt"][end]




    ind1=argmin(abs.(chrom1["MS1"]["Rt"] .- t_ms2))
    ind2=argmin(abs.(chrom1["MS1"]["Rt"] .- t_end_ms2))

    t_ms1=chrom1["MS1"]["Rt"][ind1]
    t_end_ms1=chrom1["MS1"]["Rt"][ind2]

    ind1_1=argmin(abs.(chrom1["MS2"]["Rt"] .- t_ms1))
    ind2_1=argmin(abs.(chrom1["MS2"]["Rt"] .- t_end_ms1))




    chrom1["MS1"]["Polarity"]=chrom1["MS1"]["Polarity"][ind1:ind2];
    chrom1["MS1"]["TIC"]=chrom1["MS1"]["TIC"][ind1:ind2];
    chrom1["MS1"]["Rt"]=chrom1["MS1"]["Rt"][ind1:ind2];
    chrom1["MS1"]["BasePeak"]=chrom1["MS1"]["BasePeak"][ind1:ind2];
    chrom1["MS1"]["PrecursorIon"]=chrom1["MS1"]["PrecursorIon"][ind1:ind2];
    chrom1["MS1"]["Centroid"]=chrom1["MS1"]["Centroid"][ind1:ind2];
    chrom1["MS1"]["PrecursorIonWin"]=chrom1["MS1"]["PrecursorIonWin"][ind1:ind2];
    chrom1["MS1"]["ScanType"]=chrom1["MS1"]["ScanType"][ind1:ind2];
    chrom1["MS1"]["Mz_values"]=chrom1["MS1"]["Mz_values"][ind1:ind2,:];
    chrom1["MS1"]["Mz_intensity"]=chrom1["MS1"]["Mz_intensity"][ind1:ind2,:];


    chrom1["MS2"]["Polarity"]=chrom1["MS2"]["Polarity"][ind1_1:ind2_1];
    chrom1["MS2"]["TIC"]=chrom1["MS2"]["TIC"][ind1_1:ind2_1];
    chrom1["MS2"]["Rt"]=chrom1["MS2"]["Rt"][ind1_1:ind2_1];
    chrom1["MS2"]["BasePeak"]=chrom1["MS2"]["BasePeak"][ind1_1:ind2_1];
    chrom1["MS2"]["PrecursorIon"]=chrom1["MS2"]["PrecursorIon"][ind1_1:ind2_1];
    chrom1["MS2"]["Centroid"]=chrom1["MS2"]["Centroid"][ind1_1:ind2_1];
    chrom1["MS2"]["PrecursorIonWin"]=chrom1["MS2"]["PrecursorIonWin"][ind1_1:ind2_1];
    chrom1["MS2"]["ScanType"]=chrom1["MS2"]["ScanType"][ind1_1:ind2_1];
    chrom1["MS2"]["Mz_values"]=chrom1["MS2"]["Mz_values"][ind1_1:ind2_1,:];
    chrom1["MS2"]["Mz_intensity"]=chrom1["MS2"]["Mz_intensity"][ind1_1:ind2_1,:];

    return chrom1

end # function



###################################
# testing area



# pathin="/Users/saersamanipour/Desktop/UvA/Temp_files/Phil/Drug cal/mzxml_files"
# filenames=["25PPB DRUGS.mzXML"]
# mz_thresh=[0,100]

# chrom_mat=Import_files_batch(pathin,mz_thresh)


# Import_files_batch_MS1(pathin,mz_thresh)

# mz_vals,mz_int,t0,t_end,m,pathin,msModel,msIonisation,msManufacturer
# ,polarity = import_files_MS1(pathin,filenames,mz_thresh)

# chrom=import_files(pathin,filenames,mz_thresh)
# unique(chrom["MS2"]["PrecursorIon"])



#chrom["MS_Instrument"]
