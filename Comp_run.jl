using CompCreate
using MS_Import
using JLD



path2features="/Users/saersamanipour/Desktop/dev/Data/AIMSMS-positive-VOSS-3-02_report.csv"

mass_win_per=0.8
ret_win_per=0.5
r_thresh=0.8
delta_mass=0.004
min_int=300


pathin="/Users/saersamanipour/Desktop/dev/Data"
filenames=["AIMSMS-positive-VOSS-3-02.mzXML"]
mz_thresh=[0,550]




ch=import_files(pathin,filenames,mz_thresh)
ch1=emp_scan_rm!(ch)
chrom=ms2_align!(ch1)

@time comp_DIA(chrom,path2features,mass_win_per,ret_win_per,r_thresh,delta_mass,min_int)
