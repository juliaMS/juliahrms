#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar  4 09:39:21 2020

@author: saer
"""

import pyopenms as pm
import pandas as pd
import numpy as np



##


def isodist(formula):

    try:
        FM=pm.EmpiricalFormula(formula)
        isotopes = FM.getIsotopeDistribution( pm.FineIsotopePatternGenerator(0.01) )
        #isotopes = FM.getIsotopeDistribution( pm.FineIsotopePatternGenerator(1e-3) )
        mz=[]
        prob=[]
        siz=[]
        for iso in isotopes.getContainer():
            mz.append(iso.getMZ())
            prob.append(iso.getIntensity())
            #siz.append(length())
            #print (iso.getMZ(), ":", iso.getIntensity())

        siz.append(len(mz))
    except:
        mz=[]
        prob=[]
        siz=[]


    return mz, prob, siz





def wrapper_isodist(path2metadata):
    m_data=pd.read_csv(path2metadata)

    mz=[]
    int=[]
    s=[]
    for i in range(np.size(m_data["CASRN"])):
        mmz,iint,siz=isodist(m_data["MS_Ready_Mol_Formula"][i])
        mz.append(mmz)
        int.append(iint)
        s.append(siz)
        print(i)

    data={'id': m_data["MS_Ready_DTXCID"],'formula': m_data["Preferred_Name"],
    'exact_mass': m_data["MS_Ready_Monoisotopic_Mass"],
     'mz_iso': mz,'int_iso': int, 'size': s}
    res=pd.DataFrame(data)
    res.to_csv(r'iso_dist.csv',index=False)
    return res










###################################################################



path2metadata="C:\\Temp_File\\Comp_project\\CFM-ID_metadata_DTXCID.csv"
res=wrapper_isodist(path2metadata)


#i=0
#formula=m_data["MS_Ready_Mol_Formula"][i]
#formula="C2H4O"
