push!(LOAD_PATH,pwd())

using KMD


#############################################################################
## Parameter setting

path2folder="/media/saer/Data/Data/1000Lakes/Reports"

m_ru=49.9968 # The accurate mass of the reapiting unite
kmd_bounds=[-0.25,0.1]
mass_tol=0.01


#############################################################################

KMD_filter_batch(kmd_bounds,mass_tol,m_ru,path2folder)
