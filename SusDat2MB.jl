

cd("/Users/saersamanipour/Desktop/dev/ongoing/juliahrms/")
push!(LOAD_PATH,"/Users/saersamanipour/Desktop/dev/ongoing/juliahrms/")

using CSV
using DataFrames
using Plots
using JLD
using XLSX

using Plots


###############################################################################
# Wrapper fucntion for parsing the spectra

function spec_ext_susdat(dat)

    spec_p=dat["Fragments Positive Ionization"];
    int_p=dat["Fragments Positive Ionization Intensity"];
    spec_n=dat["Fragments Negative Ionization"];
    int_n=dat["Fragments Negative Ionization Intensity"];
    ACCESSION=dat["Mol_Name"]
    NAME=dat["Name"]
    FORMULA=dat["Formula"]
    EXACT_MASS=dat["Monoiso_Mass"]
    SMILES=dat["SMILES"]
    INCHI=dat["Optimized_Std InChi"]
    CAS=dat["CAS_No"][9:end]
    PUBCHEM= "CDI:" * string(dat["PubChem_CID"])
    INCHIKEY= dat["Std InChIKey"]
    CHEMSPIDER= string(dat["ChemSpiderID"])
    MS_TYPE="MS2"
    IONIZATION="ESI"
    COLLISION_ENERGY="NA"
    SPLASH="NA"
    MZ_INT_REL="NA"
    RESOLUTION="NA"
    DATA_TYPE="EST"


    if length(spec_p)>0
        mz_p,mz_int_p=spec_parser(spec_p,int_p)
        ION_MODE_1="POSITIVE"
    else
        ION_MODE_1="NA"
        mz_p = []
        mz_int_p =[]

    end

    if length(spec_n)>0
        mz_n,mz_int_n=spec_parser(spec_n,int_n)
        ION_MODE_2="NEGATIVE"
    else
        ION_MODE_2="NA"
        mz_n = []
        mz_int_n =[]

    end

    return(ACCESSION,NAME,FORMULA,EXACT_MASS,SMILES,INCHI,CAS,PUBCHEM,INCHIKEY,
     CHEMSPIDER,MS_TYPE,IONIZATION,ION_MODE_1,ION_MODE_2,COLLISION_ENERGY,SPLASH,
     MZ_INT_REL,RESOLUTION,DATA_TYPE,mz_p,mz_int_p,mz_n,mz_int_n)

end # function



###############################################################################
## Spac parser

function spec_parser(spec,int)
    m=split(spec,"|")
    mz=Array{Any}(undef,length(m),1)
    mz_int=Array{Any}(undef,length(m),1)

    for i=1:length(m)
        if m[i] != "NA"

            mz[i]=parse.(Float64,split(m[i],":"))
            mz_int[i]=parse.(Float64,split(split(int,"|")[i],":"))
        end
    end

    return(mz,mz_int)
end # function


###############################################################################
#

# dat=susdat[i,:]

function mb_entry_create(dat)
    ACCESSION,NAME,FORMULA,EXACT_MASS,SMILES,INCHI,CAS,PUBCHEM,INCHIKEY,
     CHEMSPIDER,MS_TYPE,IONIZATION,ION_MODE_1,ION_MODE_2,COLLISION_ENERGY,SPLASH,
     MZ_INT_REL,RESOLUTION,DATA_TYPE,mz_p,mz_int_p,mz_n,mz_int_n = spec_ext_susdat(dat)

     if isassigned(mz_n) ==1 && isassigned(mz_p) ==1
         n=length(mz_p)+length(mz_n)
     elseif isassigned(mz_n) ==1 && isassigned(mz_p) ==0
         n=length(mz_n)
     elseif isassigned(mz_n) ==0 && isassigned(mz_p) ==1
         n=length(mz_p)

     end


     access=Array{Any}(undef,n)
     name=Array{Any}(undef,n)
     formula=Array{Any}(undef,n)
     mass=Array{Float64}(undef,n)
     smiles=Array{Any}(undef,n)
     inchi=Array{Any}(undef,n)
     cas=Array{Any}(undef,n)
     pubchem=Array{Any}(undef,n)
     inchikey=Array{Any}(undef,n)
     chemspider=Array{Any}(undef,n)
     ms_type=Array{Any}(undef,n)
     ion_s=Array{Any}(undef,n)
     ion_m=Array{Any}(undef,n)
     coll_e=Array{Any}(undef,n)
     res=Array{Any}(undef,n)
     splash= Array{Any}(undef,n)
     mz_val=Array{Any}(undef,n)
     mz_int=Array{Any}(undef,n)
     mz_int_r=Array{Any}(undef,n)
     d_type=Array{Any}(undef,n)

     access .= ACCESSION
     name .=NAME
     formula .=FORMULA
     mass .=EXACT_MASS
     smiles .=SMILES
     inchi .=INCHI
     cas .=CAS
     pubchem .=PUBCHEM
     inchikey .=INCHIKEY
     chemspider .=CHEMSPIDER
     ms_type .=MS_TYPE
     ion_s .=IONIZATION
     coll_e .=COLLISION_ENERGY
     res .=RESOLUTION
     splash .= SPLASH
     d_type .= DATA_TYPE
     mz_int_r .=MZ_INT_REL

     if isassigned(mz_n) ==1 && isassigned(mz_p) ==1

         ion_m[1:length(mz_p)].=ION_MODE_1
         ion_m[length(mz_p)+1:end].=ION_MODE_2

         mz_val[1:length(mz_p)]=mz_p
         mz_val[length(mz_p)+1:end]=mz_n
         mz_int[1:length(mz_p)]=mz_int_p
         mz_int[length(mz_p)+1:end]=mz_int_n
     elseif isassigned(mz_n) ==1 && isassigned(mz_p) ==0
         ion_m .= ION_MODE_2
         mz_val=mz_n
         mz_int=mz_int_n
     elseif isassigned(mz_n) ==0 && isassigned(mz_p) ==1
         ion_m .= ION_MODE_1
         mz_val=mz_p
         mz_int=mz_int_p

     else
         println(ACCESSION)
     end

     return(access,name,formula,mass,smiles,inchi,cas,pubchem,inchikey,
      chemspider,ms_type,ion_s,ion_m,coll_e,splash,mz_val,mz_int,mz_int_r,res,
      d_type)

end # function


###############################################################################
#

function mb_entry_comb(susdat)
    n=size(susdat,1)
    access=Array{Any}(undef,n*6)
    name=Array{Any}(undef,n*6)
    formula=Array{Any}(undef,n*6)
    mass=zeros(1,n*6)
    smiles=Array{Any}(undef,n*6)
    inchi=Array{Any}(undef,n*6)
    cas=Array{Any}(undef,n*6)
    pubchem=Array{Any}(undef,n*6)
    inchikey=Array{Any}(undef,n*6)
    chemspider=Array{Any}(undef,n*6)
    ms_type=Array{Any}(undef,n*6)
    ion_s=Array{Any}(undef,n*6)
    ion_m=Array{Any}(undef,n*6)
    coll_e=Array{Any}(undef,n*6)
    res=Array{Any}(undef,n*6)
    splash= Array{Any}(undef,n*6)
    mz_val=Array{Any}(undef,n*6)
    mz_int=Array{Any}(undef,n*6)
    mz_int_r=Array{Any}(undef,n*6)
    d_type=Array{Any}(undef,n*6)
    nn=1:6:n*6
    c=zeros(1,n*6)

    for i=1:n

        access1,name1,formula1,mass1,smiles1,inchi1,cas1,pubchem1,inchikey1,
         chemspider1,ms_type1,ion_s1,ion_m1,coll_e1,splash1,mz_val1,mz_int1,mz_int_r1,res1,
         d_type1=mb_entry_create(susdat[i,:])



        println(name1[1])

        access[nn[i]:nn[i]+length(mz_val1)-1]=access1
        name[nn[i]:nn[i]+length(mz_val1)-1]=name1
        formula[nn[i]:nn[i]+length(mz_val1)-1]=formula1
        mass[nn[i]:nn[i]+length(mz_val1)-1]=mass1
        smiles[nn[i]:nn[i]+length(mz_val1)-1]=smiles1
        inchi[nn[i]:nn[i]+length(mz_val1)-1]=inchi1
        cas[nn[i]:nn[i]+length(mz_val1)-1]=cas1
        pubchem[nn[i]:nn[i]+length(mz_val1)-1]=pubchem1
        inchikey[nn[i]:nn[i]+length(mz_val1)-1]=inchikey1
        chemspider[nn[i]:nn[i]+length(mz_val1)-1]=chemspider1
        ms_type[nn[i]:nn[i]+length(mz_val1)-1]=ms_type1
        ion_s[nn[i]:nn[i]+length(mz_val1)-1]=ion_s1
        ion_m[nn[i]:nn[i]+length(mz_val1)-1]=ion_m1
        coll_e[nn[i]:nn[i]+length(mz_val1)-1]=coll_e1
        res[nn[i]:nn[i]+length(mz_val1)-1]=res1
        splash[nn[i]:nn[i]+length(mz_val1)-1]=splash1
        mz_val[nn[i]:nn[i]+length(mz_val1)-1]=mz_val1
        mz_int[nn[i]:nn[i]+length(mz_val1)-1]=mz_int1
        mz_int_r[nn[i]:nn[i]+length(mz_val1)-1]=mz_int_r1
        d_type[nn[i]:nn[i]+length(mz_val1)-1]=d_type1

    end



    return(access[mass[:] .> 0],name[mass[:] .> 0],formula[mass[:] .> 0],mass[mass[:] .> 0],
    smiles[mass[:] .> 0],inchi[mass[:] .> 0],cas[mass[:] .> 0],pubchem[mass[:] .> 0],inchikey[mass[:] .> 0],
     chemspider[mass[:] .> 0],ms_type[mass[:] .> 0],ion_s[mass[:] .> 0],ion_m[mass[:] .> 0],
     coll_e[mass[:] .> 0],splash[mass[:] .> 0],mz_val[mass[:] .> 0],mz_int[mass[:] .> 0],
     mz_int_r[mass[:] .> 0],res[mass[:] .> 0],d_type[mass[:] .> 0])

end # function



###############################################################################
##

function add_susdat2mb(path2susdat,MB)

    susdat = DataFrame(XLSX.readtable(path2susdat, "SusDatPredictedFragments")...)
    println("SusDat is loaded.")

    access,name,formula,mass,smiles,inchi,cas,pubchem,inchikey,
     chemspider,ms_type,ion_s,ion_m,coll_e,splash,mz_val,mz_int,mz_int_r,res,
     d_type=mb_entry_comb(susdat)


    MassBankJulia=Dict("ACCESSION" => [MB["ACCESSION"][:];access] , "NAME"=>[MB["NAME"][:];name],
     "FORMULA"=>[MB["FORMULA"][:];formula],"EXACT_MASS"=>[MB["EXACT_MASS"];mass],
     "SMILES"=>[MB["SMILES"][:];smiles],"INCHI"=>[MB["INCHI"][:];inchi],"CAS"=>[MB["CAS"][:];cas],
     "PUBCHEM"=>[MB["PUBCHEM"][:];pubchem],"INCHIKEY"=>[MB["INCHIKEY"][:];inchikey],
     "CHEMSPIDER"=>[MB["CHEMSPIDER"][:];chemspider],"MS_TYPE"=>[MB["MS_TYPE"][:];ms_type],
     "IONIZATION"=>[MB["IONIZATION"][:];ion_s],"ION_MODE"=> [MB["ION_MODE"][:];ion_m],
     "COLLISION_ENERGY"=>[MB["COLLISION_ENERGY"][:];coll_e], "SPLASH"=> [MB["SPLASH"][:];splash],
    "MZ_VALUES"=> [MB["MZ_VALUES"][:];mz_val],"MZ_INT"=>[MB["MZ_INT"][:];mz_int],
    "MZ_INT_REL"=>[MB["MZ_INT_REL"][:];mz_int_r],"RESOLUTION"=>[MB["RESOLUTION"][:];res],
     "DATA_TYPE"=>[MB["DATA_TYPE"][:];d_type])

     return MassBankJulia
end # function



###############################################################################
# Test area

path2susdat="SusDatPredictedFragments.xlsx"

MB=load("MassBankJulia.jld","MassBankJulia")
println("Current MassBank is loaded.")
MassBankJulia=add_susdat2mb(path2susdat,MB)
println("MassBank is updated.")

jldopen("MassBankJulia.jld", "w") do file
    write(file, "MassBankJulia", MassBankJulia)  # alternatively, say "@write file A"
end
println("The updated MassBank is saved.")
#MassBankJulia["CAS"]
