using PyCall

py"""
import numpy as np
import pyopenms as PM
import scipy.io as sio
import os
import glob





def mzxml_import(path,num_channel,MS_level,mz_thresh):
    data={}
    Mz_Values=[]
    Mz_Intensity=[]

    if (num_channel==1 and MS_level==1):
        MS1={}
        Rt = []
        exp = PM.MSExperiment()
        PM.MzXMLFile().load(path,exp)
        m=exp.getNrSpectra()
        s=[]


        for i in range(0,m):
            spec=exp[i]
            if (mz_thresh==0):
                Mz, Int=spec.get_peaks()
                Mz_Values.append(Mz)
                Mz_Intensity.append(Int)
                s.append(len(Mz))
                Rt.append(spec.getRT())
            else:
                Mz, Int=spec.get_peaks()
                Mz_Values.append(Mz[Mz<=mz_thresh])
                Mz_Intensity.append(Int[Mz<=mz_thresh])
                s.append(len(Mz[Mz<=mz_thresh]))
                Rt.append(spec.getRT())

        mm=np.max(s)
        Mz_values=np.zeros((m,mm))
        Mz_intensity=Mz_values.copy()

        for j in range(0,m):
            Mz_values[j,0:len(Mz_Values[j])]=Mz_Values[j]
            Mz_intensity[j,0:len(Mz_Values[j])]=Mz_Intensity[j]

        MS1["Mz_values"]=Mz_values
        MS1["Mz_intensity"]=Mz_intensity
        MS1["Rt"]=Rt
        data["MS1"]=MS1

    elif (num_channel==1 and MS_level>1):
        exp = PM.MSExperiment()
        #path =path_in
        PM.MzXMLFile().load(path,exp)
        m=exp.getNrSpectra()
        #l=1
        for l in range(MS_level):
            s=[]
            Rt=[]
            var='MS'+str(l+1)
            Mz_Values=[]
            Mz_Intensity=[]
            #i=1
            for i in range(0,m):
                spec=exp[i]
                if (spec.getMSLevel()==l+1):
                    if (mz_thresh==0):
                        Mz, Int=spec.get_peaks()
                        Mz_Values.append(Mz)
                        Mz_Intensity.append(Int)
                        s.append(len(Mz))
                        Rt.append(spec.getRT())
                    else:
                        Mz, Int=spec.get_peaks()
                        Mz_Values.append(Mz[Mz<=mz_thresh])
                        Mz_Intensity.append(Int[Mz<=mz_thresh])
                        s.append(len(Mz[Mz<=mz_thresh]))
                        Rt.append(spec.getRT())



            mm=np.max(s)
            Mz_values=np.zeros((np.size(Mz_Values,0),mm))
            Mz_intensity=Mz_values.copy()
#j=100

            for j in range(0,np.size(Mz_Values,0)):
                #print(j)
                Mz_values[j,0:len(Mz_Values[j])]=Mz_Values[j]
                Mz_intensity[j,0:len(Mz_Values[j])]=Mz_Intensity[j]
                #np.size(Mz_Values,1)

            data[var]={}
            data[var]['Mz_values']=Mz_values
            data[var]["Mz_intensity"]=Mz_intensity
            data[var]["Rt"]=Rt

    elif (num_channel>1 and MS_level>1):
        print("Please make sure the MS1 is first path, MS2 the second and so on")

        for l in range(num_channel):
            exp = PM.MSExperiment()
            PM.MzXMLFile().load(path[l],exp)
            m=exp.getNrSpectra()
            s=[]
            Rt=[]
            var='MS'+str(l+1)
            for i in range(0,m):
                spec=exp[i]
                if (mz_thresh==0):
                    Mz, Int=spec.get_peaks()
                    Mz_Values.append(Mz)
                    Mz_Intensity.append(Int)
                    s.append(len(Mz))
                    Rt.append(spec.getRT())
                else:
                    Mz, Int=spec.get_peaks()
                    Mz_Values.append(Mz[Mz<=mz_thresh])
                    Mz_Intensity.append(Int[Mz<=mz_thresh])
                    s.append(len(Mz[Mz<=mz_thresh]))
                    Rt.append(spec.getRT())

            mm=np.max(s)
            Mz_values=np.zeros((m,mm))
            Mz_intensity=Mz_values.copy()

            for j in range(0,m):
                Mz_values[j,0:len(Mz_Values[j])]=Mz_Values[j]
                Mz_intensity[j,0:len(Mz_Values[j])]=Mz_Intensity[j]

                data[var]={}
                data[var]['Mz_values']=Mz_values
                data[var]["Mz_intensity"]=Mz_intensity
                data[var]["Rt"] = Rt
            print(var+" is successfully imported!")

    else:
         print("One or more parameters are wrong. Please check them again!")

    return data


"""


#####

py"""
import numpy as np
import pyopenms as PM
import scipy.io as sio
import os
import glob

def mzMXL_DIA_Import(path_in,path_out,num_channel,MS_level):
    N=os.path.splitext(os.path.basename(path_in)) #this will be the name of the final file
    Name=N[0]
    Loc=path_out+"\\"+Name+".mat"
    data={}
    data=mzxml_import(path_in,num_channel,MS_level)
    return(data)
    sio.savemat(Loc,data)
"""

###
# Variables
path_in="C:\\Temp_File\\NA_raw_Data\\SSA_2404019_NAs_11AA.mzXML"
path_out="C:\\Temp_File\\NA_raw_Data"
num_channel = 1
MS_level = 2
mz_thresh=900

data= py"mzxml_import"(path_in,num_channel,MS_level,mz_thresh)






#################################

py"""
import numpy as np

def sinpi(x):
    return np.sin(np.pi * x)
"""
py"sinpi"(1)
