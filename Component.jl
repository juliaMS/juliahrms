
#module Component

##############################################################################
# packages

cd("/Users/saersamanipour/Desktop/dev/ongoing/juliahrms/")
push!(LOAD_PATH,"/Users/saersamanipour/Desktop/dev/ongoing/juliahrms/")

using CSV
using MS_Import
using KMD
using DataFrames
using Plots
using Statistics
using JLD


##############################################################################
#

function xic_gen(ms_val,ms_int,feature)

    xic=zeros(size(ms_val,1),1)

    for j=1:length(xic)
        ind=findall(x -> feature[:MaxMass]>= x >= feature[:MinMass],ms_val[j,:])
        if length(ind)>0
            ms_val[j,ind]
            xic[j]=maximum(ms_int[j,ind])
        end
        #println(j)

    end

    return xic

end # function

# j=1
# plot(xic)

# plot(ms_val[11,:],ms_int[11,:])

##############################################################################
# Correlation matrix generation MS1

function corr_mat_ms1(chrom,sel_features,r_tresh,feature)


    ret_win=maximum(sel_features[:,:ScanInPeak])/2

    if floor(feature[:ScanNum]-ret_win) > 0 && ceil(feature[:ScanNum]+ret_win) <= size(chrom["MS1"]["Mz_values"],1)

        ms_val=chrom["MS1"]["Mz_values"][convert(Int32,floor(feature[:ScanNum]-ret_win)):convert(Int32,ceil(feature[:ScanNum]+ret_win)),:]
        ms_int=chrom["MS1"]["Mz_intensity"][convert(Int32,floor(feature[:ScanNum]-ret_win)):convert(Int32,ceil(feature[:ScanNum]+ret_win)),:]
    elseif floor(feature[:ScanNum]-ret_win) <= 0
        ms_val=chrom["MS1"]["Mz_values"][1:convert(Int32,ceil(feature[:ScanNum]+ret_win)),:]
        ms_int=chrom["MS1"]["Mz_intensity"][1:convert(Int32,ceil(feature[:ScanNum]+ret_win)),:]
    elseif ceil(feature[:ScanNum]+ret_win) > size(chrom["MS1"]["Mz_values"],1)
        ms_val=chrom["MS1"]["Mz_values"][convert(Int32,floor(feature[:ScanNum]-ret_win)):end,:]
        ms_int=chrom["MS1"]["Mz_intensity"][convert(Int32,floor(feature[:ScanNum]-ret_win)):end,:]
    end


    corr_mat=zeros(size(ms_val,1),size(sel_features,1)+1)
    corr_mat[:,1]=xic_gen(ms_val,ms_int,feature)

    # i=12
    # plot(xic_gen(ms_val,ms_int,feature))
    # plot!(xic_gen(ms_val,ms_int,sel_features[i-1,:]))

    #plot(corr_mat)
    for i=2:size(sel_features,1)+1
        corr_mat[:,i]=sma(xic_gen(ms_val,ms_int,sel_features[i-1,:]),3)
        #println(i)

    end
    return corr_mat

end # function

##############################################################################
# Correlation checking

function corr_check(corr_mat,r_tresh)

    r=cor(corr_mat,dims=1)
    r1=round.(r[2:end,1];digits=2)

    r_ind=findall(x -> x >=r_tresh,r1)
    return r_ind
end # function



##############################################################################
# Adduct finding

function adduct_find(pr,sel_features_cor,mode,mass_win_per,add_m)

    if mode == "POSITIVE"
        mass=sel_features_cor.MeasMass[sel_features_cor.Nr .== pr][1]- 1.007825
    elseif mode == "NEGATIVE"
        mass=sel_features_cor.MeasMass[sel_features_cor.Nr .== pr][1] + 1.007825
    end

    dmass=sel_features_cor.MeasMass .- mass
    mass_tol=mass_win_per*(sel_features_cor.MaxMass[sel_features_cor.Nr .== pr] -
    sel_features_cor.MinMass[sel_features_cor.Nr .== pr])/2

    x=zeros(length(dmass),1)
    add=zeros(length(dmass),1)

    for i=1:length(dmass)
        #println(i)
        err=abs.(abs(dmass[i]) .- add_m)
        tv=findall(x -> x <= mass_tol[1], err)
        if length(tv)>0
            x[i]=1
            add[i]=add_m[tv[1]]
        end
    end

    return(x,add,mass)
end # function


# i=2
##############################################################################
# finding isotopes

function iso_find(pr,sel_features_cor,delta_mass,ru_m)
    mass=sel_features_cor.MeasMass[sel_features_cor.Nr .== pr]
    # m_ru=[27.9949,46.9689,26.003,43.972,30.9984,13.0078]
    x=zeros(size(sel_features_cor,1),1)
    kmd_p=KMD_calc_ext(mass,ru_m["ru_m"])



    for i=1:size(sel_features_cor,1)
        kmd_temp=KMD_calc_ext(sel_features_cor.MeasMass[i],ru_m["ru_m"])
        m_tol=0.2*(sel_features_cor.MaxMass[i]-sel_features_cor.MinMass[i])
        if round(median(abs.(kmd_p[1] .- kmd_temp[1])),digits=3) <= delta_mass + m_tol
            x[i]=1
        end

    end

    return x

end # function

# i=1

##############################################################################
# finding in-source fragments

function in_source_frag_find(pr,sel_features_cor,NL)
    x=zeros(size(sel_features_cor,1),1)
    mass_err=mass_win_per*(sel_features_cor.MaxMass[sel_features_cor.Nr .== pr] -
    sel_features_cor.MinMass[sel_features_cor.Nr .== pr])/2
    mass=sel_features_cor.MeasMass[sel_features_cor.Nr .== pr]
    n_ls=mass .- sel_features_cor.MeasMass

    for i=1:size(sel_features_cor,1)
        if n_ls[i] == 0
            x[i]=1

        elseif n_ls[i] > 0 && length(findall(abs.(n_ls[i] .- NL["mass"]) .<= mass_err))>0 &&
            NL["prob"][argmin(abs.(n_ls[i] .- NL["mass"]))] >= 0.05
            x[i]=1

        end
    end

    return x

end # function


##############################################################################
# finding MS1 Component

function ms1_comp_finder(feature,feature_list,chrom,ret_win_per,r_tresh,
    delta_mass,add_m,ru_m,NL,mode)

    ret_win=ceil(feature[:ScanInPeak]*ret_win_per)
    ind_parent=[]
    ind_ms1_comp=[]
    #scan_num=feature[:ScanNum]
    if ret_win >= feature[:ScanNum]
        sel_features=feature_list[findall(x -> feature[:ScanNum]+ret_win >= x >= 1 ,feature_list[:,:ScanNum]),:]
    else
        sel_features=feature_list[findall(x -> feature[:ScanNum]+ret_win >= x >= feature[:ScanNum]-ret_win
        ,feature_list[:,:ScanNum]),:]
    end

    # plot(corr_mat)
    corr_mat=corr_mat_ms1(chrom,sel_features,r_tresh,feature)
    r_ind=corr_check(corr_mat,r_tresh)
    sel_features_cor=sel_features[r_ind[abs.(r_ind) .>= r_tresh],:]
    ind_comp=zeros(size(sel_features_cor,1),3);
    if size(sel_features_cor,1)>2 && minimum(abs.(sel_features_cor.Nr .- feature.Nr))==0

        pr=feature.Nr
        ind_comp[:,1],add,mass=adduct_find(pr,sel_features_cor,mode,mass_win_per,add_m)
        ind_comp[:,2]=iso_find(pr,sel_features_cor,delta_mass,ru_m)
        ind_comp[:,3]=in_source_frag_find(pr,sel_features_cor,NL)

        ind_parent=sel_features_cor.Nr[findall(x -> x == 3, sum(ind_comp,dims=2))]

        if length(ind_parent)>1
            ind_parent=[feature.Nr]
        end

        ind_ms1_comp=sel_features_cor.Nr[findall(x -> 0 < x < 3, sum(ind_comp,dims=2))]
    else
        ind_parent=[feature.Nr]
        ind_ms1_comp=sel_features_cor.Nr[findall(x -> 0 < x < 3, sum(ind_comp,dims=2))]
        mass="NA"
    end

    return (ind_parent,ind_ms1_comp,mass)

end # function

#sel_features_cor.Nr[2]

##############################################################################
# removing aducts, iso, frag

function rm_ms1!(ind_parent,ind_ms1_comp,mass,decon_tab,feature_list)

    decon_tab[ind_parent,1:size(feature_list,2)]=Matrix(feature_list[ind_parent,:])
    if length(ind_ms1_comp)>0
        decon_tab[ind_parent[1],13]=1.0
        decon_tab[ind_parent[1],14]=mass
        decon_tab[ind_parent[1],size(feature_list,2)+3]=string(feature_list.MeasMass[ind_ms1_comp])
        decon_tab[ind_parent[1],size(feature_list,2)+4]=string(feature_list.Int[ind_ms1_comp])

        feature_list.ScanNum[ind_ms1_comp] .= 0.0

    else
        decon_tab[ind_parent[1],13]=0
        decon_tab[ind_parent[1],14]="NA"
        decon_tab[ind_parent[1],size(feature_list,2)+3]=0
        decon_tab[ind_parent[1],size(feature_list,2)+4]=0
    end

    return(decon_tab,feature_list)

end # function


#decon_tab[ind_parent[1],:]


################################
# Signal selection SWATH

function sig_select_SWATH(chrom,timeWin,ind,SWATH_n,massWin)
    MS1val=chrom["MS1"]["Mz_values"][timeWin[1]:timeWin[2],:]
    MS1int=chrom["MS1"]["Mz_intensity"][timeWin[1]:timeWin[2],:]

    # plot(MS1val[28,:],MS1int[28,:])

    ms1val=zeros(size(MS1val,1),100)
    ms1int=zeros(size(MS1val,1),100)

    for i=1:size(MS1val,1)
        tv1=MS1val[i,findall(x -> massWin[2] > x >= massWin[1],MS1val[i,:])]
        if length(tv1)>0
            ms1val[i,1:length(tv1)]=tv1
            ms1int[i,1:length(tv1)]=MS1int[i,findall(x -> massWin[2] > x >= massWin[1],MS1val[i,:])]

        end

    end

    # scatter(ms1val[28,:])


    c=range(1,stop=(SWATH_n)*(size(chrom["MS1"]["Mz_values"],1)-1),step=SWATH_n)

    #c=range(1,stop=(size(chrom["MS2"]["Mz_values"],1)-(ind-2)),step=SWATH_n-1)


    MS2val=chrom["MS2"]["Mz_values"][c .+ (ind-1),:]
    MS2int=chrom["MS2"]["Mz_intensity"][c .+ (ind-1),:]

    # plot(ms2val[28,:],ms2int[28,:])

    ms2val=MS2val[timeWin[1]:timeWin[2],:]
    ms2int=MS2int[timeWin[1]:timeWin[2],:]

    # plot(ms2val[47,:],ms2int[47,:])
    # plot!(MS2val[236,:],MS2int[236,:])

    return(ms1val,ms1int,ms2val,ms2int)

end



################################
# Signal selection DIA


function sig_select_DIA(chrom,timeWin,massWin)

    MS1val=chrom["MS1"]["Mz_values"][timeWin[1]:timeWin[2],:]
    MS1int=chrom["MS1"]["Mz_intensity"][timeWin[1]:timeWin[2],:]

    ms1val=zeros(size(MS1val,1),100)
    ms1int=zeros(size(MS1val,1),100)

    for i=1:size(MS1val,1)
        tv1=MS1val[i,findall(x -> massWin[2] > x >= massWin[1],MS1val[i,:])]
        if length(tv1)>0
            ms1val[i,1:length(tv1)]=tv1
            ms1int[i,1:length(tv1)]=MS1int[i,findall(x -> massWin[2] > x >= massWin[1],MS1val[i,:])]

        end

    end


    ms2val=chrom["MS2"]["Mz_values"][timeWin[1]:timeWin[2],:]
    ms2int=chrom["MS2"]["Mz_intensity"][timeWin[1]:timeWin[2],:]

    return(ms1val,ms1int,ms2val,ms2int)

end


################################
# Signal selection DIA mc


function sig_select_DIA_cl(chrom,timeWin,massWin)

    MS1val=chrom["MS1"]["Mz_values"][timeWin[1]:timeWin[2],:]
    MS1int=chrom["MS1"]["Mz_intensity"][timeWin[1]:timeWin[2],:]

    ms1val=zeros(size(MS1val,1),100)
    ms1int=zeros(size(MS1val,1),100)

    for i=1:size(MS1val,1)
        tv1=MS1val[i,findall(x -> massWin[2] > x >= massWin[1],MS1val[i,:])]
        if length(tv1)>0
            ms1val[i,1:length(tv1)]=tv1
            ms1int[i,1:length(tv1)]=MS1int[i,findall(x -> massWin[2] > x >= massWin[1],MS1val[i,:])]

        end

    end


    c=round(length(chrom["MS2"]["Rt"])/length(chrom["MS1"]["Rt"]))

    # i=2
    ms2val=Array{Any}(undef,Int(c))
    ms2int=Array{Any}(undef,Int(c))

    for i=1:Int(c)
        mz=chrom["MS2"]["Mz_values"][Int.(i:c:size(chrom["MS2"]["Mz_values"],1)),:]
        mz_int=chrom["MS2"]["Mz_intensity"][Int.(i:c:size(chrom["MS2"]["Mz_values"],1)),:]
        ms2val[i]=mz[timeWin[1]:timeWin[2],:]
        ms2int[i]=mz_int[timeWin[1]:timeWin[2],:]

    end

    return(ms1val,ms1int,ms2val,ms2int)

end


#########################################################################
# XIC generator

function xic_gen_ms2!(mass2,mass_tol,ms2val,ms2int)

    ms2_val_sel=zeros(size(ms2int,1),1)
    ms2_int_sel=zeros(size(ms2int,1),1)
    ms2_int_out=copy(ms2int)

    for i=1:size(ms2int,1)
        #println(i)
        tv1 = ms2val[i,findall(x -> mass2+mass_tol > x >= mass2-mass_tol,ms2val[i,:])]
        if length(tv1)>0
            #println(i)
            ms2_val_sel[i,1]=median(tv1)
            ms2_int_sel[i,1]=maximum(ms2int[i,findall(x -> mass2+mass_tol >= x >= mass2-mass_tol,ms2val[i,:])])
            ms2_int_out[i,findall(x -> mass2+mass_tol >= x >= mass2-mass_tol,ms2val[i,:])].=0
        end


    end
    return ms2_val_sel,ms2_int_sel,ms2_int_out

end


#plot(ms2_int_sel)

#i=2
#########################################################################
# Correlation matrix generator

function corrlation_mat_gen(ms1int,ms1val,ms2int,ms2val,min_int,mass_tol)

    val,loc=findmax(ms1int)
    xic=maximum(ms1int,dims=2)

    ind_ap=convert(Int,floor(size(ms2int,1)/2)+1)


    nzeroind=findall(x -> x >= min_int, ms2int[loc[1],:])

    # plot(ms2val[loc[1],:],ms2int[loc[1],:])
    # i=1
    corr_mat=zeros(size(ms2int,1),length(nzeroind)+1)
    frags=zeros(length(nzeroind)+1)
    corr_mat[:,1]=sma(xic,5)
    frags[1]=mean(ms1val[ms1val .> 0])

    for i=1:length(nzeroind)
        #println(i)
        val2,loc2=findmax(ms2int[loc[1],:])

        mass2=ms2val[loc[1],loc2]
        #println([i,mass2])

        ms2_val_sel,ms2_int_sel,ms2_int_out=xic_gen_ms2!(mass2,mass_tol,ms2val,ms2int);
        # plot(ms2_val_sel,ms2_int_sel)
        ap_ms2_int_sel=ms2_int_sel[ind_ap-1:ind_ap+1]
        if maximum(ms2_int_sel[ind_ap-1:ind_ap+1])>=min_int && length(ap_ms2_int_sel[ap_ms2_int_sel .>0])>1
            frags[i+1]=median(ms2_val_sel[ms2_val_sel .> 0]);
            corr_mat[:,i+1]=sma(maximum(ms2_int_sel,dims=2),5);
            ms2int=ms2_int_out;

        elseif maximum(ms2_int_sel[ind_ap-1:ind_ap+1]) < min_int
            break
        else

            ms2int=ms2_int_out;
        end




    end

    return(corr_mat,frags)
end

#frags[68]
###################################################################
# Apex check

function apex_check(corr_mat,ret_tol)
    vals,locs=findmax(corr_mat,dims=1)
    corr_mat_cleaned=copy(corr_mat)
    target_ret=locs[1][1]
    for i=1:length(vals)
        tv1=corr_mat_cleaned[:,i]
        #corr_mat_cleaned[:,i]=corr_mat[:,i]
        if abs(locs[i][1]-target_ret)>ret_tol
            corr_mat_cleaned[:,i] .=0

        elseif length(tv1[tv1 .>0]) <2
            corr_mat_cleaned[:,i] .=0
        end


    end

    # plot(corr_mat_cleaned)
    # plot(corr_mat)
    # i =3
    # plot(corr_mat_cleaned[40:50,2:end])
    return corr_mat_cleaned
end

# corr_mat_cleaned=apex_check(corr_mat,ret_tol)
#
####################################################################
# fragement extractor

function frag_extract(corr_mat_cleaned,frags,r_tresh)

    r_ind=corr_check(corr_mat_cleaned,r_tresh)
    frag_mz=frags[r_ind .+ 1]

    sel_corr_mat=corr_mat_cleaned[:,r_ind .+ 1]
    frag_int=maximum(sel_corr_mat,dims=1)

# plot(sel_corr_mat)

    return(frag_mz,frag_int)
end




###############################################################################
# Function for finding MS2 adducts
#

function adduct_find_MS2(mass,frag_mz,add_m,mass_tol)


    dmass=frag_mz .- mass

    x=zeros(length(dmass),1)

    for i=1:length(dmass)
        #println(i)
        err=abs.(abs(dmass[i]) .- add_m)
        tv=findall(x -> x <= mass_tol[1]/2, err)
        if length(tv)>0
            x[i]=1
        end
    end

    return x


end # function






####################################################################
#Simple moving average

function sma(y, n)

    vals = zeros(size(y,1) ,1)
    for i in 1:size(vals,1)-(n-1)
        vals[i+1] = mean(y[i:i+(n-1)])
    end

    vals[1]=y[1]
    vals[end]=y[end]
    return vals
end

###############################################################################
# Function for finding MS2 isotopes
#

function iso_find_ms2(mass,frag_mz,ru_m,delta_mass)

    x=zeros(size(frag_mz,1),1)
    kmd_p=KMD_calc_ext(mass,ru_m["ru_m"])

    for i=1:size(x,1)
        kmd_temp=KMD_calc_ext(frag_mz[i],ru_m["ru_m"])
        if round(median(abs.(kmd_p[1] .- kmd_temp[1]));digits=3)<= 2*delta_mass
            x[i]=1
        end

    end

    return x
end # function

# i=9
###############################################################################
# Function for finding MS2 fragments
#

function frag_find_MS2(mass,frag_mz,mass_tol,NL)

    x=zeros(size(frag_mz,1),1)

    n_ls=mass .- frag_mz

    for i=1:size(x,1)
        if abs(n_ls[i]) <= mass_tol/2
            x[i]=1

        elseif n_ls[i] > 0 && length(findall(abs.(n_ls[i] .- NL["mass"]) .<= mass_tol/2))>0 &&
            NL["prob"][argmin(abs.(n_ls[i] .- NL["mass"]))]>= 0.05
            x[i]=1

        end
    end

    return x
end # function

# i=3
# plot(abs.(n_ls[i] .- NL["mass"]))

###############################################################################
# Function for filtering MS2 frags
#

function ms2filter(mass,mass2rep,frag_mz,frag_int,mass_tol,NL,ru_m,delta_mass,add_m)

    compInd=zeros(length(frag_mz),3)
    ad_ms2=0
    if length(frag_mz)>0
        compInd[:,1]=adduct_find_MS2(mass2rep,frag_mz,add_m,mass_tol)
        compInd[:,2]=iso_find_ms2(mass,frag_mz,ru_m,delta_mass)
        compInd[:,3]=frag_find_MS2(mass,frag_mz,mass_tol,NL)
    end

    frag_mz_sel=frag_mz[findall(x -> x > 0,sum(compInd, dims=2))]
    frag_int_sel=frag_int[1,findall(x -> x > 0,sum(compInd, dims=2))]

    if sum(compInd[:,1])>0 || sum([compInd[:,1];compInd[:,3]])>0
        ad_ms2=1

    end

    return(frag_mz_sel,frag_int_sel,ad_ms2)

end # function


###############################################################################
# warper function decon single feature for SWATH internal feature list
# parameters


function feature_decon_SWATH_singlefeature_internal(chrom,pre_mz,
    r_tresh,min_int,feature,mass_win_per,ret_win_per)

    SWATH_n=length(pre_mz)
    massWin=[feature.MinMass,feature.MaxMass]
    mass_tol=mass_win_per*(feature.MaxMass - feature.MinMass)
    mass=feature.MeasMass

    if mass >= minimum(chrom["MS1"]["Mz_values"]) && mass <= maximum(chrom["MS1"]["Mz_values"])


        ind=argmin(abs.(mass .- pre_mz))
        timeWin=[floor(Int,feature.ScanNum - feature.ScanInPeak),ceil(Int,feature.ScanNum + feature.ScanInPeak)]

        if timeWin[1]<=0
            timeWin[1]=1
        elseif timeWin[2] > size(chrom["MS1"]["Mz_values"],1)
            timeWin[2]=size(chrom["MS1"]["Mz_values"],1)
        end

        ret_tol=floor(ret_win_per*feature.ScanInPeak/2)

        ms1val,ms1int,ms2val,ms2int=sig_select_SWATH(chrom,timeWin,ind,SWATH_n,massWin);

        corr_mat,frags=corrlation_mat_gen(ms1int,ms1val,ms2int,ms2val,min_int,mass_tol)
        # frags[frags .>0]
        corr_mat_cleaned=apex_check(corr_mat,ret_tol)
        frag_mz,frag_int=frag_extract(corr_mat_cleaned,frags,r_tresh)


    else
        println("This feature was not within the defined SWATH windows.")
        frag_mz=[]
        frag_int=[]

    end


    return frag_mz,frag_int,mass_tol


end


# findall(x -> 148 >= x >= 147,frags)
# frags[68]

# plot(corr_mat)

# plot(corr_mat_cleaned)
###############################################################################
# warper function decon single feature for DIA internal feature list
#

function feature_decon_DIA_singlefeature_internal(chrom,filenames,r_tresh,
    min_int,feature,mass_win_per,ret_win_per)


    massWin=[feature.MinMass,feature.MaxMass]
    mass_tol=mass_win_per*(feature.MaxMass - feature.MinMass)
    mass=feature.MeasMass


    timeWin=[floor(Int,feature.ScanNum - feature.ScanInPeak),ceil(Int,feature.ScanNum + feature.ScanInPeak)]

    if timeWin[1]<=0
        timeWin[1]=1
    elseif timeWin[2]>=size(chrom["MS1"]["Mz_intensity"],1)
        timeWin[2]=size(chrom["MS1"]["Mz_intensity"],1)
    end



    ret_tol=floor(ret_win_per*feature.ScanInPeak/2)

    ms1val,ms1int,ms2val,ms2int=sig_select_DIA(chrom,timeWin,massWin)
    corr_mat,frags=corrlation_mat_gen(ms1int,ms1val,ms2int,ms2val,min_int,mass_tol)
    corr_mat_cleaned=apex_check(corr_mat,ret_tol)
    frag_mz,frag_int=frag_extract(corr_mat_cleaned,frags,r_tresh)




    return frag_mz,frag_int,mass_tol


end



###############################################################################
# wraper function decon single file for DIA internal feature list
#

function comp_DIA_ESI(chrom,path2features,mass_win_per,
    ret_win_per,r_tresh,mode,delta_mass,min_int)

    max_mass=maximum(chrom["MS1"]["Mz_values"][:])

    a_d=load("AUX_data.jld","AUX_data") # Auxilary data

    if mode == "POSITIVE"
        add_m=a_d["Ad"]["pos"]

    elseif mode == "NEGATIVE"

        add_m=a_d["Ad"]["neg"]
    end

    ru_m=a_d["Iso"]
    NL=a_d["Frag"]

    feature_list = CSV.File(path2features) |> DataFrame!
    decon_tab=Matrix(undef,size(feature_list,1),size(feature_list,2)+6)

    # i=7
    for i=1:size(feature_list,1)
        println(i)
        if feature_list.ScanNum[i]>0 && feature_list.MeasMass[i]<=max_mass

            feature=feature_list[i,:]

            ind_parent,ind_ms1_comp,mass = ms1_comp_finder(feature,feature_list,chrom,ret_win_per,r_tresh,
                delta_mass,add_m,ru_m,NL,mode)



            decon_tab,feature_list=rm_ms1!(ind_parent,ind_ms1_comp,mass,decon_tab,feature_list)


            massWin=[feature_list.MinMass[i],feature_list.MaxMass[i]]
            mass_tol=mass_win_per*(feature_list.MaxMass[i]-feature_list.MinMass[i])
            mass=feature_list.MeasMass[i]
            feature=feature_list[i,:]

            timeWin=[floor(Int,feature.ScanNum - feature.ScanInPeak),ceil(Int,feature.ScanNum + feature.ScanInPeak)]

            if timeWin[1]<=0
                timeWin[1]=1
            elseif timeWin[2]>=size(chrom["MS2"]["Mz_intensity"],1)
                timeWin[2]=size(chrom["MS2"]["Mz_intensity"],1)
            end



            ret_tol=floor(ret_win_per*feature.ScanInPeak/2)

            ms1val,ms1int,ms2val,ms2int=sig_select_DIA(chrom,timeWin,massWin)
            corr_mat,frags=corrlation_mat_gen(ms1int,ms1val,ms2int,ms2val,min_int,mass_tol)
            corr_mat_cleaned=apex_check(corr_mat,ret_tol)
            frag_mz,frag_int=frag_extract(corr_mat_cleaned,frags,r_tresh)
            if mode == "POSITIVE"
                mass2rep=feature_list.MeasMass[i]- 1.007825
                mass=feature_list.MeasMass[i]
            elseif mode == "NEGATIVE"
                mass2rep=feature_list.MeasMass[i] + 1.007825
                mass=feature_list.MeasMass[i]
            end

            frag_mz_sel,frag_int_sel,ad_ms2=ms2filter(mass,mass2rep,frag_mz,frag_int,mass_tol,NL,ru_m,delta_mass,add_m)

            if length(frag_mz_sel)>0 && ad_ms2 ==1
                decon_tab[ind_parent[1],13]=1.0
                decon_tab[ind_parent[1],14]=mass2rep
                decon_tab[ind_parent[1],17] = string(frag_mz_sel[:])
                decon_tab[ind_parent[1],18] = string(frag_int_sel[:])
            elseif length(frag_mz_sel)>0 && ad_ms2 ==0
                decon_tab[ind_parent[1],17] = string(frag_mz_sel[:])
                decon_tab[ind_parent[1],18] = string(frag_int_sel[:])

            else
                decon_tab[ind_parent[1],17]=0
                decon_tab[ind_parent[1],18]=0

            end

        else
            decon_tab[i,:] .= 0

        end


    end



    table=DataFrame(decon_tab,[:Nr,:ScanNum,:ScanInPeak,:Rt,:SecInPeak,
    :MeasMass,:MinMass,:MaxMass,:Area,:Int,:FeatPurity,:MediRes,:Parent,:AccuMass,:MS1Comp,:MS1CompInt,:MS2Comp,:MS2CompInt])
    sort!(table,[:Nr])

    final_table=table[table[!,:Int] .> 0,:]

    final_table.Nr=1:size(final_table,1)
    m=split(path2features,".")
    output=string(m[1],"_comp.csv")
    CSV.write(output,final_table)
    println("The final report has been saved in the output path!")
    return(fianl_table)


end


# i=2


###############################################################################
# wraper function decon single file for DIA internal feature list
#

function comp_DIA_ESI_mc(chrom,path2features,mass_win_per,
    ret_win_per,r_tresh,mode,delta_mass,min_int)

    max_mass=maximum(chrom["MS1"]["Mz_values"][:])

    a_d=load("AUX_data.jld","AUX_data") # Auxilary data

    if mode == "POSITIVE"
        add_m=a_d["Ad"]["pos"]

    elseif mode == "NEGATIVE"

        add_m=a_d["Ad"]["neg"]
    end

    ru_m=a_d["Iso"]
    NL=a_d["Frag"]

    feature_list = CSV.File(path2features) |> DataFrame!
    decon_tab=Matrix(undef,size(feature_list,1),size(feature_list,2)+6);

    # i=3

    for i=1:size(feature_list,1)

        if feature_list.ScanNum[i]>0 && feature_list.MeasMass[i]<=max_mass

            feature=feature_list[i,:]

            ind_parent,ind_ms1_comp,mass = ms1_comp_finder(feature,feature_list,chrom,ret_win_per,r_tresh,
                delta_mass,add_m,ru_m,NL,mode)



            decon_tab,feature_list=rm_ms1!(ind_parent,ind_ms1_comp,mass,decon_tab,feature_list);


            massWin=[feature_list.MinMass[i],feature_list.MaxMass[i]]
            mass_tol=mass_win_per*(feature_list.MaxMass[i]-feature_list.MinMass[i])
            mass=feature_list.MeasMass[i]
            feature=feature_list[i,:]

            timeWin=[floor(Int,feature.ScanNum - feature.ScanInPeak),ceil(Int,feature.ScanNum + feature.ScanInPeak)]

            if timeWin[1]<=0
                timeWin[1]=1
            elseif timeWin[2]>=size(chrom["MS2"]["Mz_intensity"],1)
                timeWin[2]=size(chrom["MS2"]["Mz_intensity"],1)
            end



            ret_tol=floor(ret_win_per*feature.ScanInPeak/2)


            ms1val,ms1int,ms2val,ms2int = sig_select_DIA_cl(chrom,timeWin,massWin)

            frag_mz_sel_=Array{Any}(undef,length(ms2val))
            frag_int_sel_=Array{Any}(undef,length(ms2val))
            ad_ms2_=Array{Any}(undef,length(ms2val))
            println(i)

            if mode == "POSITIVE"
                mass2rep=feature_list.MeasMass[i]- 1.007825
                mass=feature_list.MeasMass[i]
            elseif mode == "NEGATIVE"

                mass2rep=feature_list.MeasMass[i] + 1.007825
                mass=feature_list.MeasMass[i]

            end            # i=1
            #
            for j=1:length(ms2val)
                #println(j)
                corr_mat,frags=corrlation_mat_gen(ms1int,ms1val,ms2int[j],ms2val[j],min_int,mass_tol)
                corr_mat_cleaned=apex_check(corr_mat,ret_tol)
                frag_mz,frag_int=frag_extract(corr_mat_cleaned,frags,r_tresh)

                frag_mz_sel_[j],frag_int_sel_[j],ad_ms2_[j]=ms2filter(mass,mass2rep,frag_mz,
                    frag_int,mass_tol,NL,ru_m,delta_mass,add_m)

            end
            # i=1
            # plot(corr_mat_cleaned)
            frag_mz_sel=zeros(1,length(ms2val))
            frag_int_sel=zeros(1,length(ms2val))
            for j=1:length(ms2val)
                if length(frag_mz_sel_[j][:])>0
                    frag_mz_sel[j]=frag_mz_sel_[j][1]
                    frag_int_sel[j]=frag_int_sel_[j][1]
                end
            end




            if length(sum(frag_mz_sel))>0 && sum(ad_ms2_) ==1
                decon_tab[ind_parent[1],13]=1.0
                decon_tab[ind_parent[1],14]=mass2rep
                decon_tab[ind_parent[1],17] = string(frag_mz_sel[frag_mz_sel[:] .>0])
                decon_tab[ind_parent[1],18] = string(frag_int_sel[frag_mz_sel[:] .>0])
            elseif length(sum(frag_mz_sel))>0 && sum(ad_ms2_) ==0
                decon_tab[ind_parent[1],17] = string(frag_mz_sel[frag_mz_sel[:] .>0])
                decon_tab[ind_parent[1],18] = string(frag_int_sel[frag_mz_sel[:] .>0])

            else
                decon_tab[ind_parent[1],17]=0
                decon_tab[ind_parent[1],18]=0

            end

        else
            decon_tab[i,:] .= 0

        end


    end



    table=DataFrame(decon_tab,[:Nr,:ScanNum,:ScanInPeak,:Rt,:SecInPeak,
    :MeasMass,:MinMass,:MaxMass,:Area,:Int,:FeatPurity,:MediRes,:Parent,:AccuMass,:MS1Comp,:MS1CompInt,:MS2Comp,:MS2CompInt])
    sort!(table,[:Nr])

    final_table=table[table[!,:Int] .> 0,:]

    final_table.Nr=1:size(final_table,1)
    m=split(path2features,".")
    output=string(m[1],"_comp.csv")
    CSV.write(output,final_table)
    println("The final report has been saved in the output path!")
    return(fianl_table)


end


#############################################################################
# Wrapping function for componentization of SWATH

function comp_SWATH_ESI(chrom,path2features,mass_win_per,
    ret_win_per,r_tresh,mode,delta_mass,pre_mz,min_int)

    max_mass=maximum(chrom["MS1"]["Mz_values"][:])

    a_d=load("AUX_data.jld","AUX_data") # Auxilary data

    if mode == "POSITIVE"
        add_m=a_d["Ad"]["pos"]

    elseif mode == "NEGATIVE"

        add_m=a_d["Ad"]["neg"]
    end

    ru_m=a_d["Iso"]
    NL=a_d["Frag"]

    feature_list = CSV.File(path2features) |> DataFrame!;

    decon_tab=Matrix(undef,size(feature_list,1),size(feature_list,2)+6);
    #decon_tab=Matrix(undef,20,size(feature_list,2)+6)

    # i=7


    for i=1:size(feature_list,1)
        println(i)
        if feature_list.ScanNum[i]>0 && feature_list.MeasMass[i]<=max_mass

            feature=feature_list[i,:]

            ind_parent,ind_ms1_comp,mass = ms1_comp_finder(feature,feature_list,chrom,ret_win_per,r_tresh,
                delta_mass,add_m,ru_m,NL,mode)

            decon_tab,feature_list=rm_ms1!(ind_parent,ind_ms1_comp,mass,decon_tab,feature_list)

            frag_mz,frag_int,mass_tol=feature_decon_SWATH_singlefeature_internal(chrom,pre_mz,
                r_tresh,min_int,feature,mass_win_per,ret_win_per)

            if mode == "POSITIVE"
                mass2rep=feature_list.MeasMass[i]- 1.007825
                mass=feature_list.MeasMass[i]
            elseif mode == "NEGATIVE"
                mass2rep=feature_list.MeasMass[i] + 1.007825
                mass=feature_list.MeasMass[i]
            end
            #println(mass2rep)
            frag_mz_sel,frag_int_sel,ad_ms2=ms2filter(mass,mass2rep,frag_mz,frag_int,mass_tol,NL,ru_m,delta_mass,add_m)

            if length(frag_mz_sel)>0 && ad_ms2 ==1
                decon_tab[ind_parent[1],13]=1.0
                decon_tab[ind_parent[1],14]=mass2rep
                decon_tab[ind_parent[1],17] = string(frag_mz_sel[:])
                decon_tab[ind_parent[1],18] = string(frag_int_sel[:])
            elseif length(frag_mz_sel)>0 && ad_ms2 ==0
                decon_tab[ind_parent[1],17] = string(frag_mz_sel[:])
                decon_tab[ind_parent[1],18] = string(frag_int_sel[:])

            else
                decon_tab[ind_parent[1],17]=0
                decon_tab[ind_parent[1],18]=0

            end

        else
            decon_tab[i,:] .= 0
        end

    end

    # decon_tab[ind_parent[1],17]

    table=DataFrame(decon_tab,[:Nr,:ScanNum,:ScanInPeak,:Rt,:SecInPeak,
    :MeasMass,:MinMass,:MaxMass,:Area,:Int,:FeatPurity,:MediRes,:Parent,:AccuMass,:MS1Comp,:MS1CompInt,:MS2Comp,:MS2CompInt])
    sort!(table,[:Nr])
    final_table=table[table[!,:Int] .> 0,:]
    final_table.Nr=1:size(final_table,1)
    m=split(path2features,".")
    output=string(m[1],"_comp.csv")
    CSV.write(output,final_table)
    println("The final report has been saved in the output path!")
    return final_table

end


# i=498
# i=2

#############################################################################
# Wrapping function for componentization of MS1

function comp_ms1(chrom,path2features,mass_win_per,
    ret_win_per,r_tresh,mode,delta_mass)

    max_mass=maximum(chrom["MS1"]["Mz_values"][:])
    a_d=load("AUX_data.jld","AUX_data") # Auxilary data

    if mode == "POSITIVE"
        add_m=a_d["Ad"]["pos"]

    elseif mode == "NEGATIVE"

        add_m=a_d["Ad"]["neg"]
    end

    ru_m=a_d["Iso"]
    NL=a_d["Frag"]

    feature_list=CSV.read(path2features; copycols=true)
    #SWATH=CSV.read(path2SWATH)
    decon_tab=Matrix(undef,size(feature_list,1),size(feature_list,2)+4)
    #decon_tab=Matrix(undef,500,size(feature_list,2)+4)

    #chrom=ms_

    # i=128


    for i=1:size(feature_list,1)
        println(i)
        if feature_list.ScanNum[i]>0 && feature_list.MeasMass[i]<=max_mass

            feature=feature_list[i,:]
            ind_parent,ind_ms1_comp,mass = ms1_comp_finder(feature,feature_list,chrom,ret_win_per,r_tresh,
                delta_mass,add_m,ru_m,NL)
            decon_tab,feature_list=rm_ms1!(ind_parent,ind_ms1_comp,mass,decon_tab,feature_list)
        else
            decon_tab[i,:] .= 0
        end

    end

    table=DataFrame(decon_tab,[:Nr,:ScanNum,:ScanInPeak,:Rt,:SecInPeak,
    :MeasMass,:MinMass,:MaxMass,:Area,:Int,:FeatPurity,:MediRes,:Parent,:AccuMass,:MS1Comp,:MS1CompInt])
    sort!(table,[:Nr])
    final_table=table[table[!,:Int] .> 0,:]
    final_table.Nr=1:size(final_table,1)
    m=split(path2features,".")
    output=string(m[1],"_compMS1.csv")
    CSV.write(output,final_table)
    println("The final report has been saved in the output path!")

end


#############################################################################
# Wrapping function for componentization


function comp_DIA(chrom,path2features,mass_win_per,ret_win_per,r_thresh,delta_mass,min_int)

    if chrom["MS1"]["Polarity"][1] == "+"
        mode="POSITIVE"
    elseif chrom["MS1"]["Polarity"][1] == "-"
        mode="NEGATIVE"
    end

    pre_mz=unique(chrom["MS2"]["PrecursorIon"])
    prec_win=chrom["MS2"]["PrecursorIonWin"][1:length(pre_mz)]
    tv=chrom["MS1"]["Mz_values"][:]
    ms1_win=maximum(chrom["MS1"]["Mz_values"][:])-minimum(tv[tv .> 0])

    if length(prec_win) > 1 && length(unique(prec_win)) > 3 && minimum(prec_win) > 1


        #SWATH
        println("This file will be processed as a SWATH file.")
        final_table=comp_SWATH_ESI(chrom,path2features,mass_win_per,
            ret_win_per,r_thresh,mode,delta_mass,pre_mz,min_int)
    elseif length(prec_win) > 1 && length(unique(prec_win)) <= 3  && length(prec_win) > 1

            # DIA multi-collision
        println("This file will be processed as a multicollision one.")
        final_table=comp_DIA_ESI_mc(chrom,path2features,mass_win_per,
                ret_win_per,r_thresh,mode,delta_mass,min_int)


    elseif length(prec_win) == 1
        # DIA
        println("This file will be processed as a normal DIA file.")
        final_table=comp_DIA_ESI(chrom,path2features,mass_win_per,
            ret_win_per,r_thresh,mode,delta_mass,min_int)

    else

        return(println("The chromatogram provided is not correct! Please try again with a correct file."))
    end



end


#############################################################################
# Wrapping function for componentization batch

function comp_DIA_batch(pathin,mass_win_per,
    ret_win_per,r_thresh,delta_mass,min_int,mz_thresh)
    fs=readdir(pathin)
    for i=1:length(fs)
        fs1=split(fs[i],'.')
        if fs1[2] == "csv" && fs1[1][end-4:end] == "eport"
            tv1=fs1[1]
            filenames=[string(tv1[1:end-7],".mzXML")]
            println(filenames)
            path2features=joinpath(pathin,fs[i])
            chrom=import_files(pathin,filenames,mz_thresh)

            compo_list = comp_DIA(chrom,path2features,mass_win_per,ret_win_per,r_thresh,delta_mass,min_int)


        end


    end


end # function


#############################################################################
# Wrapping function for componentization batch


function comp_DIA_batch_dist(pathin,mass_win_per,
    ret_win_per,r_thresh,delta_mass,min_int,mz_thresh)

    chrom_mat=Import_files_batch(pathin,mz_thresh);


    mass_win_per_mat=repeat([mass_win_per],size(chrom_mat,1))
    ret_win_per_mat=repeat([ret_win_per],size(chrom_mat,1))
    r_thresh_mat=repeat([r_thresh],size(chrom_mat,1))
    delta_mass_mat=repeat([delta_mass],size(chrom_mat,1))
    min_int_mat=repeat([min_int],size(chrom_mat,1))
    fs=readdir(pathin)
    path2features_mat=Array{Any}(undef,length(fs))


    for i=1:length(fs)
        fs1=split(fs[i],'.')
        if fs1[2] == "csv" && fs1[1][end-4:end] == "eport"
            tv1=fs1[1]
            filenames=[string(tv1[1:end-7],".mzXML")]
            #println(filenames)
            path2features_mat[i]=joinpath(pathin,fs[i])

        else

            path2features_mat[i]=0

        end


    end
    path2features_mat_1=path2features_mat[path2features_mat .!=0]

    for i =1:length(chrom_mat)
        compo_list = comp_DIA(chrom_mat[i],path2features_mat_1[i],mass_win_per_mat[i],
        ret_win_per_mat[i],r_thresh_mat[i],delta_mass_mat[i],min_int_mat[i])
    end

    #pmap(comp_DIA,chrom_mat,path2features_mat_1,mass_win_per_mat,
    #ret_win_per_mat,r_thresh_mat,delta_mass_mat,min_int_mat;retry_delays = zeros(2), on_error=identity)


end

##############################################################################
# Testing area

#path2features="/Users/saersamanipour/Desktop/UvA/Temp_files/Phil/Drug cal/mzxml_files/100PPB DRUGS_report.csv"
# path2features="/Users/saersamanipour/Desktop/dev/Data/AIMSMS-positive-VOSS-3-02_report.csv"
#mass_win_per=0.8
#ret_win_per=0.5
#r_thresh=0.8
#delta_mass=0.004
#min_int=300


#pathin="/Users/saersamanipour/Desktop/UvA/Temp_files/Phil/Drug cal/mzxml_files"
#pathin="/Users/saersamanipour/Desktop/dev/Data"
#filenames=["AIMSMS-positive-VOSS-3-02.mzXML"]
#filenames=["100PPB DRUGS.mzXML"]
#mz_thresh=[0,150]




# ch=import_files(pathin,filenames,mz_thresh)
# ch1=emp_scan_rm!(ch)
# chrom=ms2_align!(ch1)

# chrom["MS2"]["Mz_values"]

#chrom=import_files(pathin,filenames,mz_thresh)

#@time comp_ms1(chrom,path2features,mass_win_per,
#    ret_win_per,r_tresh,mode,delta_mass)



#@time comp_SWATH_ESI(chrom,path2features,mass_win_per,
#    ret_win_per,r_tresh,mode,delta_mass,path2SWATH,min_int)


#@time comp_SWATH_ESI_batch(pathin,mass_win_per,
#    ret_win_per,r_tresh,mode,delta_mass,path2SWATH,min_int,format,mz_thresh)

#@time comp_DIA(chrom,path2features,mass_win_per,ret_win_per,r_thresh,delta_mass,min_int)

#@time comp_DIA_batch(pathin,mass_win_per,
#    ret_win_per,r_thresh,delta_mass,min_int,mz_thresh)


pathin="/Users/saersamanipour/Desktop/UvA/Temp_files/Phil/Drug cal/mzxml_files"
mz_thresh=[0,150]


mass_win_per=0.8
ret_win_per=0.5
r_thresh=0.8
delta_mass=0.004
min_int=300

 comp_DIA_batch(pathin,mass_win_per,
    ret_win_per,r_thresh,delta_mass,min_int,mz_thresh)



#@time comp_DIA_batch_dist(pathin,mass_win_per,
#        ret_win_per,r_thresh,delta_mass,min_int,mz_thresh)
