
module FeatureDeconvolution

push!(LOAD_PATH,pwd())

using MS_Import

using MS_Visualization
using XLSX
using CSV
using DataFrames
using Plots
using Statistics
using LsqFit
using Dierckx


export feature_decon_SWATH_singlefile_internal,
feature_decon_SWATH_singlefeature_internal, feature_decon_SWATH_singlefeature_ext,
feature_decon_DIA_singlefeature_ext, feature_decon_DIA_singlefeature_internal,
feature_decon_DIA_singlefile_internal

#############################################################################
# testing area
# chrom import

#pathin="C:\\Temp_File\\Jake"
#filenames=["180420-SWATH-JO054-006 Day5 C.mzXML"]
#format="mzxml"
#mz_thresh=600
# XIC extract
#vis=1
#mass=239.0623
#tol=0.01
#opt="Base" #"TIC"
#scan_lim=[550,610] #""
#i=2




#XIC,P=XIC_Extract_MS1(vis,chrom,mass,opt,tol,scan_lim)

#plot(XIC,label="XIC for the mass 239.0623",dpi=300)


############################################################################
# Start of the module
#

################################
# Signal selection SWATH


function sig_select_SWATH(chrom,timeWin,ind,SWATH_n,massWin)
    MS1val=chrom["MS1"]["Mz_values"][timeWin[1]:timeWin[2],:]
    MS1int=chrom["MS1"]["Mz_intensity"][timeWin[1]:timeWin[2],:]

    ms1val=zeros(size(MS1val,1),100)
    ms1int=zeros(size(MS1val,1),100)

    for i=1:size(MS1val,1)
        tv1=MS1val[i,findall(x -> massWin[2] > x >= massWin[1],MS1val[i,:])]
        if length(tv1)>0
            ms1val[i,1:length(tv1)]=tv1
            ms1int[i,1:length(tv1)]=MS1int[i,findall(x -> massWin[2] > x >= massWin[1],MS1val[i,:])]

        end

    end


    if ind>1
        ind=ind-1
    end

    c=range(1,stop=(size(chrom["MS2"]["Mz_values"],1)),step=SWATH_n-1)

    #c=range(1,stop=(size(chrom["MS2"]["Mz_values"],1)-(ind-2)),step=SWATH_n-1)

    MS2val=chrom["MS2"]["Mz_values"][c .+ (ind-2),:]
    MS2int=chrom["MS2"]["Mz_intensity"][c .+ (ind-2),:]



    ms2val=MS2val[timeWin[1]:timeWin[2],:]
    ms2int=MS2int[timeWin[1]:timeWin[2],:]

    return(ms1val,ms1int,ms2val,ms2int)

end

# ind=4

# ms1val,ms1int,ms2val,ms2int=sig_select_SWATH(chrom,timeWin,ind,SWATH_n,massWin)
#
################################
# Signal selection DIA


function sig_select_DIA(chrom,timeWin,massWin)

    MS1val=chrom["MS1"]["Mz_values"][timeWin[1]:timeWin[2],:]
    MS1int=chrom["MS1"]["Mz_intensity"][timeWin[1]:timeWin[2],:]

    ms1val=zeros(size(MS1val,1),100)
    ms1int=zeros(size(MS1val,1),100)

    for i=1:size(MS1val,1)
        tv1=MS1val[i,findall(x -> massWin[2] > x >= massWin[1],MS1val[i,:])]
        if length(tv1)>0
            ms1val[i,1:length(tv1)]=tv1
            ms1int[i,1:length(tv1)]=MS1int[i,findall(x -> massWin[2] > x >= massWin[1],MS1val[i,:])]

        end

    end


    ms2val=chrom["MS2"]["Mz_values"][timeWin[1]:timeWin[2],:]
    ms2int=chrom["MS2"]["Mz_intensity"][timeWin[1]:timeWin[2],:]

    return(ms1val,ms1int,ms2val,ms2int)

end




#########################################################################
# XIC generator

function xic_gen_ms2!(mass2,mass_tol,ms2val,ms2int)

    ms2_val_sel=zeros(size(ms2int,1),50)
    ms2_int_sel=zeros(size(ms2int,1),50)

    for i=1:size(ms2int,1)
        tv1=ms2val[i,findall(x -> mass2+mass_tol > x >= mass2-mass_tol,ms2val[i,:])]
        if length(tv1)>0
            ms2_val_sel[i,1:length(tv1)]=tv1
            ms2_int_sel[i,1:length(tv1)]=ms2int[i,findall(x -> mass2+mass_tol >= x >= mass2-mass_tol,ms2val[i,:])]
            ms2int[i,findall(x -> mass2+mass_tol >= x >= mass2-mass_tol,ms2val[i,:])].=0
        end


    end
    return ms2_val_sel,ms2_int_sel,ms2int

end


# ms2_val_sel,ms2_int_sel=xic_gen_ms2!(mass2,mass_tol,ms2val,ms2int)
#
#########################################################################
# Correlation matrix generator

function corrlation_mat_gen(ms1int,ms1val,ms2int,ms2val,min_int,mass_tol)

    val,loc=findmax(ms1int)
    xic=maximum(ms1int,dims=2)
    # plot(xic)

    nzeroind=findall(x -> x >= min_int, ms2int[loc[1],:])


    corr_mat=zeros(size(ms2int,1),length(nzeroind)+1)
    frags=zeros(length(nzeroind)+1)
    corr_mat[:,1]=xic
    frags[1]=mean(ms1val[ms1val .> 0])

    for i=1:length(nzeroind)
        val2,loc2=findmax(ms2int)
        if val2 >= min_int
            mass2=ms2val[loc2]
            ms2_val_sel,ms2_int_sel=xic_gen_ms2!(mass2,mass_tol,ms2val,ms2int)
            frags[i+1]=mean(ms2_val_sel[ms2_val_sel .> 0])
            corr_mat[:,i+1]=maximum(ms2_int_sel,dims=2)

        end



    end

    # plot(corr_mat)
    # scatter(frags)
    return(corr_mat,frags)
end


# corr_mat,frags=corrlation_mat_gen(ms1int,ms1val,ms2int,ms2val,min_int,mass_tol)
#
###################################################################
# Apex check

function apex_check(corr_mat,ret_tol)
    vals,locs=findmax(corr_mat,dims=1)
    corr_mat_cleaned=copy(corr_mat)
    target_ret=locs[1][1]
    for i=1:length(vals)
        if abs(locs[i][1]-target_ret)>ret_tol
            corr_mat_cleaned[:,i] .=0
        end


    end

    #plot(corr_mat_cleaned)
    # plot(corr_mat)
    return corr_mat_cleaned
end

# corr_mat_cleaned=apex_check(corr_mat,ret_tol)
#
####################################################################
# fragement extractor

function frag_extract(corr_mat_cleaned,frags,r_tresh)

    r_ind=corr_check(corr_mat_cleaned,r_tresh)
    frag_mz=frags[r_ind .+ 1]

    sel_corr_mat=corr_mat_cleaned[:,r_ind .+ 1]
    frag_int=maximum(sel_corr_mat,dims=1)


    return(frag_mz,frag_int)
end

# frag_mz,frag_int=frag_extract(corr_mat_cleaned,frags,r_tresh)
#
######################################################################
# Correlation check

function corr_check(corr_mat_cleaned,r_tresh)
    r=cor(corr_mat_cleaned,dims=1)
    r1=round.(r[2:end,1];digits=2)

    r_ind=findall(x -> x >=r_tresh,r1)

    return r_ind
end

#
# r_ind=corr_check(corr_mat_cleaned,r_tresh)

######################################################################
## warper function decon single file for SWATH internal feature list
# parameters


function feature_decon_SWATH_singlefile_internal(chrom,pathout,filenames,
    path2features,path2SWATH,r_tresh,min_int,mass_win_per,ret_win_per)

    feature_list=CSV.read(path2features)
    SWATH=CSV.read(path2SWATH)
    mass_vect=SWATH[2]
    SWATH_n=length(mass_vect)
    decon_tab=Matrix(undef,size(feature_list,1),7)

    for i=1:size(feature_list,1)
        decon_tab[i,1]=i
        decon_tab[i,2]=string(feature_list.ScanNum[i])
        decon_tab[i,3]=string(feature_list.MeasMass[i])
        decon_tab[i,4]=string(feature_list.Int[i])

        massWin=[feature_list.MinMass[i],feature_list.MaxMass[i]]
        mass_tol=mass_win_per*(feature_list.MaxMass[i]-feature_list.MinMass[i])
        mass=feature_list.MeasMass[i]
        if mass >= mass_vect[2]
            ind=findfirst(x -> x >= mass, mass_vect)
            timeWin=[floor(Int,feature_list.ScanNum[i] - feature_list.ScanInPeak[i]) ,
            ceil(Int,feature_list.ScanNum[i] + feature_list.ScanInPeak[i])]
            ret_tol=floor(ret_win_per*feature_list.ScanInPeak[i])

            ms1val,ms1int,ms2val,ms2int=sig_select_SWATH(chrom,timeWin,ind,SWATH_n,massWin)
            corr_mat,frags=corrlation_mat_gen(ms1int,ms1val,ms2int,ms2val,min_int,mass_tol)
            corr_mat_cleaned=apex_check(corr_mat,ret_tol)
            frag_mz,frag_int=frag_extract(corr_mat_cleaned,frags,r_tresh)
        else

            #println("This feature was not within the defined SWATH windows.")
            frag_mz=[]
            frag_int=[]

        end
        decon_tab[i,5]=string(frag_mz[:])
        decon_tab[i,6]=string(frag_int[:])
        decon_tab[i,7]=string(mass_tol)

    end

    table=DataFrame(decon_tab,[:Nr,:ScanNumb,:MS1Mass,:MS1Int,:MS2Mass,:MS2Int,:MSTol])

    if isa(filenames,Array)==1
        m=split(filenames[1],".")
    else
        m=split(filenames,".")
    end

    output=joinpath(pathout, m[1])  * "_decon_features.csv"
    CSV.write(output,table)
    println("The final report has been saved in the output path!")
    return table

end


# table=feature_decon_SWATH_singlefile_internal(format,numchan,namechan,pathout,filenames,mz_thresh,path2features,path2SWATH,r_tresh,min_int,mass_win_per,ret_win_per)
#
###############################################################################
# warper function decon single feature for SWATH internal feature list
# parameters




function feature_decon_SWATH_singlefeature_internal(chrom,filenames,path2SWATH,
    r_tresh,min_int,feature,mass_win_per,ret_win_per)


    SWATH=CSV.read(path2SWATH)
    mass_vect=SWATH[2]
    SWATH_n=length(mass_vect)

    massWin=[feature.MinMass,feature.MaxMass]
    mass_tol=mass_win_per*(feature.MaxMass - feature.MinMass)
    mass=feature.MeasMass
    if mass >= mass_vect[2]
        ind=findfirst(x -> x >= mass, mass_vect)
        timeWin=[floor(Int,feature.ScanNum - feature.ScanInPeak),ceil(Int,feature.ScanNum + feature.ScanInPeak)]
        ret_tol=floor(ret_win_per*feature.ScanInPeak)

        ms1val,ms1int,ms2val,ms2int=sig_select_SWATH(chrom,timeWin,ind,SWATH_n,massWin)
        corr_mat,frags=corrlation_mat_gen(ms1int,ms1val,ms2int,ms2val,min_int,mass_tol)
        corr_mat_cleaned=apex_check(corr_mat,ret_tol)
        frag_mz,frag_int=frag_extract(corr_mat_cleaned,frags,r_tresh)

    else
        println("This feature was not within the defined SWATH windows.")
        frag_mz=[]
        frag_int=[]

    end


    return frag_mz,frag_int,mass_tol


end

# frag_mz,frag_int=feature_decon_SWATH_singlefeature_internal(format,numchan,namechan,filenames,mz_thresh,path2SWATH,r_tresh,min_int,feature,,mass_win_per,ret_win_per)
#
###############################################################################
# warper function decon single feature for SWATH external feature list
#

function feature_decon_SWATH_singlefeature_ext(chrom,filenames,path2SWATH,
    r_tresh,min_int,feature_mass,feature_scan,mass_tol,rt_tol)


    SWATH=CSV.read(path2SWATH)
    mass_vect=SWATH[2]
    SWATH_n=length(mass_vect)

    massWin=[feature_mass-mass_tol,feature_mass+mass_tol]

    if feature_mass >= mass_vect[2]
        ind=findfirst(x -> x >= feature_mass, mass_vect)
        timeWin=[Int( feature_scan - 2*rt_tol),Int( feature_scan + 2*rt_tol)]

        if timeWin[1]<=0
            timeWin[1]=1
        elseif timeWin[2]>=size(chrom["MS1"]["Mz_intensity"],1)
            timeWin[2]=size(chrom["MS1"]["Mz_intensity"],1)
        end

        ms1val,ms1int,ms2val,ms2int=sig_select_SWATH(chrom,timeWin,ind,SWATH_n,massWin)
        corr_mat,frags=corrlation_mat_gen(ms1int,ms1val,ms2int,ms2val,min_int,mass_tol)
        corr_mat_cleaned=apex_check(corr_mat,rt_tol)
        frag_mz,frag_int=frag_extract(corr_mat_cleaned,frags,r_tresh)

    else
        println("This feature was not within the defined SWATH windows.")
        frag_mz=[]
        frag_int=[]

    end


    return frag_mz,frag_int


end


###############################################################################
# warper function decon single feature for DIA internal feature list
#

function feature_decon_DIA_singlefeature_internal(chrom,filenames,r_tresh,
    min_int,feature,mass_win_per,ret_win_per)


    massWin=[feature.MinMass,feature.MaxMass]
    mass_tol=mass_win_per*(feature.MaxMass - feature.MinMass)
    mass=feature.MeasMass


    timeWin=[floor(Int,feature.ScanNum - feature.ScanInPeak),ceil(Int,feature.ScanNum + feature.ScanInPeak)]

    if timeWin[1]<=0
        timeWin[1]=1
    elseif timeWin[2]>=size(chrom["MS1"]["Mz_intensity"],1)
        timeWin[2]=size(chrom["MS1"]["Mz_intensity"],1)
    end



    ret_tol=floor(ret_win_per*feature.ScanInPeak)

    ms1val,ms1int,ms2val,ms2int=sig_select_DIA(chrom,timeWin,massWin)
    corr_mat,frags=corrlation_mat_gen(ms1int,ms1val,ms2int,ms2val,min_int,mass_tol)
    corr_mat_cleaned=apex_check(corr_mat,ret_tol)
    frag_mz,frag_int=frag_extract(corr_mat_cleaned,frags,r_tresh)




    return frag_mz,frag_int,mass_tol


end


###############################################################################
# wraper function decon single file for DIA internal feature list
#

function feature_decon_DIA_singlefile_internal(chrom,pathout,filenames,
    path2features,r_tresh,min_int,mass_win_per,ret_win_per)

    feature_list=CSV.read(path2features)
    decon_tab=Matrix(undef,size(feature_list,1),7)

    for i=1:size(feature_list,1)
        println(i)
        decon_tab[i,1]=i
        decon_tab[i,2]=string(feature_list.ScanNum[i])
        decon_tab[i,3]=string(feature_list.MeasMass[i])
        decon_tab[i,4]=string(feature_list.Int[i])

        massWin=[feature_list.MinMass[i],feature_list.MaxMass[i]]
        mass_tol=mass_win_per*(feature_list.MaxMass[i]-feature_list.MinMass[i])
        mass=feature_list.MeasMass[i]
        feature=feature_list[i,:]

        timeWin=[floor(Int,feature.ScanNum - feature.ScanInPeak),ceil(Int,feature.ScanNum + feature.ScanInPeak)]

        if timeWin[1]<=0
            timeWin[1]=1
        elseif timeWin[2]>=size(chrom["MS1"]["Mz_intensity"],1)
            timeWin[2]=size(chrom["MS1"]["Mz_intensity"],1)
        end



        ret_tol=floor(ret_win_per*feature.ScanInPeak)

        ms1val,ms1int,ms2val,ms2int=sig_select_DIA(chrom,timeWin,massWin)
        corr_mat,frags=corrlation_mat_gen(ms1int,ms1val,ms2int,ms2val,min_int,mass_tol)
        corr_mat_cleaned=apex_check(corr_mat,ret_tol)
        frag_mz,frag_int=frag_extract(corr_mat_cleaned,frags,r_tresh)

        decon_tab[i,5]=string(frag_mz[:])
        decon_tab[i,6]=string(frag_int[:])
        decon_tab[i,7]=string(mass_tol)


    end


    table=DataFrame(decon_tab,[:Nr,:ScanNumb,:MS1Mass,:MS1Int,:MS2Mass,:MS2Int,:MSTol])

    if isa(filenames,Array)==1
        m=split(filenames[1],".")
    else
        m=split(filenames,".")
    end

    output=joinpath(pathout, m[1])  * "_decon_features.csv"
    CSV.write(output,table)
    println("The final report has been saved in the output path!")
    return table


end

###############################################################################
# warper function decon single feature for SWATH external feature list
#

function feature_decon_DIA_singlefeature_ext(chrom,filenames,r_tresh,min_int,
    feature_mass,feature_scan,mass_tol,rt_tol)



    massWin=[feature_mass-mass_tol,feature_mass+mass_tol]


    timeWin=[Int( feature_scan - 2*rt_tol),Int( feature_scan + 2*rt_tol)]

    if timeWin[1]<=0
        timeWin[1]=1
    elseif timeWin[2]>=size(chrom["MS1"]["Mz_intensity"],1)
        timeWin[2]=size(chrom["MS1"]["Mz_intensity"],1)
    end

    ms1val,ms1int,ms2val,ms2int=sig_select_DIA(chrom,timeWin,massWin)
    corr_mat,frags=corrlation_mat_gen(ms1int,ms1val,ms2int,ms2val,min_int,mass_tol)
    corr_mat_cleaned=apex_check(corr_mat,rt_tol)
    frag_mz,frag_int=frag_extract(corr_mat_cleaned,frags,r_tresh)




    return frag_mz,frag_int


end








################################################################################
end  # module
