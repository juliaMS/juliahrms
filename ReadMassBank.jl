#using Distributed
#addprocs(20)
cd("/Users/saersamanipour/Desktop/dev/ongoing/juliahrms/")
push!(LOAD_PATH,"/Users/saersamanipour/Desktop/dev/ongoing/juliahrms/")

using HDF5
using JLD
using CSV
using XLSX
using DataFrames
using PyCall
using BenchmarkTools
#@everywhere using SharedArray






##################################################################
# This is a wapper function to extract both metadata and the spectrum from each
# file. All the parameters for this fucntion are generated automatically.
#

function readMB(path2file,par1list,par2list)

    MBentry=Array{Any}(undef,Int(length(par1list)+3))

    for i=1:size(par1list,1)
        MBentry[i]=parfindMB(path2file,par1list[i],par2list[i])
        #println(MBentry[i])

    end
    MBentry[end-2],MBentry[end-1],MBentry[end]=specfindMB(path2file)

    return MBentry

end

##################################################################
# The is a function to read the metadata from each file.
# path2file: is the path to each file and generated automatically.
# par1: is the primary parameter to extract. This input is inbedded within the
# wrapper fucntion.
# par2: is the secondary parameter to extract. This input is inbedded within the
# wrapper fucntion.

function parfindMB(path2file,par1,par2)
    f=open(path2file)
    lines=readlines(f)
    parOut=[]
    for i=1:length(lines)-1
        #println(i)
        tv1=lines[i]
        #tv2=tv1[1:length(par1)]
        tv2=split(tv1," ")
        tv3=tv2[1]
        if tv3[1:end-1]==par1 && length(par2)>0

            if tv2[2]==par2
                parOut=tv2[3]
                break
            end
        elseif tv3[1:end-1]==par1 && length(par2)==0

            parOut=tv2[2]
            break

        end
    end
    close(f)
    return parOut
end
##################################################################
# This is the fucntion to extract the spectrum from each file.
# path2file: is the path to each file and gerenrated automatically.

function specfindMB(path2file)
    f=open(path2file)
    cont=read(f,String)
    ind=findfirst("PK\$PEAK:",cont)
    ind2=findlast("\n//\n",cont)
    dd=split(cont[ind[end]+1:ind2[1]],"\n")
    mz_vales=Array{Any}(undef,length(dd)-1)
    mz_int=Array{Any}(undef,length(dd)-1)
    mz_int_rel=Array{Any}(undef,length(dd)-1)

    for i=2:length(dd)
        #println(i)
        dd1=split(dd[i]," ")
        if length(dd1)>1
            mz_vales[i-1]=dd1[3]
            mz_int[i-1]=dd1[4]
            tv1=dd1[5]
            if length(tv1)>1
                mz_int_rel[i-1]=tv1[1:end]
            else
                mz_int_rel[i-1]=tv1
            end
        else
            mz_vales[i-1]="0"
            mz_int[i-1]="0"
            mz_int_rel[i-1]="0"
        end
    end

    close(f)
    return (mz_vales,mz_int,mz_int_rel)

end # function

# i=7

##################################################################
# This a wrapper function to convert MassBank into a julia Dict.
# path2dir: is the path to the main directory
# data_type: is for defining if the data is from experimental source or estimated
# the options are "EXP" (for experimental) and "EST" (for estimated).

function massbank_julia(path2dir,data_type)

    par1list=["ACCESSION","CH\$NAME","CH\$FORMULA","CH\$EXACT_MASS","CH\$SMILES",
    "CH\$IUPAC","CH\$LINK","CH\$LINK","CH\$LINK","CH\$LINK","AC\$MASS_SPECTROMETRY",
    "AC\$MASS_SPECTROMETRY","AC\$MASS_SPECTROMETRY", "AC\$MASS_SPECTROMETRY",
    "AC\$MASS_SPECTROMETRY","PK\$SPLASH"]

    par2list=["","","","","","","CAS","PUBCHEM","INCHIKEY","CHEMSPIDER","MS_TYPE",
    "IONIZATION","ION_MODE","COLLISION_ENERGY","RESOLUTION",""]

    ndir=readdir(path2dir)
    c=zeros(length(ndir))
    for i=1:length(ndir)
        p=joinpath(path2dir,ndir[i])
        c[i]=length(readdir(p))

    end
    nf=sum(c)
    println("There are in total $nf files to be imported.")

    MB=zeros(Int(length(par1list)+3),Int(nf))
    access=Array{Any}(undef,Int(nf))
    name=Array{Any}(undef,Int(nf))
    formula=Array{Any}(undef,Int(nf))
    mass=Array{Float64}(undef,Int(nf))
    smiles=Array{Any}(undef,Int(nf))
    inchi=Array{Any}(undef,Int(nf))
    cas=Array{Any}(undef,Int(nf))
    pubchem=Array{Any}(undef,Int(nf))
    inchikey=Array{Any}(undef,Int(nf))
    chemspider=Array{Any}(undef,Int(nf))
    ms_type=Array{Any}(undef,Int(nf))
    ion_s=Array{Any}(undef,Int(nf))
    ion_m=Array{Any}(undef,Int(nf))
    coll_e=Array{Any}(undef,Int(nf))
    res=Array{Any}(undef,Int(nf))
    splash= Array{Any}(undef,Int(nf))
    mz_val=Array{Any}(undef,Int(nf))
    mz_int=Array{Any}(undef,Int(nf))
    mz_int_r=Array{Any}(undef,Int(nf))
    d_type=Array{Any}(undef,Int(nf))

    # i=1
    # j=1

    global c = 1
    global c2 = 0
    for i=1:length(ndir)
        p=joinpath(path2dir,ndir[i])
        a_files=readdir(p)
        for j=1:length(a_files)

            path2file=joinpath(p,a_files[j])
            #path2file=joinpath(p,"UT002246.txt")
            #println(a_files[j])

            # par1=par1list
            # par2=par2list

            mb=[]
            try
                mb=readMB(path2file,par1list,par2list)

                access[c]=mb[1]
                name[c]=mb[2]
                formula[c]=mb[3]
                mass[c]=parse(Float64,mb[4])
                smiles[c]=mb[5]
                inchi[c]=mb[6]
                cas[c]=mb[7]
                pubchem[c]=mb[8]
                inchikey[c]=mb[9]
                chemspider[c]=mb[10]
                ms_type[c]=mb[11]
                ion_s[c]=mb[12]
                ion_m[c]=mb[13]
                coll_e[c]=mb[14]
                res[c]=mb[15]
                splash[c]=mb[16]
                mz_val[c]=parse.(Float64,mb[17])
                mz_int[c]=parse.(Float64,mb[18])
                mz_int_r[c]=parse.(Float64,mb[19])
                d_type[c]=data_type

                global c +=1
            catch
                println("The file $path2file was skipped due to an error!")
                global c2 +=1

                #continue
            end




        end

    end


    MassBankJulia=Dict("ACCESSION" => access[1:c-1] , "NAME"=>name[1:c-1],
     "FORMULA"=>formula[1:c-1],"EXACT_MASS"=>mass[1:c-1],
     "SMILES"=>smiles[1:c-1],"INCHI"=>inchi[1:c-1],"CAS"=>cas[1:c-1],
     "PUBCHEM"=>pubchem[1:c-1],"INCHIKEY"=>inchikey[1:c-1],
     "CHEMSPIDER"=>chemspider[1:c-1],"MS_TYPE"=>ms_type[1:c-1],
     "IONIZATION"=>ion_s[1:c-1],"ION_MODE"=> ion_m[1:c-1],
     "COLLISION_ENERGY"=>coll_e[1:c-1], "SPLASH"=> splash[1:c-1],
    "MZ_VALUES"=> mz_val[1:c-1],"MZ_INT"=>mz_int[1:c-1],
    "MZ_INT_REL"=>mz_int_r[1:c-1],"RESOLUTION"=>res[1:c-1],
     "DATA_TYPE"=>d_type[1:c-1])

    println("We were not able to import $c2 files into MassBankJulia!")

    jldopen("MassBankJulia.jld", "w") do file
        write(file, "MassBankJulia", MassBankJulia)  # alternatively, say "@write file A"
    end

    println("The final file is saved in the current directory.")
    return MassBankJulia

end # function


#################################################################
# Add to library

# spec2add=libr[1]

function lib_add!(lib,path2metadata,spec2add,mode)
    m_data=CSV.read(path2metadata)

    m=["0","0","0"]
    MZ_VALUES=[[],[],[]]
    MZ_INT=[[],[],[]]
    MZ_INT_REL=[[],[],[]]
    k=collect(keys(spec2add))
    for i=1:length(k)
        if k[i]=="6"
            m[1]="6"
            MZ_VALUES[1]=spec2add["6"]["ms"]
            MZ_INT[1]=spec2add["6"]["int"]
            MZ_INT_REL[1]=spec2add["6"]["int"]
        elseif k[i]=="20"
            m[2]="20"
            MZ_VALUES[2]=spec2add["20"]["ms"]
            MZ_INT[2]=spec2add["20"]["int"]
            MZ_INT_REL[2]=spec2add["20"]["int"]
        elseif k[i]=="40"
            m[3]="40"
            MZ_VALUES[3]=spec2add["40"]["ms"]
            MZ_INT[3]=spec2add["40"]["int"]
            MZ_INT_REL[3]=spec2add["40"]["int"]
        end
    end

    n_spec2add=length(m[m .!= "0"])

    ACCESSION=repeat([spec2add["accession"]],n_spec2add)
    SMILES=repeat([spec2add["smils"]],n_spec2add)
    EXACT_MASS=repeat([spec2add["mass"]],n_spec2add)
    FORMULA=repeat([spec2add["formula"]],n_spec2add)
    INCHI_KEY=repeat([spec2add["INCHI_KEY"]],n_spec2add)
    name_chem=m_data.Preferred_Name[m_data[:MS_Ready_DTXCID] .== spec2add["accession"][:]]
    NAME=repeat([name_chem[1]],n_spec2add)
    CAS=repeat(m_data.CASRN[m_data[:Preferred_Name] .== name_chem[1]],n_spec2add)
    MS_TYPE=repeat(["MS2"],n_spec2add)
    IONIZATION=repeat(["ESI"],n_spec2add)
    ION_MODE=repeat([mode],n_spec2add)
    DATA_TYPE=repeat(["EST"],n_spec2add)
    INCHI=repeat(["NA"],n_spec2add)
    PUBCHEM=repeat(["NA"],n_spec2add)
    SPLASH=repeat(["NA"],n_spec2add)
    RESOLUTION=repeat(["NA"],n_spec2add)
    CHEMSPIDER=repeat(["NA"],n_spec2add)
    COLLISION_ENERGY=m
    #MZ_VALUES=[spec2add["6"],
    #MZ_INT=spec2add[8]
    #MZ_INT_REL=spec2add[8]

    parname=["ACCESSION" , "NAME","FORMULA","EXACT_MASS","SMILES","INCHI","CAS",
     "PUBCHEM","INCHIKEY","CHEMSPIDER","MS_TYPE","IONIZATION","ION_MODE",
     "COLLISION_ENERGY", "SPLASH","MZ_VALUES","MZ_INT","MZ_INT_REL","RESOLUTION",
     "DATA_TYPE"]

     par=[ACCESSION , NAME,FORMULA,EXACT_MASS,SMILES,INCHI,CAS,
      PUBCHEM,INCHI_KEY,CHEMSPIDER,MS_TYPE,IONIZATION,ION_MODE,COLLISION_ENERGY,
      SPLASH,MZ_VALUES,MZ_INT,MZ_INT_REL,RESOLUTION,DATA_TYPE]

    for i=1:length(parname)
        lib[parname[i]]=add_par2lib!(lib,parname[i],par[i])
    end

    #println("Compound " * name_chem[1] * " is added to MassBankJulia.")

    #lib=jldopen(path2lib,"w")

    return lib

end # function

# i=1

###############################################################
# Add par to lib

function add_par2lib!(lib,parname2add,par2add)

        #lib=load(path2lib,"MassBankJulia")
        tem_par=lib[parname2add]
        n_par2add=length(par2add)
        new_tem_par=Array{Any}(undef,length(tem_par)+n_par2add)
        new_tem_par[1:length(tem_par)]=tem_par
        new_tem_par[length(tem_par)+1:end]=par2add

    return new_tem_par
end # function


#lib1=load(path2lib)

################################################################
# python parsing through the file


py"""

def line_record(path2Comptox):
    fh = open(path2Comptox)
    c = 0
    spec = []
    while True:
        line = fh.readline()
        #print(c)
        if c == 0 and len(line) > 1 and line[0:4] == '# Da':
            tv1 = []
            tv1.append(line)
            c = 1
            #print(line)
        elif  c == 1 and len(line) > 1 and line[0:4] != '# Da':
            tv1.append(line)

        elif c == 1 and len(line) == 1:
            spec.append(tv1)
            c = 0

        if not line:
            break

    fh.close()

    return spec
"""



#################################################################
# Second python function for library entery generation


py"""

def lib_entry_gen(sp):
    tv1 = sp
    d = dict()
    d["accession"] = tv1[3][10:-1]
    print(tv1[3][10:-1])
    d["smils"] = tv1[4][10:-1]
    d["mass"] = float(tv1[5][8:-1])
    d["formula"] = tv1[6][11:-1]
    d["INCHI_KEY"] = tv1[7][13:-1]
    d['6'] = dict()
    d['20'] = dict()
    d['40'] = dict()

    gl = 0
    s1 = []
    s2 = []
    s3 = []

    int1 = []
    int2 = []
    int3 = []

    for i in range(8,tv1.__len__()):
        if gl == 0 and tv1[i][0:7] == 'energy0':
            gl = 1
        elif gl == 1 and tv1[i][0:6] != 'energy':
            tv2 = tv1[i]
            ind = [l for l, k in enumerate(tv2) if k == ' ']
            s1.append( float(tv2[0:ind[0]-1]))
            int1.append( float(tv2[ind[0]+1:ind[1]-1]))
        elif gl == 1 and tv1[i][0:7] == 'energy1':
            gl = 2

        elif gl == 2 and tv1[i][0:6] != 'energy':
            tv2 = tv1[i]
            ind = [l for l, k in enumerate(tv2) if k == ' ']
            s2.append(float(tv2[0:ind[0]-1]))
            int2.append(float(tv2[ind[0]+1:ind[1]-1]))
        elif gl == 2 and tv1[i][0:7] == 'energy2':
            gl = 3

        elif gl == 3:
            tv2 = tv1[i]
            ind = [l for l, k in enumerate(tv2) if k == ' ']
            s3.append(float(tv2[0:ind[0]-1]))
            int3.append(float(tv2[ind[0]+1:ind[1]-1]))

    d['6']['ms'] = s1
    d['6']['int'] = int1

    d['20']['ms'] = s2
    d['20']['int'] = int2

    d['40']['ms'] = s3
    d['40']['int'] = int3
    return d

"""





###############################################################
# Wrapping function

py"""
def wrapper_fun(spec):
    lib = list(map(lib_entry_gen,spec))
    return lib

"""





#################################################################
# Comptox dataset to MassBank



function add_comptox2mb!(path2Comptox,path2metadata,path2lib,mode)



    #numspec=spec_count(path2Comptox)
    #println("The total number of spectra is counted.")
    #specs=spec_sep(path2Comptox,numspec)
    #println("All the spectra are being pre-processed.")
    #lib_ent=lib_entery_gen(specs)
    println("The spectra are being parsed through.")
    spec = py"line_record"(path2Comptox)
    println("The dataset has been parsed through.")
    libr = py"wrapper_fun"(spec)
    GC.gc()
    println("The library enteries have been generated.")

    println("The local library is being loaded.")
    lib=load(path2lib,"MassBankJulia")
    println("MassBankJulia has been successfully imported.")

    for i=1:length(libr)

        lib=lib_add!(lib,path2metadata,libr[i],mode)
        println(100*i/length(libr))
    end


    println("The final file is being saved in the current directory.")

    jldopen("MassBankJulia.jld", "w") do file
        write(file, "MassBankJulia", lib)  # alternatively, say "@write file A"
    end

    println("The MassBankJulia is saved in the current directory.")

    return lib


end # function


#################################################################
# Add SusDat to MassBank

function SusDat2mb(path2mb,path2susdat,data_type)

    body
end # function



#################################################################
# Test area


#path2file="/home/saer/Google Drive/EA000401.txt"

path2dir="C:\\Temp_File\\MassBank\\MassBank-data-master"
data_type="EXP"

MassBankJulia=massbank_julia(path2dir,data_type)



# Test adding Comptox

#path2Comptox="/home/saer/Desktop/TestData_julia/ESI-MSMS-pos-2018-Oct12.dat"
#path2metadata="/home/saer/Desktop/TestData_julia/CFM-ID_metadata_DTXCID.csv"
#path2lib="MassBankJulia.jld"
#mode="POSITIVE"

#@time lib=add_comptox2mb!(path2Comptox,path2metadata,path2lib,mode)
