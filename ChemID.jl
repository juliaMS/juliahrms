module ChemID
version= "0.1.0.0"

using MySQL
using DBInterface
using DataFrames
using CSV
using XLSX
using Statistics
using LinearAlgebra
using JSON

# cd("/Users/saersamanipour/Desktop/dev/ongoing/juliahrms/")
# push!(LOAD_PATH,"/Users/saersamanipour/Desktop/dev/ongoing/juliahrms/");

export featureID_comp

###############################################################################
# Start the module


# run this for ssh
localPort = 3307
#user = "ec2-user"
#server = "ec2-3-80-213-154.compute-1.amazonaws.com"
#key = "/Users/saersamanipour/Desktop/dev/Data/prod_process.pem"
#key = "prod_process.ppk"

#chmod 400 prod_process.pem

#print(string("ssh -N -L ",localPort,":127.0.0.1:3306 ",user,"@",server," -i "*key*" -o \"StrictHostKeyChecking no\" -f"))
#run this in shell
function select_lib_sql(par_ionization,par_ion_mode,par_min_mass,par_max_mass)


    query = "SELECT CAS,`NAME`,MZ_INT, DATA_TYPE,EXACT_MASS,MZ_VALUES,FORMULA from final_table_unrefined "*
    "where IONIZATION = '".*par_ionization*"' and ION_MODE = '"*par_ion_mode*"'"
    if !isnothing(par_min_mass)
        query=query*" and EXACT_MASS >= "*string(par_min_mass)
    end
    if !isnothing(par_max_mass)
        query=query*" and EXACT_MASS <= "*string(par_max_mass)
    end
    con = DBInterface.connect(MySQL.Connection,"127.0.0.1","local_user" , "54830sdDS3o#@";port=localPort,db="chemBase")
    res = DBInterface.execute(con,query)
    DBInterface.close!(con)
    cas=Array{Any}(undef,res.nrows)
    name=Array{Any}(undef,res.nrows)
    mz_int=Array{Any}(undef,res.nrows)
    d_type=Array{Any}(undef,res.nrows)
    mass=Array{Any}(undef,res.nrows)
    mz_val = Array{Any}(undef,res.nrows)
    formula = Array{Any}(undef,res.nrows)

    i=1
    for row in res
        tmp_cas = if  !ismissing(row[1]);row[1]; else "NA";end
        cas[i]= if  !ismissing(row[1]);row[1]; else "NA";end
        name[i]= if  !ismissing(row[2]);JSON.parse(row[2]); else "NA";end
        mz_int[i]= if  !ismissing(row[3]);JSON.parse(row[3]); else "NA";end
        d_type[i]=row[4]
        mass[i]=row[5]
        mz_val[i]= if  !ismissing(row[6]);JSON.parse(row[6]); else "NA";end
        formula[i] = row[7]
        i=i+1
    end

    filtered_lib=Dict("CAS"=>cas,"NAME"=>name,"MZ_INT"=>mz_int,"EXACT_MASS"=>mass,"DATA_TYPE"=>d_type,"MZ_VALUES"=>mz_val,"FORMULA"=>formula)

    return filtered_lib
end



############################################################################
#
# Wrapping function to do id based on components

function featureID_comp(mode,source,path2comp,weight_f)



    spec_list=CSV.File(path2comp) |> DataFrame!
    global final_table=zeros(1,17)

    # i=7
    for i=1:size(spec_list,1)

        id=spec_list[i,:].Nr
        rt=spec_list[i,:].Rt

        mass_tol=(spec_list[i,:].MaxMass-spec_list[i,:].MinMass)

        if spec_list.Parent[i]==1.0
            ms=parse(Float64,spec_list[i,:].AccuMass)
            min_mass=ms - mass_tol
            max_mass=ms + mass_tol

            spec=spec_list[i,:]
            ms2val,ms2int=comp_convert(spec)

            selected_lib=select_lib_sql(source,mode,min_mass,max_mass)

            if length(selected_lib["CAS"])>0
                rep=entery_match(selected_lib,ms2val,ms2int,mass_tol,id,rt,ms,weight_f)

                if length(rep)>0
                    global final_table=vcat(final_table,rep)
                end
            end

        elseif spec_list.Parent[i]==0


            if mode == "POSITIVE"
                 ms=spec_list.MeasMass[i]- 1.007825
            elseif mode == "NEGATIVE"
                  ms=spec_list.MeasMass[i]- 1.007825
            end
            min_mass=ms - mass_tol
            max_mass=ms + mass_tol

            selected_lib=select_lib_sql(source,mode,min_mass,max_mass)
            #print(selected_lib)
            rep=mf_match(selected_lib,id,rt,ms)
            if length(rep)>0
                global final_table=vcat(final_table,rep)
            end


        end

    end

    table=DataFrame(final_table[2:end,:],[:ID,:Rt,:MS1Mass,:Name,:RefMatchFrag,:UsrMatchFrag,
    :MS1Error,:MS2Error,:MS2ErrorStd,:DirectMatch,:ReversMatch,:FinalScore,
    :SpecType,:MatchedFrags,:Cas,:FragMZ,:FragInt])

    sort!(table,[:ID,:FinalScore],rev=(false, true))
    m=basename(path2comp)

    output=joinpath(dirname(path2comp),m[1:end-4]) * "_IDs.csv"
    CSV.write(output,table)

    return table

end # function




############################################################################
# Convert the MS1 and MS2 spectra in one vector

 function comp_convert(spec)

    if spec.MS1Comp != "0"

        ms1_val=parse.(Float64,split(spec.MS1Comp[2:end-1],", "))
        ms1_int=parse.(Float64,split(spec.MS1CompInt[2:end-1],", "))
    else
        ms1_val=0
        ms1_int=0

    end

    if spec.MS2Comp != "0"

        ms2_val=parse.(Float64,split(spec.MS2Comp[2:end-1],", "))
        ms2_int=parse.(Float64,split(spec.MS2CompInt[2:end-1],", "))
    else
        ms2_val=0
        ms2_int=0
    end

    ms2val=vcat(ms1_val,ms2_val)
    ms2val=ms2val[ms2val .>0]
    ms2int=vcat(ms1_int,ms2_int)
    ms2int=ms2int[ms2int .>0]

    return(ms2val,ms2int)


end


############################################################################
# Fragment entreies


function entery_match(selected_lib,frag_mz,frag_int,mass_tol,id,rt,ms,weight_f)

    n_ref=size(selected_lib["EXACT_MASS"],1)

    rep=Array{Any}(undef,n_ref,17)
    # i=1

    for i=1:n_ref

        mz_ref=if selected_lib["MZ_VALUES"][i] != "NA";copy(selected_lib["MZ_VALUES"][i]); else []; end
        int_ref=if selected_lib["MZ_INT"][i] != "NA";copy(selected_lib["MZ_INT"][i]); else []; end
        mz_user=copy(frag_mz)
        int_user=copy(frag_int)
        ref_mzs,n_ref_match,ref_match_e_av,ref_match_e_std=frag_match(mz_ref[mz_ref .> 0],mz_user,mass_tol)

        if n_ref_match>0
            master_vect_f=master_mass_gen(mz_ref,int_ref,mz_user,int_user,mass_tol)
            direct_m,revers_m=dot_prod(master_vect_f)
        else
            direct_m=0
            revers_m=0
        end

        ms1_e=selected_lib["EXACT_MASS"][i]-ms
        scors=score_calc(mz_ref,mz_user,n_ref_match,ref_match_e_av,
            ref_match_e_std,ms1_e,mass_tol,weight_f)
        final_score=round(sum(scors)+weight_f[6]*direct_m+weight_f[7]*revers_m,digits=2)
        rep[i,1]=id
        rep[i,2]=rt
        rep[i,3]=ms
        rep[i,4]=selected_lib["NAME"][i][1]
        rep[i,5]=string(n_ref_match)*"--"*string(length(mz_ref))
        rep[i,6]=string(n_ref_match)*"--"*string(length(mz_user))
        rep[i,7]=round(ms1_e,digits=3)
        rep[i,8]=round(ref_match_e_av,digits=3)
        rep[i,9]=round(ref_match_e_std,digits=3)
        rep[i,10]=direct_m
        rep[i,11]=revers_m
        rep[i,12]=final_score
        rep[i,13]=selected_lib["DATA_TYPE"][i]
        rep[i,14]=string(ref_mzs)
        rep[i,15]=selected_lib["CAS"][i]
        rep[i,16]=string(frag_mz)
        rep[i,17]=string(frag_int)


    end



    return(rep)

end





############################################################################
# Fragment matching
#

function frag_match(mz_ref,mz_user,mass_tol)

    ref_match=zeros(length(mz_ref))
    ref_match_e=zeros(length(mz_ref))

    mz_user_1=copy(mz_user)



    for i=1:length(mz_ref)
        tv1=abs.(mz_ref[i] .- mz_user_1 )
        tv2=findall(x -> x <= mass_tol/2, tv1)
        if length(tv2[tv2 .> 0]) > 0
            ref_match[i]=1
            mz_user_1[tv2] .=0
            ref_match_e[i] = mean(tv1[tv2])
        end
    end



    ref_mzs=mz_ref[ref_match .> 0]
    n_ref_match=sum(ref_match)
    ref_match_e_av=mean(ref_match_e)
    ref_match_e_std=std(ref_match_e)


    return(ref_mzs,n_ref_match,ref_match_e_av,ref_match_e_std)

end


############################################################################
# master mass list generator
#

function master_mass_gen(mz_ref,int_ref,mz_user,int_user,mass_tol)

    mz_values=vcat(mz_ref[:],mz_user[:])
    master_vect=zeros(length(vcat(mz_ref[:],mz_user[:])),3)

    for i=1:size(master_vect,1)
        tv1=abs.(mz_values[i] .- mz_values)
        tv2=findall(x -> x <= mass_tol/2, tv1)
        master_vect[i,1]=mean(mz_values[tv2])
        mz_values[tv2].=0
    end

    for i=1:size(master_vect,1)
        if master_vect[i] > 0
            #println(i)
            tv1=abs.(master_vect[i] .- mz_ref)
            tv2=findall(x -> x <= mass_tol/2, tv1)
            if length(tv2)>0
                master_vect[i,2]=maximum(int_ref[tv2])
                int_ref[tv2] .=0
            end

            ttv1=abs.(master_vect[i] .- mz_user)
            ttv2=findall(x -> x <= mass_tol/2, ttv1)
            if  length(ttv2) >0
                if maximum(ttv2) <= length(int_user)
                    master_vect[i,3]=maximum(int_user[ttv2])
                    int_user[ttv2] .=0
                elseif maximum(ttv2) > length(int_user)

                    tv3 = zeros(size(int_user))
                    for j = 1:length(ttv2)
                        if ttv2[j] > length(int_user)
                            ttv2[j]=0
                        end

                    end
                    master_vect[i,3]=maximum(int_user[ttv2[ttv2 .>0]])
                    int_user[ttv2[ttv2 .>0]] .=0
                end
            end
        end


    end


    master_vect_f=master_vect[master_vect[:,1] .> 0,:]

    return master_vect_f
end



############################################################################
# Dot product calculations
#

function dot_prod(master_vect_f)

    norm_ref_spec=(master_vect_f[:,1] .* sqrt.(master_vect_f[:,2])) ./ sum(master_vect_f[:,1] .* sqrt.(master_vect_f[:,2]))
    norm_user_spec=(master_vect_f[:,1] .* sqrt.(master_vect_f[:,3])) ./ sum(master_vect_f[:,1] .* sqrt.(master_vect_f[:,3]))

    dot_ref=dot(norm_ref_spec,norm_ref_spec)
    dot_user=dot(norm_user_spec,norm_user_spec)

    dot_direct=dot(norm_user_spec,norm_ref_spec)
    if dot_direct>0
        direct_m=round(1-abs(dot_ref-dot_direct),digits=2)
        revers_m=round(1-abs(dot_user-dot_direct),digits=2)
    else
        direct_m=0
        revers_m=0
    end

    return(direct_m,revers_m)
end


############################################################################
# Matching score calculator
#

function score_calc(mz_ref,mz_user,n_ref_match,ref_match_e_av,
    ref_match_e_std,ms1_e,mass_tol,weight_f)

    if n_ref_match > 0
        s_match_frag_ref=round(weight_f[1]*(n_ref_match/length(mz_ref)),digits=2)
        s_match_frag_usr=round(weight_f[2]*(n_ref_match/length(mz_user)),digits=2)

        s_ms2_e=abs(weight_f[4]*round((abs(ref_match_e_av)-(0.5*mass_tol))/(0.5*mass_tol),digits=2))
        s_ms2_std=abs(weight_f[5]*round((abs(ref_match_e_std)-(0.5*mass_tol))/(0.5*mass_tol),digits=2))
        if isnan(s_ms2_std)==1
            s_ms2_std=0
        end
    else
        s_match_frag_ref=0
        s_match_frag_usr=0
        s_ms2_e=0
        s_ms2_std=0
    end

    s_ms1_e=abs(weight_f[3]*round((abs(ms1_e)-(mass_tol))/(mass_tol),digits=2))


    return([s_match_frag_ref,s_match_frag_usr,s_ms1_e,s_ms2_e,s_ms2_std])
end

############################################################################
# Molecular formula match

function mf_match(selected_lib,id,rt,ms)

    ms1e= ms .- selected_lib["EXACT_MASS"]

    n_ref=size((selected_lib["FORMULA"]),1)

    rep=Array{Any}(undef,n_ref,17)
    # i=1
    rep[:,1] .=id
    rep[:,2] .=rt
    rep[:,3] .=ms
    rep[:,4] =selected_lib["FORMULA"]
    rep[:,5] .="NA"
    rep[:,6] .="NA"
    rep[:,7] =round.(ms1e,digits=3)
    rep[:,8] .="NA"
    rep[:,9] .="NA"
    rep[:,10] .="NA"
    rep[:,11] .="NA"
    rep[:,12] =if length(ms1e) > 0;1 .- abs.(ms1e/maximum(ms1e));else [];end
    rep[:,13]=selected_lib["DATA_TYPE"]
    rep[:,14] .="NA"
    rep[:,15] .="NA"
    rep[:,16] .="NA"
    rep[:,17] .="NA"


    return(rep)
end # function



###############################################################################
# test area

path2comp="/Users/saersamanipour/Desktop/UvA/Temp_files/Phil/Drug cal/mzxml_files/50 PPB DRUGS_report_comp.csv"
weight_f=[1,1,1,1,1,1,1]
mode="POSITIVE"
source="ESI"


#table=featureID_comp(mode,source,path2comp,weight_f)



###############################################################################

end # end of module
