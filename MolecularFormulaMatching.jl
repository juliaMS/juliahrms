module MolecularFormulaMatching

using CSV
using DataFrames
using Glob
using Statistics
using XLSX

export MF_match_list, MF_match_list_batch

##########################################################################
## Testing area
#path2file="/media/saer/Data/Data/1000Lakes/Reports_filtered/TRU200116_01201_report_KMD_filtered.csv"
#path2folder="/media/saer/Data/Data/1000Lakes/Reports_filtered"
#path2list="PFAS_Curated.xlsx"
#mass_tol=0.01
#mode="NEGATIVE"


#list = DataFrame(XLSX.readtable(path2list, "Sheet1")...)

#table=MF_match_list(path2file,list,mass_tol,mode)
#MF_match_list_batch(path2folder,list,mass_tol,mode)

##########################################################################
## find candidates for each feature

function MF_find(list,mass_tol,masses)

    candidates=Array{Any}(undef,length(masses))
    mass_dif=Array{Any}(undef,length(masses))
    ss=zeros(1,length(masses))

    for i=1:length(masses)
        m_dif=masses[i] .- list.MONOISOTOPIC_MASS
        if size(list[abs.(m_dif) .<= mass_tol,:],1)>0
            ss[i]= size(list[abs.(m_dif) .<= mass_tol,:],1)
        end
        candidates[i]=list[abs.(m_dif) .<= mass_tol,:]
        mass_dif[i]=m_dif[abs.(m_dif) .<= mass_tol,:]

    end

    return(ss,candidates,mass_dif)

end


##########################################################################
## group the candidates

function candicate_grouping(ss,candidates,mass_dif,feature,ad_list)

    MFs=repeat(["a"],Int(sum(ss)))
    Obs_mass=zeros(1,Int(sum(ss)))
    delta_m=zeros(1,Int(sum(ss)))
    data_source=zeros(1,Int(sum(ss)))
    aduct_ind=zeros(1,Int(sum(ss)))


    global c=1
    for i=1:length(ss)
        if ss[i]>0
            can_list=candidates[i]
            for j=1:size(can_list,1)
                #j=1
                MFs[c]=can_list.MOLECULAR_FORMULA[j]
                Obs_mass[c]=can_list.MONOISOTOPIC_MASS[j]
                data_source[c]=can_list.DATA_SOURCES[j]
                delta_m[c]=mass_dif[i][j]
                aduct_ind[c]=i
                global c=c+1

            end
        end

    end


    #rep=[MFs,Obs_mass,delta_m,aduct_ind,data_source]
    #rep=[unique(MFs),unique(Obs_mass),unique(delta_m),aduct_ind[1][2],unique(data_source)]
    MFs1=unique(MFs)
    #Obs_mass=unique(Obs_mass)
    #delta_m=unique(delta_m)
    aduct_ind=unique(aduct_ind)

    data_source1=zeros(1,length(MFs1))
    Obs_mass1=zeros(1,length(MFs1))
    delta_m1=zeros(1,length(MFs1))


    for i=1:length(MFs1)

        #i=2
        #println(i)
        data_source1[i]=sum(data_source[MFs .== MFs1[i]])
        Obs_mass1[i]=Obs_mass[findfirst(x -> x == MFs1[i], MFs)]
        delta_m1[i]=delta_m[findfirst(x -> x == MFs1[i], MFs)]
    end


    df = DataFrame()
    df.Nr = feature.Nr .* ones(length(MFs1))
    df.ScanNum = feature.ScanNum .* ones(length(MFs1))
    df.ScanInPeak = feature.ScanInPeak .* ones(length(MFs1))
    df.Rt = feature.Rt .* ones(length(MFs1))
    df.SecInPeak = feature.SecInPeak .* ones(length(MFs1))
    df.MeasMass = feature.MeasMass .* ones(length(MFs1))
    df.MinMass = feature.MinMass .* ones(length(MFs1))
    df.MaxMass = feature.MaxMass .* ones(length(MFs1))
    df.Area = feature.Area .* ones(length(MFs1))
    df.Int = feature.Int .* ones(length(MFs1))
    df.FeatPurity = feature.FeatPurity .* ones(length(MFs1))
    df.MediRes = feature.MediRes .* ones(length(MFs1))
    df.KMD = feature.KMD .* ones(length(MFs1))
    df.MolecularFormula = MFs1
    df.ObservedMass = Obs_mass1[:]
    df.MassError = round.(delta_m1[:]; digits=3)
    #df.AductMass = ad_list[Int.(aduct_ind)]
    df.DataSource = data_source1[:]

    sort!(df,[:MassError, :DataSource],rev = (true,false))


    return(df)

end



##########################################################################
## Function for MF assignment for one feature list

function MF_match_list(path2file,list,mass_tol,mode)

    pos_add_m=[1.007825,18.033823,22.989218,33.033489,38.963158,42.033823,
        44.971160,61.065340,64.015765,76.919040,79.021220,83.060370,84.055110]

    neg_add_m=[19.01839,1.007276,-20.974666,-34.969402,-36.948606,-44.998201,
        -59.013851,-78.918885,-112.985586]

    feature_list=CSV.read(path2file)


    global final_table=DataFrame(zeros(1,17),[:Nr,:ScanNum,:ScanInPeak,:Rt,:SecInPeak,
    :MeasMass,:MinMass,:MaxMass,:Area,:Int,:FeatPurity,:MediRes,:KMD,:MolecularFormula,
    :ObservedMass,:MassError,:DataSource])

    for i=1:size(feature_list,1)
        #println(i)
        #i=1
        feature=feature_list[i,:]

        if mode=="POSITIVE"
            masses=feature.MeasMass .- pos_add_m
            ad_list=pos_add_m

        elseif mode=="NEGATIVE"
            masses=feature.MeasMass .+ neg_add_m
            ad_list=neg_add_m

        end


        #final_table=zeros(1,5)
        ss,candidates,mass_dif=MF_find(list,mass_tol,masses)
        df=candicate_grouping(ss,candidates,mass_dif,feature,ad_list)

        if size(df,1)>0

            global final_table=vcat(final_table,df)

        end


    end

    m=split(path2file,".")
    output= m[1] * "_W_MFs.csv"
    CSV.write(output,final_table[2:end,:])

    return final_table[2:end,:]

end



##########################################################################
## Function for MF assignment batch


function MF_match_list_batch(path2folder,list,mass_tol,mode)

    dir_name=readdir(path2folder)
    for i=1:size(dir_name,1)
        #i=5
        path2file=joinpath(path2folder,dir_name[i])
        MF_match_list(path2file,list,mass_tol,mode)
        println("The file "* dir_name[i] *" has been processed and saved.")
    end


end






end # end of the module
