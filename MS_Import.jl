module MS_Import

using Base

using PyCall
using BenchmarkTools
using Plots
using NCDatasets
#using HDF5
#using LightXML

export import_files, import_files_MS1, Import_files_batch_MS1

#cd("/")
###############################################################################
## CDF import

function readCDF(File)
    DD=Dict()
    #File="D:\\Data\\julia\\Data_test_juliaHRMS\\Chrom_batch\\C1\\VANN-DANMARK01.CDF"
    ds = Dataset(File)

    scan_acquisition_time=ds["scan_acquisition_time"]
    scan_duration=ds["scan_duration"]                                           #The duration of each scan
    inter_scan_time=ds["inter_scan_time"]                                       #The time between scans
    total_intensity=ds["total_intensity"]                                       #TIC
    mass_range_min=ds["mass_range_min"]                                         #min mass range
    mass_range_max=ds["mass_range_max"]                                         #max mass range
    scan_index=ds["scan_index"]
    point_count=ds["point_count"]
    mass_values = ds["mass_values"]                                             #m/z values
    intensity_values=ds["intensity_values"]                                     #intensity at each m/z value

    #defining the starting point and the end point of the chromatogram
    t0=scan_acquisition_time[1]/60                                              #min
    t_end=scan_acquisition_time[end]/60                                         #min

    Max_Mz_displacement=maximum(point_count)
    Mz_values=zeros(length(scan_index),Max_Mz_displacement)
    Mz_intensity=zeros(length(scan_index),Max_Mz_displacement)

    for i=1:length(scan_index)-1
        if (scan_index[i+1] < size(mass_values,1))
            Mz_values[i,1:point_count[i]]=mass_values[scan_index[i]+1:scan_index[i+1]]
            Mz_intensity[i,1:point_count[i]]=intensity_values[scan_index[i]+1:scan_index[i+1]]
        else
            break
        end
        #println(i)
    end

    DD["total_intensity"]=total_intensity
    DD["t0"]=t0
    DD["t_end"]=t_end
    DD["Mz_values"]=Mz_values
    DD["Mz_intensity"]=Mz_intensity
    DD["scan_duration"]=scan_duration

    return DD


end



##############################################################################
## The warper function


function cdf_Import(pathin,filenames)
    chrom=Dict()
    for i=1:length(filenames)

        File=joinpath(pathin,filenames[i])
        chrom["MS$i"]=readCDF(File)

    end

    return chrom

end

########################################################################
# Array to matrix
#

function array2mat(array,dims,cond,ms_l)
    dummy_var=zeros(dims[1],dims[2])
    global c=1
    for i=1:length(ms_l)
        if ms_l[i]==cond
            dummy_var[c,1:length(array[i])]=array[i]
            #mass[k][i]=[]
            #GC.gc()
            global c += 1
        end
    end
    GC.gc()
    return dummy_var

end



########################################################################
# Read mzXML python
#

function __init__()


py"""
import pyopenms as pm
import gc
import numpy as np

def read_mzxml_py(path2file,mz_thresh):
    Mz_Values=[]
    Mz_Intensity=[]
    Rt = []
    ms_l = []
    s = []
    exp = pm.MSExperiment()
    pm.MzXMLFile().load(path2file,exp)
    m=exp.getNrSpectra()
    for i in range(0,m):
        spec = exp[i]
        Mz, Int = spec.get_peaks()
        if mz_thresh[0] > 0 and mz_thresh[1] > 0:
            indd=np.where((Mz>=mz_thresh[0]) & (Mz<=mz_thresh[1]))
            Mz_Values.append(Mz[indd])
            Mz_Intensity.append(Int[indd])
            ms_l.append(spec.getMSLevel())
            Rt.append( spec.getRT())
            s.append(len(Int[indd]))
        elif mz_thresh[0] == 0 and mz_thresh[1] > 0:
            indd=np.where(Mz<=mz_thresh[1])
            Mz_Values.append(Mz[indd])
            Mz_Intensity.append(Int[indd])
            ms_l.append(spec.getMSLevel())
            Rt.append( spec.getRT())
            s.append(len(Int[indd]))
        elif mz_thresh[0] > 0 and mz_thresh[1] == 0:
            indd=np.where(Mz>=mz_thresh[0])
            Mz_Values.append(Mz[indd])
            Mz_Intensity.append(Int[indd])
            ms_l.append(spec.getMSLevel())
            Rt.append( spec.getRT())
            s.append(len(Int[indd]))

        elif mz_thresh[0] == 0 and mz_thresh[1] == 0:
            Mz_Values.append(Mz)
            Mz_Intensity.append(Int)
            ms_l.append(spec.getMSLevel())
            Rt.append( spec.getRT())
            s.append(len(Int)) 

        else:
            print("Please make sure that the thresholds are set correctly!")
            break


    del Mz
    del Int
    del exp
    del spec
    gc.collect()
    return(Mz_Values, Mz_Intensity, Rt, ms_l, s)
"""


end



###########################################################################
#
# mzXML import julian




function mzxml_import(path2file,mz_thresh)

    chrom=Dict()

    Mz, int, rt, ms_l, s = py"read_mzxml_py"(path2file,mz_thresh)
    mass=[Mz,int]
    Mz=nothing
    int=nothing
    GC.gc()
    n_l=maximum(ms_l)
    par=["Mz_values","Mz_intensity"]


    for j=1:n_l
        chrom["MS$j"] = Dict("Rt" => rt[ms_l .== j],
        "Mz_values"=> Array{Float64,2}(undef,1,1),
        "Mz_intensity"=>Array{Float64,2}(undef,1,1))
        for k=1:length(par)
            m_l_i=ms_l[ms_l .== j]
            s_l=s[ms_l .== j]
            dims=[length(m_l_i),maximum(s_l)]
            #println(k)

            chrom["MS$j"][par[k]]= array2mat(mass[k],dims,j,ms_l)
            GC.gc()
        end
    end





    return chrom

end






#####################################################################
## Wrapper to import the files

function import_files(format,pathin,filenames,mz_thresh)


    if format == "cdf"

        chrom=cdf_Import(pathin,filenames)


    elseif format == "mzxml"
        #MS_Level=length(namechan)
        if isa(filenames,Array)==1
            path_in=joinpath(pathin,filenames[])
        else
            path_in=joinpath(pathin,filenames)
        end


        chrom = mzxml_import(path_in,mz_thresh)

    end

    return (chrom)

end




##########################################################################
# MS1 import

function import_files_MS1(format,pathin,filenames,mz_thresh)

    if isa(filenames,Array)==1
        m=split(filenames[1],".")
    else
        m=split(filenames,".")
    end

    chrom=import_files(format,pathin,filenames,mz_thresh)

    if format == "cdf"


        mz_vals=chrom["MS1"]["Mz_values"]
        mz_int=chrom["MS1"]["Mz_intensity"]
        t0=chrom["MS1"]["t0"][1]
        t_end=chrom["MS1"]["t_end"][1]

    elseif format == "mzxml"
        #MS_Level=length(namechan)
        if isa(filenames,Array)==1
            path_in=pathin * "\\" * filenames[]
        else
            path_in=pathin * "\\" * filenames
        end

        #mzxml_import(path,num_channel,MS_level,mz_thresh)
        mz_vals=chrom["MS1"]["Mz_values"]
        mz_int=chrom["MS1"]["Mz_intensity"]
        t0=chrom["MS1"]["Rt"][2]/60
        t_end=chrom["MS1"]["Rt"][end]/60

    end

    return (mz_vals,mz_int,t0,t_end,m,pathin)

end

######################################################################
## Import function batch

function Import_files_batch_MS1(format,pathin,mz_thresh)
    Dname=readdir(pathin)
    mz_vales_mat=Array{Any}(undef,size(Dname,1))
    mz_int_mat=Array{Any}(undef,size(Dname,1))
    t0_mat=Array{Float64}(undef,size(Dname,1))
    t_end_mat=Array{Float64}(undef,size(Dname,1))
    m_mat=Array{Any}(undef,size(Dname,1))
    pathin_mat=Array{String}(undef,size(Dname,1))

    for i=1:size(Dname,1)

        filenames=[Dname[i]]
        mz_vals,mz_int,t0,t_end,m,pathin=import_files_MS1(format,pathin,filenames,mz_thresh)
        mz_vales_mat[i]=mz_vals
        mz_int_mat[i]=mz_int
        t0_mat[i]=t0
        t_end_mat[i]=t_end
        m_mat[i]=m
        pathin_mat[i]=pathin

    end
    return (mz_vales_mat,mz_int_mat,t0_mat,t_end_mat,m_mat,pathin_mat)

end




##############################################################################


end # end of the module
