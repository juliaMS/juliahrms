using Distributed
addprocs(4)

@everywhere push!(LOAD_PATH,$"/media/saer/Data/Data/julia/juliahrms")
#@everywhere push!(LOAD_PATH,pwd())

#@everywhere include("FeatureDetection.jl")
@everywhere using SAFD
#@everywhere using FeatureDetection
@everywhere using MS_Import
#include("FeatureDetection.jl")
#using Main.FeatureDetection


###############################################################################


pathin="/Users/saersamanipour/Desktop/UvA/Temp_files/Phil/Drug cal/mzxml_files"
mz_thresh=[0,550]


max_numb_iter=20000
max_t_peak_w=400
res=20000
min_ms_w=0.02

r_thresh=0.75
min_int=500

sig_inc_thresh=5
S2N=2


min_peak_w_s=3

function feature_detect_batch(pathin,max_numb_iter,
    max_t_peak_w,res,min_ms_w,r_tresh,min_int,sig_inc_thresh,S2N,min_peak_w_s,mz_thresh)


    mz_vales_mat,mz_int_mat,t0_mat,t_end_mat,m_mat,pathin_mat=Import_files_batch_MS1(pathin,mz_thresh)

    Max_it=repeat([max_numb_iter],size(pathin_mat,1))
    Max_t_peak_w=repeat([max_t_peak_w],size(pathin_mat,1))
    Res=repeat([res],size(pathin_mat,1))
    Min_ms_w=repeat([min_ms_w],size(pathin_mat,1))
    R_thresh=repeat([r_thresh],size(pathin_mat,1))
    Min_int=repeat([min_int],size(pathin_mat,1))
    Sig_inc_thresh=repeat([sig_inc_thresh],size(pathin_mat,1))
    S2N1=repeat([S2N],size(pathin_mat,1))
    Min_peak_w_s=repeat([min_peak_w_s],size(pathin_mat,1))

    pmap(safd_s3D,mz_vales_mat,
    mz_int_mat,t0_mat,t_end_mat,m_mat,pathin_mat,Max_it,Max_t_peak_w,Res,
    Min_ms_w,R_thresh,Min_int,Sig_inc_thresh,S2N1,Min_peak_w_s;retry_delays = zeros(2))

     #pmap(FeatureDetection.feature_detect_signle_data,mz_vales_mat,
     #mz_int_mat,t0_mat,t_end_mat,m_mat,pathin_mat,Max_it,Max_t_peak_w,Res,
     #Min_ms_w,R_tresh,Min_int,Sig_inc_thresh,S2N1,Min_peak_w_s;
     # batch_size= 2, retry_delays = zeros(2))


end




@time feature_detect_batch(pathin,max_numb_iter,
    max_t_peak_w,res,min_ms_w,r_tresh,min_int,sig_inc_thresh,S2N,min_peak_w_s,mz_thresh)

#interrupt()
