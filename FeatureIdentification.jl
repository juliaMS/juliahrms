# __precompile__(false)

module FeatureIdentification

push!(LOAD_PATH,pwd())
###########################################
# Import dependencies

using CSV
using HDF5
using JLD
using BenchmarkTools
using Plots
using Statistics
using LinearAlgebra
using DataFrames

export featureID_single_feature_inter, featureID_spec_batch_inter,
    featureID_spec_single, filter_lib, selec_lib, featureID_comp_inter
############################################################################
#
# test area

#GC.gc()
#
#format="mzxml"
#pathin="C:\\Temp_File\\Jake"
#filenames=["180420-SWATH-JO054-006 Day5 C.mzXML"]
#mz_thresh=600


# Decon
#path2SWATH="C:\\Temp_File\\Jake\\SWATH_Info.csv"
#path2features="C:\\Temp_File\\Jake\\180420-SWATH-JO054-006 Day5 C_report.csv"
#path2spec="C:\\Temp_File\\Jake\\180420-SWATH-JO054-006 Day5 C_decon_features.csv"

## feature_list=CSV.read("C:\\Temp_File\\Jake\\180420-SWATH-JO054-006 Day5 C_report.csv")

#feature=feature_list[454,:] # this is an example for the structure of the feature. First run feature_list=CSV.read("C:\\Temp_File\\Jake180420-SWATH-JO054-006 Day5 C_report.csv")
#mass_win_per=0.8
#ret_win_per=0.25
#r_tresh=0.9
#min_int=500

#mass_tol=0.005

# Identification
#path2lib="MassBankJulia.jld"
#mode="POSITIVE"
#source="ESI"
#weight_f=[1,1,1,1,1,1,1]
#parent=1



#chrom=import_files(format,pathin,filenames,mz_thresh)

#frag_mz,frag_int,mass_tol=feature_decon_SWATH_singlefeature_internal(chrom,pathin,
#    filenames,path2SWATH,r_tresh,min_int,feature,mass_win_per,ret_win_per)

#lib=load(path2lib,"MassBankJulia")

#ms1val=195.085
#ms2val=[138.066, 195.087, 110.071, 83.0603, 69.044]
#ms2int=[1813.0, 687.0, 579.0, 570.0, 471.0]

#pos_add_m=[1.007825,18.033823,22.989218,33.033489,38.963158,42.033823,
#    44.971160,61.065340,64.015765,76.919040,79.021220,83.060370,84.055110]

#neg_add_m=[19.01839,1.007276,-20.974666,-34.969402,-36.948606,-44.998201,
#    -59.013851,-78.918885,-112.985586]

############################################################################
# Start the module

#
# Filter the library

function filter_lib(lib,mode,source)


    vect_1=lib["IONIZATION"]
    vect_2=lib["ION_MODE"]


    inds=zeros(length(vect_1))
    global c=1
    for i=1:length(vect_1)
        tv1=vect_1[i]
        tv2=vect_2[i]

        if length(tv1) > 0 && tv2==mode && tv1==source
            inds[c]=i
            global c += 1
            #println(i)

        end

    end

    return(inds[1:c-1])

end

############################################################################
# Selecting the relevant lib enteries for batch analysis

function selec_lib(lib,inds)

    access=Array{Any}(undef,length(inds))
    name=Array{Any}(undef,length(inds))
    formula=Array{Any}(undef,length(inds))
    mass=Array{Float64}(undef,length(inds))
    smiles=Array{Any}(undef,length(inds))
    inchi=Array{Any}(undef,length(inds))
    cas=Array{Any}(undef,length(inds))
    pubchem=Array{Any}(undef,length(inds))
    inchikey=Array{Any}(undef,length(inds))
    chemspider=Array{Any}(undef,length(inds))
    ms_type=Array{Any}(undef,length(inds))
    ion_s=Array{Any}(undef,length(inds))
    ion_m=Array{Any}(undef,length(inds))
    coll_e=Array{Any}(undef,length(inds))
    res=Array{Any}(undef,length(inds))
    splash= Array{Any}(undef,length(inds))
    mz_val=Array{Any}(undef,length(inds))
    mz_int=Array{Any}(undef,length(inds))
    mz_int_r=Array{Any}(undef,length(inds))
    d_type=Array{Any}(undef,length(inds))
    for i=1:length(inds)
        access[i]=lib["ACCESSION"][Int(inds[i])]
        name[i]=lib["NAME"][Int(inds[i])]
        formula[i]=lib["FORMULA"][Int(inds[i])]
        mass[i]=lib["EXACT_MASS"][Int(inds[i])]
        smiles[i]=lib["SMILES"][Int(inds[i])]
        inchi[i]=lib["INCHI"][Int(inds[i])]
        cas[i]=lib["CAS"][Int(inds[i])]
        pubchem[i]=lib["PUBCHEM"][Int(inds[i])]
        inchikey[i]=lib["INCHIKEY"][Int(inds[i])]
        chemspider[i]=lib["CHEMSPIDER"][Int(inds[i])]
        ms_type[i]=lib["MS_TYPE"][Int(inds[i])]
        ion_s[i]=lib["IONIZATION"][Int(inds[i])]
        ion_m[i]=lib["ION_MODE"][Int(inds[i])]
        coll_e[i]=lib["COLLISION_ENERGY"][Int(inds[i])]
        res[i]=lib["RESOLUTION"][Int(inds[i])]
        splash[i]=lib["SPLASH"][Int(inds[i])]
        mz_val[i]=lib["MZ_VALUES"][Int(inds[i])]
        mz_int[i]=lib["MZ_INT"][Int(inds[i])]
        mz_int_r[i]=lib["MZ_INT_REL"][Int(inds[i])]
        d_type[i]=lib["DATA_TYPE"][Int(inds[i])]


    end

    filtered_lib=Dict("ACCESSION" => access , "NAME"=>name,
     "FORMULA"=>formula,"EXACT_MASS"=>mass,
     "SMILES"=>smiles,"INCHI"=>inchi,"CAS"=>cas,
     "PUBCHEM"=>pubchem,"INCHIKEY"=>inchikey,
     "CHEMSPIDER"=>chemspider,"MS_TYPE"=>ms_type,
     "IONIZATION"=>ion_s,"ION_MODE"=> ion_m,
     "COLLISION_ENERGY"=>coll_e, "SPLASH"=> splash,
    "MZ_VALUES"=> mz_val,"MZ_INT"=>mz_int,
    "MZ_INT_REL"=>mz_int_r,"RESOLUTION"=>res,
     "DATA_TYPE"=>d_type)

     return filtered_lib

end



############################################################################
# Selecting the relevant lib enteries based on the mass

function lib_select_mass(lib,inds,mass,mass_tol)
    mass_vect=lib["EXACT_MASS"]

    masses=abs.(mass .- mass_vect[Int.(inds)])


    sel_masses=zeros(length(inds))
    for i=1:length(inds)
        if masses[i] <= mass_tol
            sel_masses[i]=inds[i]
            #println(masses[i])
        end


    end
    sel_masses_1=sel_masses[sel_masses.>0]

    access=Array{Any}(undef,length(sel_masses_1))
    name=Array{Any}(undef,length(sel_masses_1))
    formula=Array{Any}(undef,length(sel_masses_1))
    mass=Array{Float64}(undef,length(sel_masses_1))
    smiles=Array{Any}(undef,length(sel_masses_1))
    inchi=Array{Any}(undef,length(sel_masses_1))
    cas=Array{Any}(undef,length(sel_masses_1))
    pubchem=Array{Any}(undef,length(sel_masses_1))
    inchikey=Array{Any}(undef,length(sel_masses_1))
    chemspider=Array{Any}(undef,length(sel_masses_1))
    ms_type=Array{Any}(undef,length(sel_masses_1))
    ion_s=Array{Any}(undef,length(sel_masses_1))
    ion_m=Array{Any}(undef,length(sel_masses_1))
    coll_e=Array{Any}(undef,length(sel_masses_1))
    res=Array{Any}(undef,length(sel_masses_1))
    splash= Array{Any}(undef,length(sel_masses_1))
    mz_val=Array{Any}(undef,length(sel_masses_1))
    mz_int=Array{Any}(undef,length(sel_masses_1))
    mz_int_r=Array{Any}(undef,length(sel_masses_1))
    d_type=Array{Any}(undef,length(sel_masses_1))

    for i=1:length(sel_masses_1)
        access[i]=lib["ACCESSION"][Int(sel_masses_1[i])]
        name[i]=lib["NAME"][Int(sel_masses_1[i])]
        formula[i]=lib["FORMULA"][Int(sel_masses_1[i])]
        mass[i]=lib["EXACT_MASS"][Int(sel_masses_1[i])]
        smiles[i]=lib["SMILES"][Int(sel_masses_1[i])]
        inchi[i]=lib["INCHI"][Int(sel_masses_1[i])]
        cas[i]=lib["CAS"][Int(sel_masses_1[i])]
        pubchem[i]=lib["PUBCHEM"][Int(sel_masses_1[i])]
        inchikey[i]=lib["INCHIKEY"][Int(sel_masses_1[i])]
        chemspider[i]=lib["CHEMSPIDER"][Int(sel_masses_1[i])]
        ms_type[i]=lib["MS_TYPE"][Int(sel_masses_1[i])]
        ion_s[i]=lib["IONIZATION"][Int(sel_masses_1[i])]
        ion_m[i]=lib["ION_MODE"][Int(sel_masses_1[i])]
        coll_e[i]=lib["COLLISION_ENERGY"][Int(sel_masses_1[i])]
        res[i]=lib["RESOLUTION"][Int(sel_masses_1[i])]
        splash[i]=lib["SPLASH"][Int(sel_masses_1[i])]
        mz_val[i]=lib["MZ_VALUES"][Int(sel_masses_1[i])]
        mz_int[i]=lib["MZ_INT"][Int(sel_masses_1[i])]
        mz_int_r[i]=lib["MZ_INT_REL"][Int(sel_masses_1[i])]
        d_type[i]=lib["DATA_TYPE"][Int(sel_masses_1[i])]


    end
    selected_lib=Dict("ACCESSION" => access , "NAME"=>name,
     "FORMULA"=>formula,"EXACT_MASS"=>mass,
     "SMILES"=>smiles,"INCHI"=>inchi,"CAS"=>cas,
     "PUBCHEM"=>pubchem,"INCHIKEY"=>inchikey,
     "CHEMSPIDER"=>chemspider,"MS_TYPE"=>ms_type,
     "IONIZATION"=>ion_s,"ION_MODE"=> ion_m,
     "COLLISION_ENERGY"=>coll_e, "SPLASH"=> splash,
    "MZ_VALUES"=> mz_val,"MZ_INT"=>mz_int,
    "MZ_INT_REL"=>mz_int_r,"RESOLUTION"=>res,
     "DATA_TYPE"=>d_type)
     return selected_lib

end

############################################################################
# master mass list generator
#


function master_mass_gen(mz_ref,int_ref,mz_user,int_user,mass_tol)

    mz_values=vcat(mz_ref[:],mz_user[:])
    master_vect=zeros(length(vcat(mz_ref[:],mz_user[:])),3)

    for i=1:size(master_vect,1)
        tv1=abs.(mz_values[i] .- mz_values)
        tv2=findall(x -> x <= mass_tol/2, tv1)
        master_vect[i,1]=mean(mz_values[tv2])
        mz_values[tv2].=0
    end

    for i=1:size(master_vect,1)
        if master_vect[i] > 0
            #println(i)
            tv1=abs.(master_vect[i] .- mz_ref)
            tv2=findall(x -> x <= mass_tol/2, tv1)
            if length(tv2)>0
                master_vect[i,2]=maximum(int_ref[tv2])
                int_ref[tv2] .=0
            end

            ttv1=abs.(master_vect[i] .- mz_user)
            ttv2=findall(x -> x <= mass_tol/2, ttv1)
            if  length(ttv2) >0
                if maximum(ttv2) <= length(int_user)
                    master_vect[i,3]=maximum(int_user[ttv2])
                    int_user[ttv2] .=0
                elseif maximum(ttv2) > length(int_user)

                    tv3 = zeros(size(int_user))
                    for j = 1:length(ttv2)
                        if ttv2[j] > length(int_user)
                            ttv2[j]=0
                        end

                    end
                    master_vect[i,3]=maximum(int_user[ttv2[ttv2 .>0]])
                    int_user[ttv2[ttv2 .>0]] .=0
                end
            end
        end


    end


    master_vect_f=master_vect[master_vect[:,1] .> 0,:]

    return master_vect_f
end


############################################################################
# Dot product calculations
#

function dot_prod(master_vect_f)

    norm_ref_spec=(master_vect_f[:,1] .* sqrt.(master_vect_f[:,2])) ./ sum(master_vect_f[:,1] .* sqrt.(master_vect_f[:,2]))
    norm_user_spec=(master_vect_f[:,1] .* sqrt.(master_vect_f[:,3])) ./ sum(master_vect_f[:,1] .* sqrt.(master_vect_f[:,3]))

    dot_ref=dot(norm_ref_spec,norm_ref_spec)
    dot_user=dot(norm_user_spec,norm_user_spec)

    dot_direct=dot(norm_user_spec,norm_ref_spec)

    direct_m=round(1-abs(dot_ref-dot_direct),digits=2)
    revers_m=round(1-abs(dot_user-dot_direct),digits=2)
    return(direct_m,revers_m)
end



############################################################################
# Fragment matching
#

function frag_match(mz_ref,mz_user,mass_tol)

    ref_match=zeros(length(mz_ref))
    ref_match_e=zeros(length(mz_ref))

    mz_user_1=copy(mz_user)



    for i=1:length(mz_ref)
        tv1=abs.(mz_ref[i] .- mz_user_1 )
        tv2=findall(x -> x <= mass_tol/2, tv1)
        if length(tv2[tv2 .> 0]) > 0
            ref_match[i]=1
            mz_user_1[tv2] .=0
            ref_match_e[i] = mean(tv1[tv2])
        end
    end



    ref_mzs=mz_ref[ref_match .> 0]
    n_ref_match=sum(ref_match)
    ref_match_e_av=mean(ref_match_e)
    ref_match_e_std=std(ref_match_e)


    return(ref_mzs,n_ref_match,ref_match_e_av,ref_match_e_std)

end

############################################################################
# Matching score calculator
#

function score_calc(mz_ref,mz_user,n_ref_match,ref_match_e_av,
    ref_match_e_std,ms1_e,mass_tol,weight_f)

    if n_ref_match > 0
        s_match_frag_ref=round(weight_f[1]*(n_ref_match/length(mz_ref)),digits=2)
        s_match_frag_usr=round(weight_f[2]*(n_ref_match/length(mz_user)),digits=2)

        s_ms2_e=abs(weight_f[4]*round((abs(ref_match_e_av)-(0.5*mass_tol))/(0.5*mass_tol),digits=2))
        s_ms2_std=abs(weight_f[5]*round((abs(ref_match_e_std)-(0.5*mass_tol))/(0.5*mass_tol),digits=2))
        if isnan(s_ms2_std)==1
            s_ms2_std=0
        end
    else
        s_match_frag_ref=0
        s_match_frag_usr=0
        s_ms2_e=0
        s_ms2_std=0
    end

    s_ms1_e=abs(weight_f[3]*round((abs(ms1_e)-(mass_tol))/(mass_tol),digits=2))


    return([s_match_frag_ref,s_match_frag_usr,s_ms1_e,s_ms2_e,s_ms2_std])
end

############################################################################
# Fragment entreies
#

# rep=entery_match(selected_lib,ms2val,ms2int,mass_tol,masses[i],id,rt,ms1val)

function entery_match(selected_lib,frag_mz,frag_int,mass_tol,mass,id,rt,ms,weight_f)

    n_ref=size(selected_lib["EXACT_MASS"],1)

    rep=Array{Any}(undef,n_ref,17)

    for i=1:n_ref
        mz_ref=copy(selected_lib["MZ_VALUES"][i])
        int_ref=copy(selected_lib["MZ_INT"][i])
        mz_user=copy(frag_mz)
        int_user=copy(frag_int)
        ref_mzs,n_ref_match,ref_match_e_av,ref_match_e_std=frag_match(mz_ref[mz_ref .> 0],mz_user,mass_tol)
        if n_ref_match>0
            master_vect_f=master_mass_gen(mz_ref,int_ref,mz_user,int_user,mass_tol)
            direct_m,revers_m=dot_prod(master_vect_f)
        else
            direct_m=0
            revers_m=0
        end

        ms1_e=selected_lib["EXACT_MASS"][i]-mass
        scors=score_calc(mz_ref,mz_user,n_ref_match,ref_match_e_av,
            ref_match_e_std,ms1_e,mass_tol,weight_f)
        final_score=round(sum(scors)+weight_f[6]*direct_m+weight_f[7]*revers_m,digits=2)
        rep[i,1]=id
        rep[i,2]=rt
        rep[i,3]=ms
        rep[i,4]=selected_lib["NAME"][i]
        rep[i,5]=string(n_ref_match)*"--"*string(length(mz_ref))
        rep[i,6]=string(n_ref_match)*"--"*string(length(mz_user))
        rep[i,7]=round(ms1_e,digits=3)
        rep[i,8]=round(ref_match_e_av,digits=3)
        rep[i,9]=round(ref_match_e_std,digits=3)
        rep[i,10]=direct_m
        rep[i,11]=revers_m
        rep[i,12]=final_score
        rep[i,13]=selected_lib["DATA_TYPE"][i]
        rep[i,14]=string(ref_mzs)
        rep[i,15]=selected_lib["CAS"][i]
        rep[i,16]=string(frag_mz)
        rep[i,17]=string(frag_int)


    end



    return(rep)

end


############################################################################
#

 function spec_convert(spec)

    tv1=split(spec.MS2Mass[2:end-1],", ")
    ms2val=parse.(Float64,tv1)

    tv2=split(spec.MS2Int[2:end-1],", ")
    ms2int=parse.(Float64,tv2)

    return(ms2val,ms2int)


end


############################################################################
# Convert the MS1 and MS2 spectra in one vector

 function comp_convert(spec)

    if spec.MS1Comp != "0"

        ms1_val=parse.(Float64,split(spec.MS1Comp[2:end-1],", "))
        ms1_int=parse.(Float64,split(spec.MS1CompInt[2:end-1],", "))
    else
        ms1_val=0
        ms1_int=0

    end

    if spec.MS2Comp != "0"

        ms2_val=parse.(Float64,split(spec.MS2Comp[2:end-1],", "))
        ms2_int=parse.(Float64,split(spec.MS2CompInt[2:end-1],", "))
    else
        ms2_val=0
        ms2_int=0
    end

    ms2val=vcat(ms1_val,ms2_val)
    ms2val=ms2val[ms2val .>0]
    ms2int=vcat(ms1_int,ms2_int)
    ms2int=ms2int[ms2int .>0]

    return(ms2val,ms2int)


end



############################################################################
# Warpping function

function featureID_single_feature_inter(lib,frag_mz,frag_int,mass_tol,feature,
    mode,source,parent,weight_f)


    pos_add_m=[1.007825,18.033823,22.989218,33.033489,38.963158,42.033823,
        44.971160,61.065340,64.015765,76.919040,79.021220,83.060370,84.055110]

    neg_add_m=[19.01839,1.007276,-20.974666,-34.969402,-36.948606,-44.998201,
        -59.013851,-78.918885,-112.985586]

    inds=filter_lib(lib,mode,source)
    if parent ==1 && mode=="POSITIVE"
        masses=feature.MeasMass - 1.007825

    elseif parent ==1 && mode=="NEGATIVE"
        masses=feature.MeasMass + 1.007825

    elseif parent ==0 && mode=="POSITIVE"
        masses=feature.MeasMass .- pos_add_m

    elseif parent ==0 && mode=="NEGATIVE"
        masses=feature.MeasMass .+ neg_add_m

    end

    id=feature.Nr
    rt=feature.Rt
    ms=feature.MeasMass
    global final_table=zeros(1,17)
    for j=1:length(masses)
        selected_lib=lib_select_mass(lib,inds,masses[j],mass_tol)
        if length(selected_lib["ACCESSION"])>0
            rep=entery_match(selected_lib,frag_mz,frag_int,mass_tol,masses[j],id,rt,ms,weight_f)

            if length(rep)>0
                global final_table=vcat(final_table,rep)
            end
        end
    end

    table=DataFrame(final_table[2:end,:],[:ID,:Rt,:MS1Mass,:Name,:RefMatchFrag,:UsrMatchFrag,
    :MS1Error,:MS2Error,:MS2ErrorStd,:DirectMatch,:ReversMatch,:FinalScore,
    :SpecType,:MatchedFrags,:Cas,:FragMZ,:FragInt])
    sort!(table,[:FinalScore],rev = true)

    return table


end

############################################################################
#
# Wrapping function to do id based on previosly deconvoluted feature list

function featureID_spec_batch_inter(lib,mode,source,path2spec,mass_tol,parent,pathout,weight_f)


    inds=filter_lib(lib,mode,source)
    pos_add_m=[1.007825,18.033823,22.989218,33.033489,38.963158,42.033823,
        44.971160,61.065340,64.015765,76.919040,79.021220,83.060370,84.055110]

    neg_add_m=[19.01839,1.007276,-20.974666,-34.969402,-36.948606,-44.998201,
        -59.013851,-78.918885,-112.985586]

    spec_list=CSV.read(path2spec)
    global final_table=zeros(1,17)

    for i=1:size(spec_list,1)

        if parent ==1 && mode=="POSITIVE"
            masses=feature.MeasMass - 1.007825

        elseif parent ==1 && mode=="NEGATIVE"
            masses=feature.MeasMass + 1.007825

        elseif parent ==0 && mode=="POSITIVE"
            masses=feature.MeasMass .- pos_add_m

        elseif parent ==0 && mode=="NEGATIVE"
            masses=feature.MeasMass .+ neg_add_m

        end

        spec=spec_list[i,:]
        ms2val,ms2int=spec_convert(spec)

        id=spec_list[i,:].Nr
        rt=spec_list[i,:].ScanNumb
        ms=spec_list[i,:].MS1Mass

        for j=1:length(masses)
            selected_lib=lib_select_mass(lib,inds,masses[j],mass_tol)
            if length(selected_lib["ACCESSION"])>0
                rep=entery_match(selected_lib,ms2val,ms2int,mass_tol,masses[j],id,rt,ms,weight_f)

                if length(rep)>0
                    global final_table=vcat(final_table,rep)
                end
            end
        end


    end

    table=DataFrame(final_table[2:end,:],[:ID,:Rt,:MS1Mass,:Name,:RefMatchFrag,:UsrMatchFrag,
    :MS1Error,:MS2Error,:MS2ErrorStd,:DirectMatch,:ReversMatch,:FinalScore,
    :SpecType,:MatchedFrags,:Cas,:FragMZ,:FragInt])

    sort!(table,[:ID,:FinalScore],rev=(false, true))
    m=split(path2spec,"\\")
    m2=split(m[end],"_decon_features")
    output=joinpath(pathout,m2[1]) * "_IDs.csv"
    CSV.write(output,table)

    return table

end # function


############################################################################
#
# Wrapper function to id on spectrum

function featureID_spec_single(lib,mode,source,ms1val,ms2val,ms2int,mass_tol,parent,weight_f)

    pos_add_m=[1.007825,18.033823,22.989218,33.033489,38.963158,42.033823,
        44.971160,61.065340,64.015765,76.919040,79.021220,83.060370,84.055110]

    neg_add_m=[19.01839,1.007276,-20.974666,-34.969402,-36.948606,-44.998201,
        -59.013851,-78.918885,-112.985586]

    if parent ==1 && mode=="POSITIVE"
        masses=ms1val - 1.007825

    elseif parent ==1 && mode=="NEGATIVE"
        masses=ms1val + 1.007825

    elseif parent ==0 && mode=="POSITIVE"
        masses=ms1val .- pos_add_m

    elseif parent ==0 && mode=="NEGATIVE"
        masses=ms1val .+ neg_add_m

    end

    inds=filter_lib(lib,mode,source)
    global final_table=zeros(1,17)

    for i=1:length(masses)
        selected_lib=lib_select_mass(lib,inds,masses[i],mass_tol)
        if length(selected_lib["ACCESSION"])>0
            id=1
            rt=0
            rep=entery_match(selected_lib,ms2val,ms2int,mass_tol,masses[i],id,rt,ms1val,weight_f)
            if length(rep)>0
                global final_table=vcat(final_table,rep)
            end
        end

    end
    table=DataFrame(final_table[2:end,:],[:ID,:Rt,:MS1Mass,:Name,:RefMatchFrag,:UsrMatchFrag,
    :MS1Error,:MS2Error,:MS2ErrorStd,:DirectMatch,:ReversMatch,:FinalScore,
    :SpecType,:MatchedFrags,:Cas,:FragMZ,:FragInt])

    sort!(table,[:ID,:FinalScore],rev=(false, true))

    return table

end



############################################################################
#
# Wrapping function to do id based on components

function featureID_comp_inter(lib,mode,source,path2spec,weight_f)


    inds=filter_lib(lib,mode,source)

    spec_list=CSV.read(path2spec)
    global final_table=zeros(1,17)

    for i=1:size(spec_list,1)
        println(i)

        if spec_list.Parent[i]==1.0

            spec=spec_list[i,:]
            ms2val,ms2int=comp_convert(spec)

            id=spec_list[i,:].Nr
            rt=spec_list[i,:].Rt
            ms=parse(Float64,spec_list[i,:].AccuMass)
            mass_tol=(spec_list[i,:].MaxMass-spec_list[i,:].MinMass)

            selected_lib=lib_select_mass(lib,inds,ms,mass_tol)
            if length(selected_lib["ACCESSION"])>0
                rep=entery_match(selected_lib,ms2val,ms2int,mass_tol,ms,id,rt,ms,weight_f)

                if length(rep)>0
                    global final_table=vcat(final_table,rep)
                end
            end

        end

    end

    table=DataFrame(final_table[2:end,:],[:ID,:Rt,:MS1Mass,:Name,:RefMatchFrag,:UsrMatchFrag,
    :MS1Error,:MS2Error,:MS2ErrorStd,:DirectMatch,:ReversMatch,:FinalScore,
    :SpecType,:MatchedFrags,:Cas,:FragMZ,:FragInt])

    sort!(table,[:ID,:FinalScore],rev=(false, true))
    m=basename(path2spec)

    output=joinpath(dirname(path2spec),m[1:end-4]) * "_IDs.csv"
    CSV.write(output,table)

    return table

end # function


############################################################################


############################################################################


end # end of the module
