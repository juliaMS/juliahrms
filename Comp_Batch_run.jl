

using Distributed
addprocs(3)

@everywhere push!(LOAD_PATH,$"/Users/saersamanipour/Desktop/dev/ongoing/juliahrms")

@everywhere using CompCreate
#@everywhere include("/Users/saersamanipour/Desktop/dev/ongoing/juliahrms/Component.jl")
@everywhere using MS_Import
@everywhere using JLD
#@everywhere include("/Users/saersamanipour/Desktop/dev/ongoing/juliahrms/MS_Import_new.jl")

#############################################################################
# Wrapping function for componentization batch


function comp_DIA_batch(pathin,mass_win_per,
    ret_win_per,r_thresh,delta_mass,min_int,mz_thresh)
    fs=readdir(pathin)
    for i=1:length(fs)
        fs1=split(fs[i],'.')
        if fs1[2] == "csv" && fs1[1][end-4:end] == "eport"
            tv1=fs1[1]
            filenames=[string(tv1[1:end-7],".mzXML")]
            println(filenames)
            path2features=joinpath(pathin,fs[i])
            chrom=import_files(pathin,filenames,mz_thresh)

            compo_list = comp_DIA(chrom,path2features,mass_win_per,ret_win_per,r_thresh,delta_mass,min_int)


        end


    end


end # function



#############################################################################
# Wrapping function for componentization batch


function comp_DIA_batch_dist(pathin,mass_win_per,
    ret_win_per,r_thresh,delta_mass,min_int,mz_thresh)

    chrom_mat=Import_files_batch(pathin,mz_thresh);


    mass_win_per_mat=repeat([mass_win_per],size(chrom_mat,1))
    ret_win_per_mat=repeat([ret_win_per],size(chrom_mat,1))
    r_thresh_mat=repeat([r_thresh],size(chrom_mat,1))
    delta_mass_mat=repeat([delta_mass],size(chrom_mat,1))
    min_int_mat=repeat([min_int],size(chrom_mat,1))
    fs=readdir(pathin)
    path2features_mat=Array{Any}(undef,length(fs))


    for i=1:length(fs)
        fs1=split(fs[i],'.')
        if fs1[2] == "csv" && fs1[1][end-4:end] == "eport"
            tv1=fs1[1]
            filenames=[string(tv1[1:end-7],".mzXML")]
            #println(filenames)
            path2features_mat[i]=joinpath(pathin,fs[i])

        else

            path2features_mat[i]=0

        end


    end
    path2features_mat_1=path2features_mat[path2features_mat .!=0]

    #for i =1:length(chrom_mat)
    #    compo_list = comp_DIA(chrom_mat[i],path2features_mat_1[i],mass_win_per_mat[i],
    #    ret_win_per_mat[i],r_thresh_mat[i],delta_mass_mat[i],min_int_mat[i])
    #end

    pmap(comp_DIA,chrom_mat,path2features_mat_1,mass_win_per_mat,
    ret_win_per_mat,r_thresh_mat,delta_mass_mat,min_int_mat;retry_delays = zeros(2), on_error=identity)


end # function



###############################################################################
# Parameters


pathin="/Users/saersamanipour/Desktop/UvA/Temp_files/Phil/Drug cal/mzxml_files"
mz_thresh=[0,550]


mass_win_per=0.8
ret_win_per=0.5
r_thresh=0.8

delta_mass=0.004
min_int=300

#comp_DIA_batch(pathin,mass_win_per,
#    ret_win_per,r_thresh,delta_mass,min_int,mz_thresh)

#chrom_mat[1]["MS2"]["PrecursorIonWin"]


comp_DIA_batch_dist(pathin,mass_win_per,
    ret_win_per,r_thresh,delta_mass,min_int,mz_thresh)
