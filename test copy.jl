using Main.MS_Import

filename="JBA_20180219_BJORNOYA_NON-TARGET_00101.mat"
pathtofile="Users/saersamani/Desktop/Company/Python_code/Cython/"
pathout="Users/saersamani/Desktop/NIVA/Julia_project/"
savefile=0

chrom=py2julia_conversion(filename,pathtofile,pathout,savefile) #just a test





y=[1,3,5,10,20,18,22,11,4,1]

x=range(1,length(y))

spl=Spline1D(x,y; k=3, bc="nearest", s=0.0)
x2=range(minimum(x),stop=maximum(x),length=20)
y2=spl(x2)

y2_s=sma(y,3)





#################################################################
#test
numchan=3
namechan=["MS1","MS2","Cal"]
pathin="D:\\Data\\julia\\Data_test_juliaHRMS\\Chrom1"
filenames=["JBA_20180219_BJORNOYA_NON-TARGET_00101.CDF",
"JBA_20180219_BJORNOYA_NON-TARGET_00102.CDF","JBA_20180219_BJORNOYA_NON-TARGET_00103.CDF"]

max_numb_iter=200
max_t_peak_w=30
res=20000
min_ms_w=0.02
r_tresh=0.8
min_int=5000
sig_inc_thresh=5
S2N=1.5


@time results,final_table=feature_detect_signle_file!(numchan,namechan,pathin,filenames,
    max_numb_iter,max_t_peak_w,res,min_ms_w,r_tresh,min_int,sig_inc_thresh)


##
#QAQC



plot(feature[:,10],leg=false)
scatter!(feature[:,10],leg=false)
plot!(t_dim,leg=false)


plot(masses[31,:],feature[31,:],leg=false)
plot!(masses[32,:],feature[32,:],leg=false)
plot!(masses[33,:],feature[33,:],leg=false)
plot!(masses[30,:],feature[30,:],leg=true)
xlims!(570.3,570.4)

plot(masses',feature',leg=false)
xlims!(570.3,570.4)

y_r=model(x2fit,fit.param)

plot(x2fit, y2fit)
scatter!(x2fit,y_r)
plot(fit.resid)

for i=1:convert(Int32,floor(size(t_x_r,1)/2))
    println(i)

end

##############################################################################
# Completing the MS_Import.jl module

push!(LOAD_PATH,pwd())

using MS_Import
using FeatureDetection


numchan=1
namechan=["MS1","MS2"]
pathin="C:\\Temp_File\\Jake"
filenames=["180420-SWATH-JO054-006 Day5 C.mzXML"]
format="mzxml"
mz_thresh=600

max_numb_iter=15
max_t_peak_w=30
res=20000
min_ms_w=0.02
r_tresh=0.85
min_int=2000
sig_inc_thresh=5
S2N=2
min_peak_w_s=3


@time mz_vals,mz_int,t0,t_end,m,pathin=MS_Import.import_files_MS1(format,numchan,namechan,pathin,filenames,mz_thresh)

@time rep_table,final_table=FeatureDetection.feature_detect_signle_data(mz_vals,mz_int,t0,t_end,m,pathin,
    max_numb_iter,max_t_peak_w,res,min_ms_w,r_tresh,min_int,sig_inc_thresh,S2N,format,min_peak_w_s,mz_thresh)



###############################################################################
#

push!(LOAD_PATH,pwd())

using MS_Import

numchan=1
namechan=["MS1"]
pathin="C:\\Temp_File\\Jake\\mzXML_files"
format="mzxml"
mz_thresh=600







function Import_files_batch_MS1(format,numchan,namechan,pathin,mz_thresh)
    Dname=readdir(pathin)
    mz_vales_mat=Array{Any}(undef,size(Dname,1))
    mz_int_mat=Array{Any}(undef,size(Dname,1))
    t0_mat=Array{Float64}(undef,size(Dname,1))
    t_end_mat=Array{Float64}(undef,size(Dname,1))
    m_mat=Array{Any}(undef,size(Dname,1))
    pathin_mat=Array{String}(undef,size(Dname,1))

    for i=1:size(Dname,1)

        filenames=[Dname[i]]
        mz_vals,mz_int,t0,t_end,m,pathin=import_files_MS1(format,numchan,namechan,pathin,filenames,mz_thresh)
        mz_vales_mat[i]=mz_vals
        mz_int_mat[i]=mz_int
        t0_mat[i]=t0
        t_end_mat[i]=t_end
        m_mat[i]=m
        pathin_mat[i]=pathin

    end
    return (mz_vales_mat,mz_int_mat,t0_mat,t_end_mat,m_mat,pathin_mat)

end

mz_vales_mat,mz_int_mat,t0_mat,t_end_mat,m_mat,pathin_mat=Import_files_batch_MS1(format,numchan,namechan,pathin,mz_thresh)



###############################################################################
# Orbi test

#numchan=1
#namechan=["MS1"]
#namechan=["MS1"]
#pathin="D:\\Data\\julia\\data_jake\\Mat_files"
#pathin="C:\\Temp_File\\Beijing\\mzXML_files"
#filenames=["20190522-STR1.mzXML"]

#max_numb_iter=15
#max_t_peak_w=60
#res=45000
#min_ms_w=0.01
#r_tresh=0.85
#min_int=10000
#sig_inc_thresh=5
#S2N=3
#format="mzxml"
#min_peak_w_s=6
#mz_thresh=600

###############################################################################

#numchan=1
#namechan=["MS1","MS2"]
#pathin="C:\\Temp_File\\Jake"
#filenames=["180420-SWATH-JO054-006 Day5 C.mzXML"]

#max_numb_iter=15
#max_t_peak_w=30
#res=20000
#min_ms_w=0.02
#r_tresh=0.85
#min_int=2000
#sig_inc_thresh=5
#S2N=2
#format="cdf"
#format="mat"
#format="mzxml"
#mz_thresh=600
#min_peak_w_s=3

##############################
#numchan=1
#namechan=["MS1"]
#pathin="D:\\Data\\julia\\data_jake\\Mat_files"
#filenames=["180419-TofMS-JO055-eq.mat"]

#max_numb_iter=844
#max_t_peak_w=30
#res=20000
#min_ms_w=0.02
#r_tresh=0.8
#min_int=5000
#sig_inc_thresh=5
#S2N=1.5
#format="cdf"
#format="mat"
#min_peak_w_s=3



#function feature_detect_batch(numchan,namechan,pathin,max_numb_iter,
#   max_t_peak_w,res,min_ms_w,r_tresh,min_int,sig_inc_thresh,S2N,format)


#    Fnames,Paths=file_name2_Import(format,numchan,pathin)
#   Numchan=repeat([numchan],size(Paths,1))
#    Namechan=repeat([namechan],size(Paths,1))
#   Max_it=repeat([max_numb_iter],size(Paths,1))
#    Max_t_peak_w=repeat([max_t_peak_w],size(Paths,1))
#    Res=repeat([res],size(Paths,1))
#    Min_ms_w=repeat([min_ms_w],size(Paths,1))
#    R_tresh=repeat([r_tresh],size(Paths,1))
#    Min_int=repeat([min_int],size(Paths,1))
#    Sig_inc_thresh=repeat([sig_inc_thresh],size(Paths,1))
#    S2N1=repeat([S2N],size(Paths,1))
#    Format=repeat([format],size(Paths,1))

    #pmap(feature_detect_signle_file,Numchan,Namechan,Paths,Fnames,Max_it,
    #Max_t_peak_w,Res,Min_ms_w,R_tresh,Min_int,Sig_inc_thresh,S2N1,Format)

#    for i=1:size(Paths,1)
#        feature_detect_signle_data(Numchan[i],Namechan[i],Paths[i],Fnames[i],Max_it[i],
#        Max_t_peak_w[i],Res[i],Min_ms_w[i],R_tresh[i],Min_int[i],Sig_inc_thresh[i],S2N1[i],Format[i])
#
#    end

    #(numchan,namechan,pathin,filenames,
    #    max_numb_iter,max_t_peak_w,res,min_ms_w,r_tresh,min_int,sig_inc_thresh,S2N,format)


#end



#######################
# Test batch
#numchan=3
#namechan=["MS1","MS2","Cal"]
#pathin="D:\\Data\\julia\\Data_test_juliaHRMS\\Chrom_batch"
#filenames=[""]

#max_numb_iter=30
#max_t_peak_w=30
#res=20000
#min_ms_w=0.02
#r_tresh=0.8
#min_int=5000
#sig_inc_thresh=5
#S2N=1.5
#format="cdf"


#@time feature_detect_batch(numchan,namechan,pathin,max_numb_iter,
#    max_t_peak_w,res,min_ms_w,r_tresh,min_int,sig_inc_thresh,S2N,format,mz_thresh)


#####################################################################
# Test single Dataset
#numchan=3
#namechan=["MS1","MS2","Cal"]
#pathin="D:\\Data\\julia\\Data_test_juliaHRMS\\Chrom_batch\\C6"
#filenames=["VANN-GREENLAND01.CDF","VANN-GREENLAND02.CDF","VANN-GREENLAND03.CDF"]

#max_numb_iter=600
#max_t_peak_w=30
#res=20000
#min_ms_w=0.02
#r_tresh=0.8
#min_int=5000
#sig_inc_thresh=5
#S2N=1.5
#format="cdf"

#@time results,final_table=feature_detect_signle_data(numchan,namechan,pathin,filenames,
#    max_numb_iter,max_t_peak_w,res,min_ms_w,r_tresh,min_int,sig_inc_thresh,S2N,format,mz_thresh)


##########################################################################
# Test mat_mzXML file mat format

#numchan=1
#namechan=["MS1"]
#pathin="D:\\Data\\julia\\data_jake\\Mat_files"
#filenames=["180419-TofMS-JO004-011 Day5 B.mat"]


#max_numb_iter=500
#max_t_peak_w=30
#res=20000
#min_ms_w=0.02
#r_tresh=0.85
#min_int=2000
#sig_inc_thresh=5
#S2N=3
#format="mat"
#min_peak_w_s=3

#@time results,final_table=feature_detect_signle_data(numchan,namechan,pathin,filenames,
#    max_numb_iter,max_t_peak_w,res,min_ms_w,r_tresh,min_int,sig_inc_thresh,S2N,format,mz_thresh)

##########################################################################
# Test mat_mzXML file

#numchan=1
#namechan=["MS1","MS2"]
#pathin="C:\\Temp_File\\NA_raw_Data\\mzxmlFiles"
#filenames=["SSA_2404019_NAs_10AA.mzXML"]
#mz_thresh=900


#max_numb_iter=500
#max_t_peak_w=30
#res=20000
#min_ms_w=0.02
#r_tresh=0.85
#min_int=2000
#sig_inc_thresh=5
#S2N=3
#format="mzxml"
#min_peak_w_s=3

#@time results,final_table=feature_detect_signle_data(numchan,namechan,pathin,filenames,
#    max_numb_iter,max_t_peak_w,res,min_ms_w,r_tresh,min_int,sig_inc_thresh,S2N,format,mz_thresh)



####################################################################
# Reading MassBank.mat into julia and save it as julia format

using PyCall

sio=pyimport("scipy.io")

MB_m=sio.loadmat("/home/saer/Google Drive/MassBank_matlab.mat")


##################################################################
# Reading the JASON files into julia format

using JSON




f=open("/home/saer/Google Drive/EA000401.txt")
lines = readlines(f)


spec=BSON.load("/home/saer/Google Drive/EA000401.txt")




##########################################################################
#

using PyCall

pm=pyimport("pyopenms")


pathin="/home/saer/Desktop/juliaMS_data_tests"
filenames=["180420-SWATH-JO054-006 Day5 C.mzXML"]

path2file=joinpath(pathin,filenames[])


exp = pm.MSExperiment()
pm.MzXMLFile().load(path2file,exp)


#############################################################################
##

sig=[678389	432380	344277
622334	503873	313434
644632	463232	394345
425874	213517	182302
497876	204545	163437
475788	225562	193454
377850	191444	115874
322213	201231	132424
321212	189322	122242
216379	147823	130684
183243	100443	134545
171084	90343	123242
84400	76251	75087
79232	72324	67432
82121	68322	71321
12992	10391	12168
12313	11332	10452
10442	10932	13434
12992	10391	12168
11213	12332	13224
10434	10233	13424]

y=[50000.0
50000.0
50000
20000
20000
20000
10000
10000
10000
5000
5000
5000
1000
1000
1000
300
300
300
100
100
100]

using LsqFit
using Statistics
using Plots

@. multimodel(x, p) = p[1] + x[:, 1]*p[2] + x[:, 2]*p[3] + x[:, 3]*p[4] + x[:, 1]*x[:, 2]*p[5]

#lbp=[206.0,0.01,-10,-10,0]

fit1 = curve_fit(multimodel ,sig, y, [10.0,0.03,-0.001,-0.001,1e-7])
r2=1 - var(fit1.resid) / var(y)


fit1.param



y_r=multimodel(sig,fit1.param)/1000

scatter(y/1000,y_r)
plot!([0.0,50],[0.0,50])
xlabel!("Nominal Concentration (µg)")
ylabel!("Estimated Concentration (µg)")



#############################################################################
#
using CSV
using DataFrames
using JLD

##
data=CSV.read("Netura_Loss_Prob.csv")


Frag=Dict("mass" => round.(data.mass,digits=3), "prob" => round.(data.Probability/maximum(data.Probability),digits=3))



Iso=Dict("ru_m" => [27.9949,46.9689,26.003,43.972,30.9984,13.0078])
Ad=Dict("pos" => [1.007825,18.033823,22.989218,33.033489,38.963158,42.033823,
    44.971160,61.065340,64.015765,76.919040,79.021220,83.060370,84.055110],
    "neg" => [19.01839,1.007276,-20.974666,-34.969402,-36.948606,-44.998201,
        -59.013851,-78.918885,-112.985586])


aux_data=Dict("Frag" => Frag, "Iso" => Iso, "Ad" => Ad)


jldopen("AUX_data.jld", "w") do file
    write(file, "AUX_data", aux_data)  # alternatively, say "@write file A"
end

ad=load("AUX_data.jld","AUX_data")

ad["Ad"]["pos"][:]

#


####################################
# Creating the test files for SAFD package


push!(LOAD_PATH,pwd())

using MS_Import
using JLD


format="mzxml"
pathin="/Users/saersamanipour/Desktop/dev/Data"
filenames=["PFAS191007_002.mzXML"]
mz_thresh=[0,380]

chrom=import_files(format,pathin,filenames,mz_thresh)

jldopen("test_chrom.jld", "w") do file
    write(file, "chrom", chrom)  # alternatively, say "@write file A"
end

d=load("/Users/saersamanipour/Desktop/dev/pkgs/SAFD/test/test_chrom.jld")





########################################################################

using CompCreate
using JLD
using MS_Import



path2features="/Users/saersamanipour/Desktop/dev/Data/PFAS191007_002_report.csv"
path2SWATH="/Users/saersamanipour/Desktop/dev/Data/swath_windows.csv"
mass_win_per=0.8
ret_win_per=0.5
r_tresh=0.8


pathin="/Users/saersamanipour/Desktop/dev/Data"

format="mzxml"
filenames=["PFAS191007_002.mzXML"]
mz_thresh=[0,600]

#mode="POSITIVE"
mode="NEGATIVE"
delta_mass=0.004
min_int=300
# i=498



chrom=import_files(format,pathin,filenames,mz_thresh)


comp_ms1(chrom,path2features,mass_win_per,ret_win_per,r_tresh,mode,delta_mass)


<<<<<<< HEAD




#################################################################################

pathin="/media/saer/Data/Data/Temp_data/Phil/Cal/"

Dname=readdir(pathin)
ind=zeros(length(Dname),1)

for i=1:length(Dname)
    if string(Dname[i][1]) != "."
        if Dname[i][end-4:end] == "mzXML" || Dname[i][end-4:end]== "mzxml" || Dname[i][end-4:end] == "MZXML"
            ind[i]=1
        elseif Dname[i][end-3:end] == "CDF" || Dname[i][end-3:end]== "cdf"
            ind[i]=1

        end
    end

end
=======
##############################################################################
using MS_Import
cd("/Users/saersamanipour/Desktop/dev/ongoing/juliahrms/")
push!(LOAD_PATH,"/Users/saersamanipour/Desktop/dev/ongoing/juliahrms/")



pathin="/Users/saersamanipour/Desktop/dev/Data"
filenames=["Wa2_002_A_neg.mzXML"]
mz_thresh=[0,650]

chrom=import_files(pathin,filenames,mz_thresh)

haskey(chrom, "MS2")==1

###############################################################################

using Plots
using PyPlot
default(show = true)
gui()

x2=[194.097, 259.085, 194.098, 193.089, 179.074, 192.083, 237.104, 165.07, 116.048, 151.058, 178.068, 190.063, 238.107]
y2=[5149.0, 15633.0, 16333.4, 3129.2, 1739.6, 3290.0, 1323.8, 924.6, 303.6, 206.2, 488.2, 821.4, 629.0]

x1=[237.1017]
y1=[5000]


plot(x2, y2, line=:stem, label = "")

xlabel!("m/z values")
ylabel!("Intensity")
title!("236.093875")
Plots.savefig("/Users/saersamanipour/OneDrive - UvA/Lecturs/Pics/Spec.png")



x_ref=[91.0541	2.650336
116.0493	2.900163
117.0573	2.522446
151.0544	0.770319
152.0619	8.920137
165.0697	25.38307
166.0649	2.008013
166.0776	2.26331
167.0729	9.391698
176.0615	1.834183
177.058	2.218852
178.0655	3.860421
179.0728	53.904087
190.0653	4.747403
191.0727	17.922527
192.0806	47.8604
193.0884	100
194.0962	89.863655]

plot(x2, 100 .* y2./maximum(y2), line=:stem, label = "User Spec")
plot!(x_ref[:,1], -1 .* x_ref[:,2], line=:stem, label = "Ref Spec")

xlabel!("m/z values")
ylabel!("Intensity")
title!("236.093875")
Plots.savefig("/Users/saersamanipour/OneDrive - UvA/Lecturs/Pics/Spec_matched.png")




x21=[307.174, 91.0554, 108.082, 120.077, 132.079, 183.122, 185.136, 304.175, 307.174]
y21=[7758.0, 950.8, 2041.2, 475.2, 524.2, 2245.0, 1764.2, 1165.2, 1604.8]


plot(x21, 100 .* y21./maximum(y21), line=:stem, label = "")
xlabel!("m/z values")
ylabel!("Intensity")
title!("303.160875")
Plots.savefig("/Users/saersamanipour/OneDrive - UvA/Lecturs/Pics/Spec_qz3.png")
>>>>>>> 6e51373230f789f9b46513dcd28a3fe0749cf5e0
