# -*- coding: utf-8 -*-
"""
This a package for importing the Open MS files into the pythn environment

@author: Dr. Saer Samanipour
Norwegian Institut for Water Research (NIVA)
saer.samanipour@niva.no
"""


import numpy as np
import matplotlib.pyplot as plt
from netCDF4 import Dataset
import os
import scipy.io as sio
import Import_mzXML as mz


"""
This function reads a NetCDF file and produces a Dict with the attributs

The only parameter is the path to the file
"""
def Import_NetCDF(path):
    data={}
    NC=Dataset(path,"r", format="NETCDF3")
    total_intensity=NC.variables["total_intensity"][:,]
    scan_index=NC.variables["scan_index"][:,]
    mass_values=NC.variables["mass_values"][:,]
    intensity_values=NC.variables["intensity_values"][:,]
    scan_acquisition_time=NC.variables["scan_acquisition_time"][:,]
    t0=scan_acquisition_time[0]/60
    t_end=scan_acquisition_time[len(scan_acquisition_time)-1]/60
    point_count=NC.variables["point_count"][:,]
    Max_Mz_displacement=np.max(point_count)
    dim=(int(len(point_count)),Max_Mz_displacement)
    
    Mz_values=np.zeros(dim)
    Mz_intensity=Mz_values.copy()
    
    for i in range(len(point_count)-1):
        Mz_values[i,0:point_count[i]]=mass_values[scan_index[i]:scan_index[i+1]]
        Mz_intensity[i,0:point_count[i]]=intensity_values[scan_index[i]:scan_index[i+1]]
        
    
    data['total_intensity']=total_intensity
    data['t0']=t0
    data['t_end']=t_end
    data['Mz_values']=Mz_values
    data['Mz_intensity']=[Mz_intensity]
    return(data)





"""
This function is to import netCDF 3-4 files. The necessary parameters for this
function are:
    paths_in: the path of the files that must be imported
    path_out: the path of the folder for saving the final file
    num_channels: the number of channels to be imported
    Name_channel: the name of the channels that should be imported
    N_MS_Levels: The number of MS levels
    
Please note the size of num_channels and Name_channel should be the same. Also,
N_MS_levels >= num_channels and paths_in
"""


def Import_CDF_DIA_Channel(paths_in,path_out,num_channels,Name_Channels,N_MS_Levels):
    data={}
    N=os.path.splitext(os.path.basename(paths_in[0])) #this will be the name of the final file
    Name=N[0]
    Loc=path_out+"\\"+Name+".mat"
    if (num_channels==1 and N_MS_Levels==1):
        if "Name_Channels" in locals():
           data[Name_Channels]=Import_NetCDF(paths_in) 
            
        else:
            data["MS1"]=Import_NetCDF(paths_in)
            
        
        
    elif (num_channels>1 and N_MS_Levels>1):
        if "Name_Channels" in locals():
           for i in range(num_channels):
                data[Name_Channels[i]]=Import_NetCDF(paths_in[i])
            
        else:
            for i in range(num_channels):
                var='MS'+str(i+1)
                data[var]=Import_NetCDF(paths_in[i])
                
                
    
    elif (num_channels==1 and N_MS_Levels>1):
        print("Still to come! :D")
        
    
    sio.savemat(Loc,data)
    return(data)
    
        
    
"""
Import mzXML files.
The needed parameters are:
    path_in: the path to the file or files to import
    path_out: the path to the folder where you want to save the data at 
    numb_channel: the number of channels to import
    MS_levels: the number of MS experiments
    
Please note that when you have more than one file to import the order of the paths
should follow the order of the mass levels. Also the num_channel <= MS_level
"""            


def mzMXL_DIA_Import(path_in,path_out,num_channel,MS_level):
    N=os.path.splitext(os.path.basename(path_in[0])) #this will be the name of the final file
    Name=N[0]
    Loc=path_out+"\\"+Name+".mat"
    data={}
    data=mz.mzxml_import(path_in,num_channel,MS_level)
    return(data)
    sio.savemat(Loc,data)
    
    
            
        
