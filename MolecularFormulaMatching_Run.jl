push!(LOAD_PATH,pwd())


using XLSX
using DataFrames
using MolecularFormulaMatching


#path2file="/media/saer/Data/Data/1000Lakes/Reports_filtered/TRU200116_01201_report_KMD_filtered.csv"
path2folder="/media/saer/Data/Data/1000Lakes/Reports_filtered"
path2list="PFAS_Curated.xlsx"
mass_tol=0.01
mode="NEGATIVE"


list = DataFrame(XLSX.readtable(path2list, "Sheet1")...) # Import the traget list

MF_match_list_batch(path2folder,list,mass_tol,mode) #Batch process the data
