
using PkgTemplates


t= Template(;
    user="saersamani",
    dir="/Users/saersamanipour/Desktop/dev/pkgs",
    authors=["Saer Samanipour, PhD", "Computational Mass Spec Lab (CMSL)"],
    julia=v"1.1",
    plugins=[
        License(; name="MIT"),
        TravisCI(),
        #Codecov(),
        AppVeyor(),
    ],
)


t("MS_CompCreate")
