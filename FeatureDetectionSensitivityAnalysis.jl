
using XLSX
using LsqFit
using Plots
using Dierckx
using Statistics
using PyCall
np = pyimport("numpy")
PM = pyimport("pyopenms")
sio = pyimport("scipy.io")
using DataFrames
using CSV
using DSP
using NCDatasets
#include("MS_Import.jl")
#using Main.MS_Import

#export feature_detect_signle_data, file_name2_Import

#@everywhere include("MS_Import.jl")
#@everywhere using Main.MS_Import


#@pyimport scipy.io as sio



###################################################################
###
#netCDF file import

function readCDF(File)
    DD=Dict()
    #File="D:\\Data\\julia\\Data_test_juliaHRMS\\Chrom_batch\\C1\\VANN-DANMARK01.CDF"
    ds = Dataset(File)

    scan_acquisition_time=ds["scan_acquisition_time"]
    scan_duration=ds["scan_duration"]                                           #The duration of each scan
    inter_scan_time=ds["inter_scan_time"]                                       #The time between scans
    total_intensity=ds["total_intensity"]                                       #TIC
    mass_range_min=ds["mass_range_min"]                                         #min mass range
    mass_range_max=ds["mass_range_max"]                                         #max mass range
    scan_index=ds["scan_index"]
    point_count=ds["point_count"]
    mass_values = ds["mass_values"]                                             #m/z values
    intensity_values=ds["intensity_values"]                                     #intensity at each m/z value

    #defining the starting point and the end point of the chromatogram
    t0=scan_acquisition_time[1]/60                                              #min
    t_end=scan_acquisition_time[end]/60                                         #min

    Max_Mz_displacement=maximum(point_count)
    Mz_values=zeros(length(scan_index),Max_Mz_displacement)
    Mz_intensity=zeros(length(scan_index),Max_Mz_displacement)

    for i=1:length(scan_index)-1
        if (scan_index[i+1] < size(mass_values,1))
            Mz_values[i,1:point_count[i]]=mass_values[scan_index[i]+1:scan_index[i+1]]
            Mz_intensity[i,1:point_count[i]]=intensity_values[scan_index[i]+1:scan_index[i+1]]
        else
            break
        end
        #println(i)
    end

    DD["total_intensity"]=total_intensity
    DD["t0"]=t0
    DD["t_end"]=t_end
    DD["Mz_values"]=Mz_values
    DD["Mz_intensity"]=Mz_intensity
    DD["scan_duration"]=scan_duration

    return DD


end


#ncinfo("/home/samanipour_sa/Desktop/Data_feat_detect_test/JBA_20180219_BJORNOYA_NON-TARGET_00101.CDF")
#ds = Dataset("/home/samanipour_sa/Desktop/Data_feat_detect_test/JBA_20180219_BJORNOYA_NON-TARGET_00101.CDF")

#numchan=3
#namechan=["MS1","MS2","Cal"]
#pathin="/home/samanipour_sa/Desktop/Data_feat_detect_test/"
#filenames=["JBA_20180219_BJORNOYA_NON-TARGET_00101.CDF",
#"JBA_20180219_BJORNOYA_NON-TARGET_00102.CDF","JBA_20180219_BJORNOYA_NON-TARGET_00103.CDF"]
#pathout="/home/samanipour_sa/Desktop/Data_feat_detect_test/"


function cdf_Import(numchan,namechan,pathin,filenames)
    chrom=Dict()
    for i=1:numchan

        File=pathin*"\\"*filenames[i]
        chrom[namechan[i]]=readCDF(File)

    end
    #m=split(filenames[1],".")
    #sio.savemat(pathout*m[1],chrom)
    #println("A matfile, related to this file has been saved in the requested location!")
    return chrom

end


####################################################################

function mzXML_MAT_Import(numchan,namechan,pathin,filenames)
    DD=Dict()
    if isa(filenames,Array)==1
        File=pathin * "\\" * filenames[]
    else
        File=pathin * "\\" * filenames
    end
    chrom=sio.loadmat(File)
    for i=1:numchan

        DD[namechan[i]] = Dict("Mz_values" => chrom[namechan[i]][1][1][1],
        "Mz_intensity" => chrom[namechan[i]][1][1][2], "Rt" => chrom[namechan[i]][1][1][3])


    end

    return DD
end

####################################################################
# max_sig_finder is a fucntion to define the boundaries of each feature in the
# sample.

#plot(tv2,tv1)
#xlims!(156,157)
#tv1=mz_int[4,:]
#tv2=mz_vals[4,:]
#134.0368	134.0417


#tv3=findall(x -> 134.0417>x>134.0368,tv2)
#tv4=tv1[tv3]
#m=maximum(tv1[tv3])
#mm=argmax(tv1[tv3])

#mind=CartesianIndex(4,tv3[1]+mm-1)
#mz_int[mind]


function max_sig_finder(mz_vals,mz_int,max_t_peak_w,min_int)
    m=maximum(mz_int[:])
    #m=mz_int[4,8302]
    if m >= min_int
        mind=argmax(mz_int)
        #mind=CartesianIndex(4,8302)
        if mind[1]+max_t_peak_w <= size(mz_vals,1) && mind[1]-max_t_peak_w>=1
            t_y_r=mz_int[mind[1]-max_t_peak_w:mind[1]+max_t_peak_w,:]
            t_x_r=mz_vals[mind[1]-max_t_peak_w:mind[1]+max_t_peak_w,:]
            bounds=[mind[1]-max_t_peak_w,max_t_peak_w+1,mind[1]+max_t_peak_w]
            ms_maxim=float(mz_vals[mind])
        elseif mind[1]+max_t_peak_w <= size(mz_vals,1) && mind[1]-max_t_peak_w<=1
            t_y_r=mz_int[1:mind[1]+max_t_peak_w,:]
            t_x_r=mz_vals[1:mind[1]+max_t_peak_w,:]
            bounds=[1,mind[1],mind[1]+max_t_peak_w]
            ms_maxim=float(mz_vals[mind])
        elseif mind[1]+max_t_peak_w > size(mz_vals,1) && mind[1]-max_t_peak_w>=1
            t_y_r=mz_int[mind[1]-max_t_peak_w:size(mz_vals,1),:]
            t_x_r=mz_vals[mind[1]-max_t_peak_w:size(mz_vals,1),:]
            bounds=[mind[1]-max_t_peak_w,max_t_peak_w+1,size(mz_vals,1)]
            ms_maxim=float(mz_vals[mind])
        end
    else
        t_y_r=[]
        t_x_r=[]
        bounds=[]
        mind=[]
        ms_maxim=[]
    end

    return (t_y_r,t_x_r,bounds,mind,ms_maxim)

end

#t_y_r,t_x_r,bounds,mind,ms_maxim=max_sig_finder(mz_vals,mz_int,max_t_peak_w,min_int)

####################################################################
# Defins the boundaries of each feature based on the given coordinates.

function max_sig_finder_cor_based(mz_vals,mz_int,max_t_peak_w,min_int,
    scan_numb,mass_bounds)

    tv1=mz_int[scan_numb,:]
    tv2=mz_vals[scan_numb,:]

    tv3=findall(x -> mass_bounds[1]>=x>=mass_bounds[2],tv2)
    m1=argmax(tv1[findall(x -> mass_bounds[1]>=x>=mass_bounds[2],tv2)])
    mind=CartesianIndex(scan_numb,tv3[m1])

    m=mz_int[mind]
    if m >= min_int
        #mind=argmax(mz_int)
        #mind=CartesianIndex(4,8302)
        if mind[1]+max_t_peak_w <= size(mz_vals,1) && mind[1]-max_t_peak_w>=1
            t_y_r=mz_int[mind[1]-max_t_peak_w:mind[1]+max_t_peak_w,:]
            t_x_r=mz_vals[mind[1]-max_t_peak_w:mind[1]+max_t_peak_w,:]
            bounds=[mind[1]-max_t_peak_w,max_t_peak_w+1,mind[1]+max_t_peak_w]
            ms_maxim=float(mz_vals[mind])
        elseif mind[1]+max_t_peak_w <= size(mz_vals,1) && mind[1]-max_t_peak_w<=1
            t_y_r=mz_int[1:mind[1]+max_t_peak_w,:]
            t_x_r=mz_vals[1:mind[1]+max_t_peak_w,:]
            bounds=[1,mind[1],mind[1]+max_t_peak_w]
            ms_maxim=float(mz_vals[mind])
        elseif mind[1]+max_t_peak_w > size(mz_vals,1) && mind[1]-max_t_peak_w>=1
            t_y_r=mz_int[mind[1]-max_t_peak_w:size(mz_vals,1),:]
            t_x_r=mz_vals[mind[1]-max_t_peak_w:size(mz_vals,1),:]
            bounds=[mind[1]-max_t_peak_w,max_t_peak_w+1,size(mz_vals,1)]
            ms_maxim=float(mz_vals[mind])
        end
    else
        t_y_r=[]
        t_x_r=[]
        bounds=[]
        mind=[]
        ms_maxim=[]
    end

    return (t_y_r,t_x_r,bounds,mind,ms_maxim)

end
####################################################################
#Simple moving average

function sma(y, n)

    vals = zeros(size(y,1) ,1)
    for i in 1:size(vals,1)-(n-1)
        vals[i+1] = mean(y[i:i+(n-1)])
    end

    vals[1]=y[1]
    vals[end]=y[end]
    return vals
end

####################################################################
#Rolling average smoothing
#n must be an odd number

function ra_smooth(y, n)

    w=convert(Int64,(n-1)/2)
    vals = zeros(size(y,1))

    for i=2:size(vals,1)-w
        tv1=y[i-w:i+w]
        vals[i] = mean(tv1)
    end

    vals[1]=y[1]
    vals[end]=y[end]
    return vals
end

####################################################################
#This function produced the mass vectors for limited to the peak
#
##



function res2mass_window(mz_vect,int_vect,res,min_ms_w,ms_maxim)
    tv1=abs.(mz_vect .- ms_maxim)
    tv2=length(filter(x->x <= min_ms_w,tv1))
    if  tv2 > 0
        ms_max_int=maximum(int_vect[abs.(mz_vect .- ms_maxim) .<= min_ms_w])
        ms_max=mz_vect[argmin(abs.(mz_vect .- ms_maxim))]

        #plot(int_vect)
        dms=2*(ms_max/res)
        lb=argmin(abs.(mz_vect .- (ms_max-dms/2)))
        ub=argmin(abs.(mz_vect .- (ms_max+dms/2)))

        if mz_vect[ub]-mz_vect[lb] >= min_ms_w
            x_t=mz_vect[lb:ub]
            y_t=int_vect[lb:ub]
        else
            lb_1=argmin(abs.(mz_vect .- (ms_max-min_ms_w/2)))
            ub_1=argmin(abs.(mz_vect .- (ms_max+min_ms_w/2)))
            x_t=mz_vect[lb_1:ub_1]
            y_t=int_vect[lb_1:ub_1]

        end

    else

        x_t=[]
        y_t=[]


    end
    return (x_t,y_t)


end



#x_t,y_t = res2mass_window(mz_vect,int_vect,res,min_ms_w,ms_maxim)
####################################################################
#
# typeof(findfirst(x -> x < maximum(y)/2,y2)) != Nothing

function isolate_sig(x,y)


    y1=y[1:argmax(y)]
    y2=y[argmax(y):end]
    if findlast(x -> x < maximum(y)/2,y1) != nothing && findfirst(x -> x < maximum(y)/2,y2) != nothing
        ys=y[findlast(x -> x < maximum(y)/2,y1):findfirst(x -> x < maximum(y)/2,y2)+argmax(y)-1]
        xs=x[findlast(x -> x < maximum(y)/2,y1):findfirst(x -> x < maximum(y)/2,y2)+argmax(y)-1]
    elseif findlast(x -> x < maximum(y)/2,y1) == nothing && findfirst(x -> x < maximum(y)/2,y2)!= nothing
        ys=y[1:findfirst(x -> x < maximum(y)/2,y2)+argmax(y)-1]
        xs=x[1:findfirst(x -> x < maximum(y)/2,y2)+argmax(y)-1]

    elseif findlast(x -> x < maximum(y)/2,y1) != nothing && findfirst(x -> x < maximum(y)/2,y2)== nothing
        ys=y[findlast(x -> x < maximum(y)/2,y1):end]
        xs=x[findlast(x -> x < maximum(y)/2,y1):end]
    else
        ys=y
        xs=x

    end

    if length(xs)>3

        spl=Spline1D(xs, ys; k=3, bc="nearest", s=0.0)
        x2=range(minimum(x),stop=maximum(x),length=50)
        y2=spl(x2)
        #y2_s=sma()
        y2_out=y2[y2.>=maximum(y)/2]
        x2_out=x2[y2.>=maximum(y)/2]
        res_m=x2[y2.==maximum(y2)]/(x2_out[end]-x2_out[1])
    elseif length(xs)-1 == 2
        spl=Spline1D(xs, ys; k=length(xs)-1, bc="nearest")
        x2=range(minimum(x),stop=maximum(x),length=50)
        y2=spl(x2)
        y2_out=y2[y2.>=maximum(y)/2]
        x2_out=x2[y2.>=maximum(y)/2]
        res_m=x2[y2.==maximum(y2)]/(x2_out[end]-x2_out[1])

    else
        y2_out=[]
        x2_out=[]
        res_m=[]

    end


    return(x2_out, y2_out,res_m)

end


# x2fit, y2fit, res_m=isolate_sig(x_t,y_t,min_int)


######################################################################


function simple_gauss(x2fit,y2fit,g_numb,r_tresh,min_ms_w)
    if g_numb == 1
        @.model(x, p) = (p[1]/(p[3]*sqrt(pi*2))) * exp(-(x-p[2])^2 / (2*p[3]^2))

        p=zeros(3)
        p[1]=maximum(y2fit)
        p[2]=x2fit[argmax(y2fit)]
        p[3]=(maximum(x2fit)-minimum(x2fit))
        lbp=[0,x2fit[argmax(y2fit)]-min_ms_w,0]
        ubp=[Inf,x2fit[argmax(y2fit)]+min_ms_w,Inf]

        fit1 = curve_fit(model ,x2fit, y2fit, p)
        r2=1 - var(fit1.resid) / var(y2fit)
        #y_r=model(x_t,fit1.param)
        if r2 < r_tresh
            try
                fit1 = curve_fit(model ,x2fit, y2fit, p,lower=lbp, upper=ubp)
                r2=1 - var(fit1.resid) / var(y2fit)
            catch
                fit1 = curve_fit(model ,x2fit, y2fit, p)
                r2=1 - var(fit1.resid) / var(y2fit)
            #fit1,r2=gauss_par_optimiz(x2fit,y2fit,g_numb,p,model,r_tresh,lbp,ubp)
            end

        end

        return (r2,fit1)
    elseif g_numb == 2
        @.model1(x, p) = (p[1]/(p[3]*sqrt(pi*2))) * exp(-(x-p[2])^2 / (2*p[3]^2))+
        (p[4]/(p[6]*sqrt(pi*2))) * exp(-(x-p[5])^2 / (2*p[6]^2))

        p=zeros(6)
        p[1]=maximum(y2fit)
        p[2]=x2fit[argmax(y2fit)]
        p[3]=(maximum(x2fit)-minimum(x2fit))
        p[4]=maximum(y2fit)
        p[5]=x2fit[argmax(y2fit)]
        p[6]=(maximum(x2fit)-minimum(x2fit))
        lbp=[0,x2fit[argmax(y2fit)]-min_ms_w,0,
        0,x2fit[argmax(y2fit)]-min_ms_w,0]
        ubp=[Inf,x2fit[argmax(y2fit)]+min_ms_w,Inf,
        Inf,x2fit[argmax(y2fit)]+min_ms_w,Inf]

        fit1 = curve_fit(model1, x2fit, y2fit, p)
        r2=1 - var(fit1.resid) / var(y2fit)
        if r2 < r_tresh
            #fit1,r2=gauss_par_optimiz(x2fit,y2fit,g_numb,p,model1,r_tresh,lbp,ubp)
            try
                fit1 = curve_fit(model1 ,x2fit, y2fit, p,lower=lbp, upper=ubp)
                r2=1 - var(fit1.resid) / var(y2fit)
            catch
                fit1 = curve_fit(model1 ,x2fit, y2fit, p)
                r2=1 - var(fit1.resid) / var(y2fit)
            #fit1,r2=gauss_par_optimiz(x2fit,y2fit,g_numb,p,model,r_tresh,lbp,ubp)
            end
        end


        return (r2,fit1)


    else
        println("The number of Gaussians to fit is limited to two.")
        r2=0
        fit=[]
        return (r2,fit)

    end

end

# r2,fit1=simple_gauss(x2fit,y2fit,1,r_tresh)

######################################################################
# This needs to be fixed


function gauss_par_optimiz(x2fit,y2fit,g_numb,p,model,r_tresh,lbp,ubp)

    global fit
    global r21
    if length(p) ==3
        tv1=p[3]
        tv2=tv1-0.3*tv1
        #ind=0
        for i=1:10
            p[3]=tv2+0.05*tv2
            Fit = curve_fit(model ,x2fit, y2fit, p,lower=lbp, upper=ubp)
            R2=1 - var(Fit.resid) / var(y2fit)
            #println(R2)
            if R2 >= r_tresh
                #push!(fit,Fit)
                #push!(r2,R2)
                r21=R2
                fit=Fit
                #ind=i

                break
            end
        end

    elseif length(p) == 6
        tv1=p[3]
        tv2=tv1-0.3*tv1
        tv3=p[6]
        tv4=tv3-0.3*tv3

        for i=1:10
            p[3]=tv2+0.05*tv2
            p[6]=tv4+0.05*tv4
            Fit = curve_fit(model ,x2fit, y2fit, p,lower=lbp, upper=ubp)
            R2=1 - var(Fit.resid) / var(y2fit)
            if R2 >= r_tresh
                #println(i)
                #push!(fit,Fit)
                #push!(r2,R2)
                r21=R2
                fit=Fit
                break
            end
        end


    end

    return (fit,r21)



end


#fit,r2=gauss_par_optimiz(x2fit,y2fit,g_numb,p,model,r_tresh)

######################################################################
#


function signal2store!(fit1,x2fit,y2fit,x_t,y_t,min_int)
    if size(fit1.param,1) == 3
        @.model(x, p) = (p[1]/(p[3]*sqrt(pi*2))) * exp(-(x-p[2])^2 / (2*p[3]^2))
        y_r=model(x_t,fit1.param)
        #y_r1=model(x2fit,fit.param)
        x_max=x_t[argmax(y_r)]
        s_sig=length(y_r[y_r.>min_int/2])

    elseif size(fit1.param,1) == 6
        @.model1(x, p) = (p[1]/(p[3]*sqrt(pi*2))) * exp(-(x-p[2])^2 / (2*p[3]^2))+
        (p[4]/(p[6]*sqrt(pi*2))) * exp(-(x-p[5])^2 / (2*p[6]^2))
        y_r=model1(x_t,fit1.param)
        x_max=x_t[argmax(y_r)]
        s_sig=length(y_r[y_r.>min_int/2])

    end
    x_rep=x_t[y_r.>min_int/2]
    y_rep=y_t[y_r.>min_int/2]
    if length(y_r[y_r .> min_int/2]) > 0
        y_t[y_r .> min_int/2]=(min_int/2)*ones(length(y_t[y_r .> min_int/2]))
    else
        y_t[y_t .> min_int/2]=(min_int/2)*ones(length(y_t[y_t .> min_int/2]))
    end


    return (x_rep,y_rep,y_t,x_max,s_sig)


end

#x_rep,y_rep,y,x_max,s_sig=signal2store!(fit1,x2fit,y2fit,x_t,y_t,min_int)






#######################################################################


function peak_detect(mz_vect,int_vect,res,mind,min_ms_w,r_tresh,ms_maxim,min_int,S2N)

    x_t,y_t = res2mass_window(mz_vect,int_vect,res,min_ms_w,ms_maxim)

    if length(x_t)>0 && maximum(y_t)> min_int/2
        y_s=ra_smooth(y_t,3)
        #x=x_t
        #y=y_s[:,1]
        x2fit, y2fit, res_m=isolate_sig(x_t,y_s[:,1])
        if length(x2fit)>0 #&& maximum(y_t)/median(y_t)>=S2N
            g_numb=1
            r2,fit1=simple_gauss(x2fit,y2fit,g_numb,r_tresh,min_ms_w)
            if r2 <= r_tresh
                g_numb=2
                r2,fit1=simple_gauss(x2fit,y2fit,g_numb,r_tresh,min_ms_w)
            end


            if r2 >= r_tresh
                x_rep,y_rep,y,x_max,s_sig=signal2store!(fit1,x2fit,y2fit,x_t,y_t,min_int)
            else
                #x_rep,y_rep,y,x_max,s_sig=signal2store!(fit1,x2fit,y2fit,x_t,y_t,min_int)
                y_t=(min_int/2)*ones(length(y_t))
                y=y_t
                s_sig=0
                x_rep=[]
                y_rep=[]
                x_max=[]

            end
        else
            y_t=(min_int/2)*ones(length(y_t))
            y=y_t
            s_sig=0
            x_rep=[]
            y_rep=[]
            x_max=[]


        end


    else
        y_t=(min_int/2)*ones(length(y_t))
        y=y_t
        s_sig=0
        x_rep=[]
        y_rep=[]
        x_max=[]


    end

    return (x_rep,y_rep,y,x_t,x_max,s_sig)


end

#######################################################################
#

function feature_isolate!(bounds,t_x_r,t_y_r,res,mind,min_ms_w,r_tresh,ms_maxim,
    sig_inc_thresh,min_int,S2N)

    mz_vect=t_x_r[bounds[2],:]
    int_vect=t_y_r[bounds[2],:]
    #plot(mz_vect,int_vect)
    x_rep,y_rep,y,x_t,x_max,s_sig=peak_detect(mz_vect,int_vect,res,mind,
    min_ms_w,r_tresh,ms_maxim,min_int,S2N)
    feature=zeros(size(t_x_r,1),4*length(y))
    masses=zeros(size(t_x_r,1),4*length(y))
    y_f=zeros(size(t_x_r,1),4*length(y))
    if length(x_rep)>0
        in_ind=convert(Int32,ceil((length(y)/2)-(size(x_rep,1)/2)))
        if in_ind > 0
            feature[bounds[2],in_ind:(in_ind+size(x_rep,1))-1]=y_rep
            masses[bounds[2],in_ind:(in_ind+size(x_rep,1))-1]=x_rep
        elseif in_ind == 0

            feature[bounds[2],in_ind+1:(in_ind+size(x_rep,1))]=y_rep
            masses[bounds[2],in_ind+1:(in_ind+size(x_rep,1))]=x_rep

        end

    end

    if length(y)==length(t_y_r[1,:])
        t_y_r[bounds[2],:]=y
    else
        ln=findfirst(isequal(minimum(x_t)),mz_vect)
        hn=findfirst(isequal(maximum(x_t)),mz_vect)
        t_y_r[bounds[2],ln:hn]=y
    end

    mz_vect=[]
    int_vect=[]
    ind_f=bounds[3]-bounds[1]-bounds[2]
    ind_rev=bounds[3]-bounds[1]-ind_f-1

    for i=1:ind_f

        #i=3
        #println(i)

        mz_vect=t_x_r[convert(Int32,bounds[2]+i),:]
        int_vect=t_y_r[convert(Int32,bounds[2]+i),:]

        x_rep,y_rep,y,x_t,x_max,s_sig=peak_detect(mz_vect,int_vect,res,mind,
        min_ms_w,r_tresh,ms_maxim,min_int,S2N)

        #plot(x_rep,y_rep)


        if s_sig > 0 && maximum(y_rep)/maximum(feature[convert(Int32,bounds[2]+i-1),:]) <= ((100+sig_inc_thresh)/100)


            in_ind=convert(Int32,floor(length(y)/2-(size(x_rep,1)/2)))
            if in_ind == 0
                in_ind=1
            end
            #println(y_rep)
            feature[convert(Int32,bounds[2]+i),in_ind:(in_ind+size(x_rep,1))-1]=y_rep
            masses[convert(Int32,bounds[2]+i),in_ind:(in_ind+size(x_rep,1))-1]=x_rep
            ln=findfirst(isequal(minimum(x_t)),mz_vect)
            hn=findfirst(isequal(maximum(x_t)),mz_vect)
            t_y_r[convert(Int32,bounds[2]+i),ln:hn]=y
            y_rep=[]
            x_rep=[]
            s_sig=[]

            #plot(masses[convert(Int32,bounds[2]+i),:],feature[convert(Int32,bounds[2]+i),:])

        elseif s_sig > 0 && maximum(y_rep)<3*min_int && abs(maximum(y_rep)-maximum(feature[convert(Int32,bounds[2]+i-1),:])) <= ((3*sig_inc_thresh)/100)*min_int

            in_ind=convert(Int32,floor(length(y)/2-(size(x_rep,1)/2)))
            if in_ind == 0
                in_ind=1
            end
            #println(y_rep)
            feature[convert(Int32,bounds[2]+i),in_ind:(in_ind+size(x_rep,1))-1]=y_rep
            masses[convert(Int32,bounds[2]+i),in_ind:(in_ind+size(x_rep,1))-1]=x_rep
            ln=findfirst(isequal(minimum(x_t)),mz_vect)
            hn=findfirst(isequal(maximum(x_t)),mz_vect)
            t_y_r[convert(Int32,bounds[2]+i),ln:hn]=y
            y_rep=[]
            x_rep=[]
            s_sig=[]
        elseif s_sig == 0 && length(x_rep) < 1 && length(y) == length(t_y_r[1,:])
            t_y_r[convert(Int32,bounds[2]+i),:]=y
            y_rep=[]
            x_rep=[]
            s_sig=[]
            break

        elseif s_sig == 0 && length(x_rep) < 1 && length(y) > 0 && length(y) < length(t_y_r[1,:])
            ln=findfirst(isequal(minimum(x_t)),mz_vect)
            hn=findfirst(isequal(maximum(x_t)),mz_vect)
            t_y_r[convert(Int32,bounds[2]+i),ln:hn]=y
            y_rep=[]
            x_rep=[]
            s_sig=[]
            break

        elseif s_sig == 0 && length(x_rep) < 1 && length(y) ==0
            y_rep=[]
            x_rep=[]
            s_sig=[]
            break

        end

    end

    for i=1:ind_rev
        #println(i)

        if convert(Int32,bounds[2]-i)>0
            mz_vect=t_x_r[convert(Int32,bounds[2]-i),:]
            int_vect=t_y_r[convert(Int32,bounds[2]-i),:]
            ind2=convert(Int32,bounds[2]-i)
        elseif convert(Int32,bounds[2]-i)==0
            mz_vect=t_x_r[convert(Int32,1),:]
            int_vect=t_y_r[convert(Int32,1),:]
            ind2=1
        end

        x_rep,y_rep,y,x_t,x_max,s_sig=peak_detect(mz_vect,int_vect,res,mind
        ,min_ms_w,r_tresh,ms_maxim,min_int,S2N)

        #println(y_rep)

        if s_sig > 0 &&  maximum(y_rep)/maximum(feature[convert(Int32,bounds[2]-i+1),:]) <= ((100+sig_inc_thresh)/100)
            in_ind=convert(Int32,floor(length(y)/2-(size(x_rep,1)/2)))
            if in_ind == 0
                in_ind=1
            end
            feature[convert(Int32,bounds[2]-i),in_ind:(in_ind+size(x_rep,1))-1]=y_rep
            masses[convert(Int32,bounds[2]-i),in_ind:(in_ind+size(x_rep,1))-1]=x_rep
            ln=findfirst(isequal(minimum(x_t)),mz_vect)
            hn=findfirst(isequal(maximum(x_t)),mz_vect)
            t_y_r[convert(Int32,bounds[2]-i),ln:hn]=y
            y_rep=[]
            x_rep=[]
            s_sig=[]

            #plot(masses[convert(Int32,bounds[2]+i),:],feature[convert(Int32,bounds[2]+i),:])
        elseif s_sig > 0 && maximum(y_rep)<3*min_int && abs(maximum(y_rep)-maximum(feature[convert(Int32,bounds[2]-i+1),:])) <= ((3*sig_inc_thresh)/100)*min_int
            in_ind=convert(Int32,floor(length(y)/2-(size(x_rep,1)/2)))
            if in_ind == 0
                in_ind=1
            end
            feature[convert(Int32,bounds[2]-i),in_ind:(in_ind+size(x_rep,1))-1]=y_rep
            masses[convert(Int32,bounds[2]-i),in_ind:(in_ind+size(x_rep,1))-1]=x_rep
            ln=findfirst(isequal(minimum(x_t)),mz_vect)
            hn=findfirst(isequal(maximum(x_t)),mz_vect)
            t_y_r[convert(Int32,bounds[2]-i),ln:hn]=y
            y_rep=[]
            x_rep=[]
            s_sig=[]

        elseif s_sig == 0 && length(x_rep) < 1 && length(y) == length(t_y_r[1,:])

            t_y_r[ind2,:]=y
            y_rep=[]
            x_rep=[]
            s_sig=[]
            break
        elseif s_sig == 0 && length(x_rep) < 1 && length(y) > 0 &&length(y) < length(t_y_r[1,:])
            ln=findfirst(isequal(minimum(x_t)),mz_vect)
            hn=findfirst(isequal(maximum(x_t)),mz_vect)
            t_y_r[ind2,ln:hn]=y
            y_rep=[]
            x_rep=[]
            s_sig=[]
            break
        elseif s_sig == 0 && length(x_rep) < 1 && length(y) == 0
            y_rep=[]
            x_rep=[]
            s_sig=[]
            break

        end

    end

    return (feature,masses,t_y_r)



end

#masses[masses.>0]

###################################################
#Resolution calculator
#  i=3
# tv2[13]

function res_calc(feature,masses)
    resolution=zeros(size(feature,1))

    for i=1:size(feature,1)
        ttv1=feature[i,:]
        ttv2=masses[i,:]
        tv1=ttv1[ttv1.>0]
        tv2=ttv2[ttv2.>0]
        #plot(tv1)
        if size(tv1[tv1 .> 0 ],1)>1
            m_max=maximum(tv1)
            lb_m=findfirst(x -> x >= m_max/2,tv1)
            hb_m=findlast(x -> x >= m_max/2,tv1[lb_m:end])
            if hb_m+lb_m <= length(tv1) && tv2[hb_m+lb_m] > 0
                dm_meas=tv2[hb_m+lb_m]-tv2[lb_m]
            else
                dm_meas=tv2[hb_m+lb_m-1]-tv2[lb_m]
            end

            if isnan(tv2[argmax(tv1)]/dm_meas*1) .==0 && isequal(tv2[argmax(tv1)]/dm_meas,missing)==0
                resolution[i]=round(tv2[argmax(tv1)]/dm_meas)
            else
                resolution[i]=0
            end


        end
        #println(resolution[i])
    end
    return resolution

end


#plot(resolution)
###################################################
#The feature detection function


function feature_detect_cor_based(hz,mz_vals,mz_int,max_t_peak_w,min_int,res,min_ms_w,
    r_tresh,sig_inc_thresh,S2N,min_peak_w_s,scan_numb,mass_bounds)

    t_y_r,t_x_r,bounds,mind,ms_maxim =max_sig_finder_cor_based(mz_vals,mz_int,
    max_t_peak_w,min_int,scan_numb,mass_bounds)

    if size(t_y_r,1) >0

        feature,masses,t_y_r=feature_isolate!(bounds,t_x_r,t_y_r,res,mind,min_ms_w,
        r_tresh,ms_maxim,sig_inc_thresh,min_int,S2N)
        if size(feature,1) > 1
            t_dim=zeros(size(feature,1))

            for i=1:size(feature,1)
                tv1=feature[i,:]

                if size(tv1[tv1 .> 0 ],1)>1

                    t_dim[i]=tv1[argmin(abs.(masses[i,:] .-  ms_maxim))]

                end
            end

            #plot(t_dim)
            #scatter!(t_dim)

            if size(t_dim[t_dim .> 0],1)>1 && maximum(t_dim)/median(t_dim) >= S2N
                g_numb=1
                r2,fit1=simple_gauss(range(1,stop=length(t_dim)),t_dim,g_numb,r_tresh,min_ms_w)
                #Run the model first
                #y_r=model(range(1,stop=length(t_dim)),fit1.param)
                if r2 < r_tresh
                    g_numb=2
                    r2,fit1=simple_gauss(range(1,stop=length(t_dim)),t_dim,g_numb,r_tresh,min_ms_w)
                end

                if length(r2) > 0 && r2 >= r_tresh && length(t_dim[t_dim .> 0]) > min_peak_w_s
                    resolution=res_calc(feature,masses)
                    resolution[isnan.(resolution)].=0
                    #plot(resolution)
                    med_res=round(mean(resolution[resolution .> 0]))
                    scan_num=mind[1]
                    p_w_s=length(t_dim[t_dim .> 0])
                    ret_t=round(mind[1]/(hz*60),digits=2)
                    p_w_t=round(p_w_s/(hz),digits=2)
                    mass_meas=round(mean(masses[masses .> 0]),digits=4)
                    min_mass=round(minimum(masses[masses .> 0]),digits=4)
                    max_mass=round(maximum(masses[masses .> 0]),digits=4)
                    feat_int=round(sum(feature))
                    feat_purity=g_numb
                    sig=maximum(feature)
                else
                    scan_num=mind[1]
                    p_w_s=[]
                    ret_t=[]
                    p_w_t=[]
                    mass_meas=[]
                    min_mass=[]
                    max_mass=[]
                    feat_int=[]
                    feat_purity=[]
                    med_res =[]
                    sig=maximum(feature)

                end

            else
                scan_num=mind[1]
                p_w_s=[]
                ret_t=[]
                p_w_t=[]
                mass_meas=[]
                min_mass=[]
                max_mass=[]
                feat_int=[]
                feat_purity=[]
                med_res =[]
                sig=maximum(feature)
            end

        else
            scan_num=mind[1]
            p_w_s=[]
            ret_t=[]
            p_w_t=[]
            mass_meas=[]
            min_mass=[]
            max_mass=[]
            feat_int=[]
            feat_purity=[]
            med_res =[]
            sig=maximum(feature)


        end
    else
        scan_num=[]
        p_w_s=[]
        ret_t=[]
        p_w_t=[]
        mass_meas=[]
        min_mass=[]
        max_mass=[]
        feat_int=[]
        feat_purity=[]
        med_res =[]
        sig=min_int

    end


    return (sig,t_y_r,bounds,scan_num,p_w_s,ret_t,p_w_t,mass_meas,min_mass,max_mass,feat_int,
    feat_purity,med_res)

end

######################################################################
## Import function name(args)

function import_files(format,numchan,namechan,pathin,filenames)
    if (format=="mat")
        chrom=mzXML_MAT_Import(numchan,namechan,pathin,filenames)
        mz_vals=chrom["MS1"]["Mz_values"]
        mz_int=chrom["MS1"]["Mz_intensity"]
        #plot(mz_vals[100,:],mz_int[100,:])
        #plot!(mz_vals[101,:],mz_int[101,:])
        t0=chrom["MS1"]["Rt"][1]/60
        t_end=chrom["MS1"]["Rt"][end]/60

    elseif format == "cdf"

        chrom=cdf_Import(numchan,namechan,pathin,filenames)

        mz_vals=chrom["MS1"]["Mz_values"]
        mz_int=chrom["MS1"]["Mz_intensity"]
        t0=chrom["MS1"]["t0"][1]
        t_end=chrom["MS1"]["t_end"][1]
    end

    return (mz_vals,mz_int,t0,t_end)

end

#run(`python Import_mzXML.py`)

######################################################################
## Import function batch

function file_name2_Import(format,numchan,pathin)
    Dname=readdir(pathin)
    Fnames=[]
    Paths=[]
    for i=1:size(Dname,1)
        if (format == "cdf" && isdir(pathin*"\\"*Dname[i])==1)
            append!(Fnames,[readdir(pathin*"\\"*Dname[i])])
            append!(Paths,[pathin*"\\"*Dname[i]])
        elseif (format == "mat" )
            append!(Fnames,[Dname[i]])
            append!(Paths,[pathin])

        else
            continue
        end
    end
    return (Fnames,Paths)

end

#Fnames,Paths=file_name2_Import(format,numchan,pathin)



######################################################################
## Wrapper function


function feature_detect_signle_data_cor_based(numchan,namechan,pathin,filenames,
    max_t_peak_w,res,min_ms_w,r_tresh,min_int,sig_inc_thresh,S2N,format,min_peak_w_s)

    #cd("/")
    if isa(filenames,Array)==1
        m=split(filenames[1],".")
    else
        m=split(filenames,".")
    end

    target_name=m[1]*"_target.xlsx"
    target_table=DataFrame(XLSX.readtable(pathin*"\\"*target_name,"Sheet1")...)


    mz_vals,mz_int,t0,t_end=import_files(format,numchan,namechan,pathin,filenames)
    #chrom=py2julia_conversion(filename,pathtofile,pathout,savefile)
    #size(mz_vals)

    hz=(size(mz_int,1)/(60*(t_end-t0)))
    rep_table=zeros(size(target_table,1),11)

    for i=1:size(target_table,1)
        #println(i)
        #i=1
        scan_numb=target_table[:ScanNum][i]
        mass_bounds=[target_table[:MeasMass][i],target_table[:MinMass][i],
        target_table[:MaxMass][i]]

        sig,t_y_r,bounds,scan_num,p_w_s,ret_t,p_w_t,mass_meas,min_mass,max_mass,feat_int,
        feat_purity,med_res=feature_detect_cor_based(hz,mz_vals,mz_int,max_t_peak_w,min_int,res,min_ms_w,
            r_tresh,sig_inc_thresh,S2N,min_peak_w_s,scan_numb,mass_bounds)

        println([i,scan_num,mass_meas,sig,med_res])
        if length(med_res) > 0 && sig > min_int

            rep=[scan_num,p_w_s,ret_t,p_w_t,mass_meas,min_mass,max_mass,feat_int,
            sig,feat_purity,med_res]
            rep_table[i,:]=rep

        elseif sig == min_int
            break

        end
        mz_int[bounds[1]:bounds[3],:]=t_y_r
    end

    table=DataFrame(rep_table,[:ScanNum,:ScanInPeak,:Rt,:SecInPeak,
    :MeasMass,:MinMass,:MaxMass,:Area,:Int,:FeatPurity,:MediRes])

    #sort!(table,[:Rt,:MeasMass])

    #final_table=table[table[:Int] .> 0,:]
    final_table=table


    return (final_table,m)

end



###################
## A wraper function for sensistivity analysis min_ms_w

function Sensitivity_test_fun(numchan,namechan,pathin,filenames,
    max_t_peak_w,res,min_ms_w,r_tresh,min_int,sig_inc_thresh,S2N,format,min_peak_w_s)


    for i=1:length(min_ms_w)
        for j=1:length(res)

            final_table,m=feature_detect_signle_data_cor_based(numchan,namechan,pathin,filenames,
                max_t_peak_w,res[j],min_ms_w[i],r_tresh,min_int,sig_inc_thresh,S2N,format,min_peak_w_s)

            fn="MassWin_"*string(min_ms_w[i])*"_Res_"*string(res[j])
            XLSX.writetable(pathin *"\\"*fn*".xlsx",DataFrames.columns(final_table), DataFrames.names(final_table))
        end


    end



end




#end
#i=1
#j=1
##############################
# Test

numchan=1
namechan=["MS1"]
pathin="D:\\Data\\julia\\data_jake\\Sensitivity_test"
filenames=["180419-TofMS-JO054-067 Day5 B.mat"]

max_numb_iter=8000
max_t_peak_w=30
res=5000:10000:100000
min_ms_w=0.001:0.005:0.2
r_tresh=0.85
min_int=2000
sig_inc_thresh=5
S2N=1.5
#format="cdf"
format="mat"
min_peak_w_s=3

@time Sensitivity_test_fun(numchan,namechan,pathin,filenames,
    max_t_peak_w,res,min_ms_w,r_tresh,min_int,sig_inc_thresh,S2N,format,min_peak_w_s)



#########################################
